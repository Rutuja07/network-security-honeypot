-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 24, 2015 at 08:06 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `joomla`
--

-- --------------------------------------------------------

--
-- Table structure for table `imwls_assets`
--

CREATE TABLE IF NOT EXISTS `imwls_assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) unsigned NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_asset_name` (`name`),
  KEY `idx_lft_rgt` (`lft`,`rgt`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `imwls_assets`
--

INSERT INTO `imwls_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 0, 115, 0, 'root.1', 'Root Asset', '{"core.login.site":{"6":1,"2":1},"core.login.admin":{"6":1},"core.login.offline":{"6":1},"core.admin":{"8":1},"core.manage":{"7":1},"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(8, 1, 17, 32, 1, 'com_content', 'com_content', '{"core.admin":{"7":1},"core.options":[],"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(9, 1, 33, 34, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 35, 36, 1, 'com_installer', 'com_installer', '{"core.admin":[],"core.manage":{"7":0},"core.delete":{"7":0},"core.edit.state":{"7":0}}'),
(11, 1, 37, 38, 1, 'com_languages', 'com_languages', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(12, 1, 39, 40, 1, 'com_login', 'com_login', '{}'),
(13, 1, 41, 42, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 43, 44, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 45, 46, 1, 'com_media', 'com_media', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":{"5":1}}'),
(16, 1, 47, 48, 1, 'com_menus', 'com_menus', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(17, 1, 49, 50, 1, 'com_messages', 'com_messages', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(18, 1, 51, 82, 1, 'com_modules', 'com_modules', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(19, 1, 83, 86, 1, 'com_newsfeeds', 'com_newsfeeds', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(20, 1, 87, 88, 1, 'com_plugins', 'com_plugins', '{"core.admin":{"7":1},"core.manage":[],"core.edit":[],"core.edit.state":[]}'),
(21, 1, 89, 90, 1, 'com_redirect', 'com_redirect', '{"core.admin":{"7":1},"core.manage":[]}'),
(22, 1, 91, 92, 1, 'com_search', 'com_search', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(23, 1, 93, 94, 1, 'com_templates', 'com_templates', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(24, 1, 95, 98, 1, 'com_users', 'com_users', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(26, 1, 99, 100, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 21, 2, 'com_content.category.2', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(30, 19, 84, 85, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(32, 24, 96, 97, 1, 'com_users.category.7', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(33, 1, 101, 102, 1, 'com_finder', 'com_finder', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(34, 1, 103, 104, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{"core.admin":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(35, 1, 105, 106, 1, 'com_tags', 'com_tags', '{"core.admin":[],"core.manage":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(36, 1, 107, 108, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(37, 1, 109, 110, 1, 'com_ajax', 'com_ajax', '{}'),
(38, 1, 111, 112, 1, 'com_postinstall', 'com_postinstall', '{}'),
(39, 18, 52, 53, 2, 'com_modules.module.1', 'Main Menu', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(40, 18, 54, 55, 2, 'com_modules.module.2', 'Login', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(41, 18, 56, 57, 2, 'com_modules.module.3', 'Popular Articles', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(42, 18, 58, 59, 2, 'com_modules.module.4', 'Recently Added Articles', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(43, 18, 60, 61, 2, 'com_modules.module.8', 'Toolbar', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(44, 18, 62, 63, 2, 'com_modules.module.9', 'Quick Icons', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(45, 18, 64, 65, 2, 'com_modules.module.10', 'Logged-in Users', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(46, 18, 66, 67, 2, 'com_modules.module.12', 'Admin Menu', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(47, 18, 68, 69, 2, 'com_modules.module.13', 'Admin Submenu', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(48, 18, 70, 71, 2, 'com_modules.module.14', 'User Status', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(49, 18, 72, 73, 2, 'com_modules.module.15', 'Title', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(50, 18, 74, 75, 2, 'com_modules.module.16', 'Login Form', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(51, 18, 76, 77, 2, 'com_modules.module.17', 'Breadcrumbs', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(52, 18, 78, 79, 2, 'com_modules.module.79', 'Multilanguage status', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(53, 18, 80, 81, 2, 'com_modules.module.86', 'Joomla Version', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(54, 8, 22, 25, 2, 'com_content.category.8', 'About', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(55, 27, 19, 20, 3, 'com_content.article.1', 'Welcome to Comedy Central...!!!', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(56, 57, 27, 28, 3, 'com_content.article.2', 'How the Internet and a New Generation of Superfans Helped Create the Second Comedy Boom', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(57, 8, 26, 31, 2, 'com_content.category.9', 'News', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(58, 54, 23, 24, 3, 'com_content.article.3', 'Comedic genres', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}'),
(63, 1, 113, 114, 1, 'com_comment', 'COM_COMMENT', '{}'),
(64, 57, 29, 30, 3, 'com_content.article.4', ' Justin Bieber’s Comedy Central Roast: The top 10 jokes from the night ', '{"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1}}');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_associations`
--

CREATE TABLE IF NOT EXISTS `imwls_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.',
  PRIMARY KEY (`context`,`id`),
  KEY `idx_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_banners`
--

CREATE TABLE IF NOT EXISTS `imwls_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `custombannercode` varchar(2048) NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `params` text NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) NOT NULL DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`),
  KEY `idx_banner_catid` (`catid`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_banner_clients`
--

CREATE TABLE IF NOT EXISTS `imwls_banner_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_banner_tracks`
--

CREATE TABLE IF NOT EXISTS `imwls_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  KEY `idx_track_date` (`track_date`),
  KEY `idx_track_type` (`track_type`),
  KEY `idx_banner_id` (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_categories`
--

CREATE TABLE IF NOT EXISTS `imwls_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`extension`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `imwls_categories`
--

INSERT INTO `imwls_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 15, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '{}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 9, 10, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2011-01-01 00:00:01', 0, '0000-00-00 00:00:00', 0, '*', 1),
(8, 54, 1, 11, 12, 1, 'about', 'com_content', 'About', 'about', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 258, '2015-04-08 01:12:19', 0, '2015-04-08 01:12:19', 0, '*', 1),
(9, 57, 1, 13, 14, 1, 'news', 'com_content', 'News', 'news', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":"","image_alt":""}', '', '', '{"author":"","robots":""}', 258, '2015-04-08 05:37:31', 258, '2015-04-08 05:37:34', 0, '*', 1);

-- --------------------------------------------------------

--
-- Table structure for table `imwls_comment`
--

CREATE TABLE IF NOT EXISTS `imwls_comment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `contentid` int(10) NOT NULL DEFAULT '0',
  `component` varchar(50) NOT NULL DEFAULT '',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `userid` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `website` varchar(100) NOT NULL DEFAULT '',
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `spam` tinyint(1) NOT NULL DEFAULT '0',
  `voting_yes` int(10) NOT NULL DEFAULT '0',
  `voting_no` int(10) NOT NULL DEFAULT '0',
  `parentid` int(10) NOT NULL DEFAULT '-1',
  `importtable` varchar(30) NOT NULL DEFAULT '',
  `importid` int(10) NOT NULL DEFAULT '0',
  `importparentid` int(10) NOT NULL DEFAULT '-1',
  `unsubscribe_hash` varchar(255) NOT NULL,
  `moderate_hash` varchar(255) NOT NULL,
  `customfields` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `com_contentid` (`component`,`contentid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_comment_captcha`
--

CREATE TABLE IF NOT EXISTS `imwls_comment_captcha` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `insertdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `referenceid` varchar(100) NOT NULL DEFAULT '',
  `hiddentext` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_comment_queue`
--

CREATE TABLE IF NOT EXISTS `imwls_comment_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mailfrom` varchar(255) DEFAULT NULL,
  `fromname` varchar(255) DEFAULT NULL,
  `recipient` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `created` datetime NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'html',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_comment_setting`
--

CREATE TABLE IF NOT EXISTS `imwls_comment_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` varchar(50) NOT NULL DEFAULT '',
  `component` varchar(50) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `imwls_comment_setting`
--

INSERT INTO `imwls_comment_setting` (`id`, `note`, `component`, `params`) VALUES
(1, 'The standard joomla article manager', 'com_content', '{"basic":{"include_categories":"0","exclude_content_items":""},"security":{"authorised_users":["1"],"auto_publish":"1","notify_moderators":"0","moderators":["8"],"captcha":"0","captcha_type":"default","maxlength_text":"30000"},"layout":{"tree":"1","sort":"0","comments_per_page":"0","support_ubb":"1","support_pictures":"0","pictures_maxwidth":"200","voting_visible":"1","date_format":"age","show_readon":"1","menu_readon":"0","intro_only":"0","emoticon_pack":"default"},"template":{"template":"default"},"template_params":{"emulate_bootstrap":"1","minify_scripts":"1","notify_users":"1","pagination_position":"0","form_position":"0","form_avatar":"0","form_ubb":"1","required_user":"0","required_email":"0","show_rss":"0","show_search":"0","preview_visible":"0","preview_length":"80","preview_lines":"5"},"integrations":{"gravatar":"1","support_profiles":"0"}}');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_comment_voting`
--

CREATE TABLE IF NOT EXISTS `imwls_comment_voting` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ip` varchar(15) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_compojoom_customfields`
--

CREATE TABLE IF NOT EXISTS `imwls_compojoom_customfields` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `slug` varchar(255) NOT NULL DEFAULT '',
  `component` varchar(255) NOT NULL DEFAULT '',
  `show` enum('all','category') NOT NULL DEFAULT 'all',
  `type` varchar(100) NOT NULL DEFAULT 'text',
  `options` mediumtext,
  `default` varchar(255) DEFAULT '',
  `allow_empty` tinyint(3) NOT NULL DEFAULT '0',
  `params` mediumtext,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `filter` tinyint(3) NOT NULL DEFAULT '0',
  `ordering` bigint(20) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_compojoom_customfields_cats`
--

CREATE TABLE IF NOT EXISTS `imwls_compojoom_customfields_cats` (
  `compojoom_customfields_id` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  PRIMARY KEY (`compojoom_customfields_id`,`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_contact_details`
--

CREATE TABLE IF NOT EXISTS `imwls_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  `sortname1` varchar(255) NOT NULL,
  `sortname2` varchar(255) NOT NULL,
  `sortname3` varchar(255) NOT NULL,
  `language` char(7) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_content`
--

CREATE TABLE IF NOT EXISTS `imwls_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` varchar(5120) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `imwls_content`
--

INSERT INTO `imwls_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(1, 55, 'Welcome to Comedy Central...!!!', 'welcome-to-comedy-central', '<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;"><img class="pull-center" src="images/comedy.jpg" alt="" /></span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">According to modern views, Comedy (from the Greek: κωμῳδία, kōmōidía) refers to any discourse or work generally intended to be humorous or to amuse by inducing laughter, especially in theatre, television, film and stand-up comedy. The origins of the term are found in Ancient Greece. In the Athenian democracy, the public opinion of voters was influenced by the political satire performed by the comic poets at the theaters. The theatrical genre of Greek comedy can be described as a dramatic performance which pits two groups or societies against each other in an amusing agon or conflict. Northrop Frye depicted these two opposing sides as a "Society of Youth" and a "Society of the Old". A revised view characterizes the essential agon of comedy as a struggle between a relatively powerless youth and the societal conventions that pose obstacles to his hopes. In this struggle, the youth is understood to be constrained by his lack of social authority, and is left with little choice but to take recourse in ruses which engender very dramatic irony which provokes laughter.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Satire and political satire use comedy to portray persons or social institutions as ridiculous or corrupt, thus alienating their audience from the object of their humor. Parody subverts popular genres and forms, critiquing those forms without necessarily condemning them.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Other forms of comedy include Screwball comedy, which derives its humor largely from bizarre, surprising (and improbable) situations or characters, and Black comedy, which is characterized by a form of humor that includes darker aspects of human behavior or human nature. Similarly scatological humor, sexual humor, and race humor create comedy by violating social conventions or taboos in comic ways. A comedy of manners typically takes as its subject a particular part of society (usually upper class society) and uses humor to parody or satirize the behavior and mannerisms of its members. Romantic comedy is a popular genre that depicts burgeoning romance in humorous terms and focuses on the foibles of those who are falling in love.</span></span></p>', '', 1, 2, '2015-04-08 01:13:18', 258, '', '2015-04-12 18:54:19', 258, 0, '0000-00-00 00:00:00', '2015-04-08 01:13:18', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 1, '', '', 1, 31, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(2, 56, 'How the Internet and a New Generation of Superfans Helped Create the Second Comedy Boom', 'how-the-internet-and-a-new-generation-of-superfans-helped-create-the-second-comedy-boom', '<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;"><img class="pull-center" src="images/echo-boom-opener.w529.h352.2x.jpg" alt="Photo: Nathaniel Wood for Vulture" width="750" /></span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">“What’s up, people of the sex shop?” asks Hannibal Buress into an overdriven microphone at 9:30 p.m. on a recent Tuesday in L.A. He’s standing in front of a wall of crotchless panties, and next to a black curtain that divides the part of the West Hollywood sex-toy shop where a customer is examining Fleshlights from the part where the bi-weekly comedy show Performance Anxiety is taking place. Buress is in town for the roast of Justin Bieber, and he’s at the Pleasure Chest to rehearse his set for a small but enthusiastic audience that’s doing its best to ignore the commercial for stainless-steel G-spotters playing on a TV to Buress’s right.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">The sex-toy shop was the second place in two hours where I saw Buress, whose upcoming Comedy Central show had just been announced that morning. Earlier, he made a surprise appearance at Put Your Hands Together, a weekly stand-up show at the UCB Theatre on Franklin Avenue, to do the same roast jokes to a fully packed house. Of course he’s doing multiple sets: It’s Tuesday in Los Angeles, the best night for comedy in the country. I’d planned to see four shows, and I could’ve easily picked four different, equally great-sounding shows happening at the same time.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">After both sets, Buress, as many comedians do at midnight on Tuesdays, came to the Belly Room at the Comedy Store to watch a show called Roast Battle. Roast Battle is a phenomenon in L.A. (and will be everywhere when a TV deal comes through, which it surely will) in which comedians, two at a time, face off against each other, trading insults. Looking around the room — to Buress’s left is Jeff Ross, the stand-up veteran who mentors this small show; behind Buress is Jerrod Carmichael, the young comedian with a Spike Lee–directed HBO special under his belt; to Buress’s right is Dave Chappelle, smoking cigarettes and laughing loudly at every joke; further back, the standing-room-only crowd is spilling into the hallway — one can’t help but feel that this is the epicenter of a boom.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">It’s not just Los Angeles. There has never been a better time to be a comedian: The talent pool is broad, deep, and more diverse than ever before; a new generation of passionate fans is supporting experimental work; and there are countless ways (online, onscreen, in your earbuds, at live shows) for new voices to be heard and — not always a given when it comes to the internet — make a living. It’s a peak that hasn’t been seen since the first comedy boom, which lasted from 1979 to about 1995, and was defined by two stages: First, in the wake of the phenomenon that was Saturday Night Live, the popularity of George Carlin, Richard Pryor, and Steve Martin, and the debut of TV shows like An Evening at the Improv, hundreds of comedy clubs opened up overnight, resulting in a wave of frequently terrible observational comedians getting paid a lot of money to fill the bounty of open stages. Then, after the huge success of Seinfeld, Home Improvement, and Roseanne, many of those same comedians were all given sitcoms, most of which failed.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">With clubs closing and TV deals drying up, alternative comedy began to rise from the ashes around 1995, with confessional comedians like Janeane Garofalo and Marc Maron pushing comedy shows into nontraditional spaces. That lit a long fuse that finally boomed in a big way in 2009: the year Marc Maron started “WTF”; the year Broad City launched as a web series; the year Aziz Ansari taped his first stand-up special; the year Rob Delaney joined Twitter; the year Parks and Recreation, Community, and The League premiered, and Ellie Kemper joined The Office; when it seemed like UCB was the unofficial farm team for Saturday Night Live and every sitcom on TV.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Prior to 2009, only three comedians had ever sold out Madison Square Garden (not the theater, the actual arena); three comedians have done it in the last three years, each multiple times. According to UCB, over 10,000 people will take improv classes in the U.S. this year. More people watched SNL 40 than the Golden Globes. Comedy Central’s original programming has nearly doubled in the last three years, in competition with other networks that have beefed up their own comedy offerings. The hiring of a new Daily Show host — congratulations, Trevor Noah — was treated with as much anticipation and passionate critique as when LeBron James decided to go play for the Miami Heat. About as many people follow Sarah Silverman on Twitter than follow Hillary and Bill Clinton combined. Critics revere Louis C.K. and Amy Schumer as geniuses. Practically every comedian has a podcast or web series, or both. Right now, comedy is bigger than it’s ever been. There are more people doing it, and more people enjoying it. Thanks to the internet, comedians now have infinite means by which to reach their fans; and those fans are more fanatical than ever. Welcome to the Second Comedy Boom.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">“Amazon has shows, Netflix, YouTube, Hulu — there are a lot of different ways to get yourself out there as a comedian,” Buress told Vulture before the taping of the Bieber roast. “You can build yourself through the internet. People have been getting TV shows from having successful podcasts.” And even those who aren’t on TV are finding enthusiastic audiences, and ways to sustain a living in comedy on their own terms.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Thirty years ago, comedians had to fight for a few large slices of a small pie. In the ’90s, a few performers made millions as stars of network sitcoms, but most were left in the cold when comedy clubs started shutting down. Now the pie is bigger and slices more plentiful, which benefits everyone from Buress (who now draws paychecks as a star on three cable shows) down to the armies of unknown UCB performers getting paid to make videos for Funny or Die and College Humor.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Podcasting is just one of the many ways by which comedians can develop a fan base, but over the last couple years, it’s also become one of the most lucrative. According to Adam Sachs, the CEO of Midroll — the company sells ads for popular podcasts such as “WTF With Marc Maron,” Scott Aukerman’s “Comedy Bang! Bang!,” Pete Holmes’s “You Made It Weird,” and Paul Scheer’s “How Did This Get Made” — “many comedians could survive today with the revenue from their podcasts alone.” Sachs says a podcast with 40,000 downloads per episode can gross well over $75,000 a year, and shows in the 100,000-download range can gross somewhere between $250,000 and $400,000. He estimates that three or four Midroll podcasts will make over $1 million this year. This has been a boon to comedians like Maron, Aukerman, Chris Hardwick, and Bill Burr, comedy veterans who had to wait for the boom to truly break through — all of whom have recently parlayed successful podcasts into cable-TV shows and larger fees for stand-up appearances.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">“Pretty much all of my friends that came up with me in comedy are working,” says Kristen Schaal. “Television is in a golden age, and now there’s like 2,000 channels for comedians to play around on.” Instead of broad network sitcoms, more comedians are getting small, idiosyncratic shows on cable or streaming sites. In the last five years, Comedy Central has seen increased competition from IFC, FX, FXX, Fusion, TBS, Netflix, Amazon, Hulu, Yahoo, and TruTV.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Broadcast networks have also been getting smaller and stranger lately, as with Fox’s brilliantly weird (and well-rated) The Last Man on Earth. Vulture’s Josef Adalian explains that “with most new network comedies in recent years getting ratings not much higher than cable,” networks are realizing they might as well aim for “cable quality.” The ultimate goal for a comedian in 2015 isn’t starring on his or her own Home Improvement, a mainstream sitcom that waters down their stand-up material — it’s having their own Louie, a show on which the creator retains full auteurist control over every weird aspect: Kroll Show, Broad City, Comedy Bang! Bang!, The Last Man on Earth, The Eric Andre Show, NTSF:SD:SUV, Inside Amy Schumer, Nathan for You, Lucas Bros. Moving Co., Mulaney (yes, Mulaney might’ve seemed like a traditional broad sitcom, but the point is that was exactly what John Mulaney wanted), and others were all created, written, and headlined by their comedian stars. None of these performers are rich for life thanks to their shows, but they’re all earning a living doing artistically fulfilling work.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Compare this to the grind of the original comedy boom, during which, to eke out a decent income, a comedian had to tour the country doing the same 45-minute set. As Marc Maron explained to me over the phone, comedy clubs of the time were a “bar business, [they had] never been a show business. You were there to sell drinks.” Discos were dying, and comedy was the word. Richard Zoglin, explaining the original Comedy Boom in his 2008 book Comedy at the Edge, wrote: “An evening at the comedy club was perfect entertainment for a baby-boom generation that was just hitting its peak dating years. You could take a date to the movies, but the conversation had to wait until afterward. The discos were too noisy. At a comedy club you could talk during the show, gauge each other’s reactions, see what your date laughed at — or was grossed out by.”</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">To today’s fans, comedy is more than just background noise: “Millennials have a deeper connection to comedy than previous generations,” says Chanon Cook, Comedy Central’s head of research. According to Cook, it was television that made the difference. The explosion of comedy on TV helped kill comedy clubs and bring the original boom to an end — but the biggest stars of that boom wound up on Saturday Night Live or on their own sitcoms, many of which were essential viewing for millennials, planting the seeds of the second boom. Endless reruns of Seinfeld and stand-up specials on Comedy Central gave this generation an inbuilt awareness of comedy’s conventions: The comedy nerd was born.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">As Comedy Central claimed in an (admittedly self-serving) 2012 survey, millennials are the first generation that views humor as the most valuable part of their self-definition. “Comedy is to this generation what music was to previous generations,” Cook says. “They use it to define themselves. They use it to connect with people.” Maron described comedy nerds to me as “people who are into different facets of the history of comedy, the different types of comedy.” Says Pete Holmes: “The comedy audience of 2015 is like the guitar to the musician. They’re not just sitting there to get fucked up and smoke a cigarette inside, which is what it was in the ’80s; they’re there to actually participate in something.”</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;"><img class="pull-center" src="images/echo-boom-1.w529.h352.2x.jpg" alt="" width="750" /></span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">On Wednesday in L.A., I saw this type of fan in action at The Meltdown, the best comedy show in the city. Situated at NerdMelt, the West Hollywood comedy theater in the back of a comic-book store, produced by Emily Gordon, and hosted by comedians Jonah Ray and Kumail Nanjiani (who is Gordon’s husband and a star of HBO’s Silicon Valley), The Meltdown has run for five years and last year became a show on Comedy Central. With 200 people crammed into a 150-person space, the room was hot — figuratively hot and, thanks to L.A.’s late-winter heat wave, literally hot. The stage is less a stage and more a place where the chairs stop and the lights shine. The audience …Well, the audience is something. I’ll let Ray describe it:</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">They are some of the most dedicated, sweetest weirdos around. When we started, there were these different people that we would recognize, and they slowly started sitting closer to the stage, and they all started to become friends and know each other. Now, every week, the first three or four rows are the diehard regulars. They’re creatives and artists and comics themselves, and them knowing each other, and us knowing them, has created an amazing dynamic for Kumail and I to play with.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Gordon would tell me later that the week I was there, one of these fans got a tattoo of the hand stamp given to The Meltdown attendees.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Diehard fans, like those of The Meltdown, are now the norm at comedy shows around the country. Because of them, comedy has fundamentally changed in the following ways:</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><strong><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.4px;">1. Comedy is more meta.</span></span></strong></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Modern fans’ familiarity with the way comedy works means that the content of the comedy is often based in the process by which it’s created. “There’s a sophistication [in the audience] that wasn’t there back in the Evening at the Improv days,” Holmes tells me, referring to the 1980s stand-up TV show. “They know what a bit is.” Holmes, like most comedians in 2015, addresses the fact that he’s doing stand-up while he’s performing. Early in my week in L.A., at UCB’s monolithic Thai Town theater, Holmes closed the weeklyshow stand-up show Church, following Melissa Villaseñor, who destroyed the audience with a series of impressions ranging from Pikachu to Wanda Sykes. Taking the stage, Holmes sighed: “Well, she killed. Great.” He began his set: “My dog got sprayed by a skunk today — another comedian would then do 20 minutes of perfect material about that,” he said, mocking comedians who say things happened to them recently when, in fact, the things happened ages ago, whenever the performer started working on that joke. (Nowadays you’ll often hear jokes that start like, “A week ago — or a week ago from whenever I wrote this bit.”) His set’s biggest laugh probably came during a bit about having a dog despite having grown up with cats, riffing, “Ooh, a comedian talking about the difference between cats and dogs.” Comedy nerds are as interested in the source code as they are the jokes themselves. In 2015, the source code is the jokes.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><strong><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.4px;">2. The line between fan and performer is blurred.</span></span></strong></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">The UCB Theatre has done plenty to help foster the second comedy boom — popularizing improv, launching countless star performers, serving as the comedic hub for both New York and L.A., providing budding comedians with free stage time — but its most important contribution is the way it changed the relationship between audience and performer. According to Kevin Hines, from the UCB Theatre’s training center in New York, 11,918 students took classes at UCB NY or LA last year. 11,918! (About 6,400 of those took Improv 101, the theater’s entry-level class, which is twice as many that took the same class five years ago). At UCB, everyone performs, and everyone watches everyone else. It’s a scene, not just a bunch of shows. Pete Holmes, who took classes at UCB while pursuing stand-up, explains: “I like doing stand-up like improv, by looking at the audience as your scene partner.” At a comedy show in 2015, everybody works together to create the best show possible. Everyone’s a comedian.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;"><img class="pull-center" src="images/echo-boom-3.w529.h352.2x.jpg" alt="" width="750" /></span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><strong><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.4px;">3. Comedy is more conversational and less punch-line-driven.</span></span></strong></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Today, audiences expect comedians to talk to them like peers — they don’t like being performed down to. “[Comedy’s] changed and become more conversational,” Nanjiani says. “People are okay listening to a podcast that’s an hour and a half, and it’s not funny, funny, funny, funny.” Nanjiani says those fans “take that expectation to a comedy show as well,” and as a result, he’s learned to “hide his punch lines” so as not to seem detached. Now Nanjiani and Ray often go into The Meltdown shows without any particular plan, and essentially just hang out for a few minutes. “Comics that don’t make an effort to exist in that moment with everybody else in the room tend to get a colder reaction,” says Ray.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">This was on my mind as I watched comedian Mark Normand take the stage at The Meltdown. Two days earlier, at Hot Tub, Kristen Schaal and Kurt Braunohler’s weekly show, he had a rough go of it, telling well-crafted jokes (i.e., “I look at racism the same way I look at Nickelback: It’s fun to joke about, but ugh, you never want to see it live”) that would get short pulses of laughter instead of the bigger, rolling laugh of a fully engaged audience. The distance increased as Normand blamed the crowd for being too sensitive. He was trying to deliver his best material to an audience that didn’t want “material” at all — they just wanted to just get to know him. He connected better at the Meltdown. Setting up a set of jokes about homophobia, he asked if there were any gay guys in the audience. When no one responded, he joked, “Well, that is statistically impossible, [especially here,] in the back of a nerd museum.”</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><strong><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.4px;">4. Experimentation is expected.</span></span></strong></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Kristen Schaal, however, killed at both Hot Tub and The Meltdown with one of the funniest things I’ve seen in years: a one-woman show about Emily Dickinson (titled Emily Dick-unsung). Schaal’s version of Dickinson is a spinster with messy hair who hides poems around the stage and asks audience members to read them. One poem was just a grocery list, but Schaal acted as though it contained her innermost secrets: “This poem is not ready for the world!” Another poem was actually just an olive in a folded piece of paper; she made an audience member “read” it by eating the olive. The bit doesn’t make much sense (especially when one attempts to describe it after the fact), but it destroyed both of the audiences I saw it with. I learned later that these were only the second and third times she’d ever performed it.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;"><img class="pull-center" src="images/echo-boom-2.w529.h352.2x.jpg" alt="" width="750" /></span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">In 2015, audiences are happily willing to follow comedians down any rabbit hole, no matter how unlikely the payoff. Sure, experimental comedians have existed since the ’70s – Steve Martin, Andy Kaufman, Emo Philips, etc. — but no longer are they the exception, as comedy-nerd audiences expect and are ready for comedians to have fun with the form (especially, for the first time, not just white males). Watch Rory Scovel’s brilliant set from The Meltdown TV show, for example. Scovel takes the stage and, responding to the audience’s enthusiastic welcome, ditches his planned material, and tells the crowd to keep clapping for the entirety of his ten-minute set. It’s an odd bit, especially considering that it’s being taped for television. He doesn’t tell jokes. He just offers a running commentary on what’s happening and eggs his audience on. He wonders what it would be like to be a TV viewer tuning in halfway through the bit. Eventually, he becomes mock-angry at his at-home audience: “Stay the fuck out of here!” he shouts at the camera. “This is our time down here!” At the end, the claps become a standing ovation, and Scovel seems actually touched that everyone went along with this very silly idea. Even in the two minutes and 41 seconds that aired on Comedy Central, you can see exactly what comedy looks like right now.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">So, where to from here? Actually, comedy’s next generation is already beginning to emerge. Back in New York, I go to the Holy Fuck Comedy Hour, a madcap weekly sketch show at the new Williamsburg location of the Annoyance Theatre. Started in Chicago in the late ’80s, the Annoyance has been a launchpad for many comedy careers — including those of Stephen Colbert, Jane Lynch, Jeff Garlin, Vanessa Bayer, Jason Sudeikis, and others — though Second City, where those Chicagoans would also perform, usually hogs credit for them. The Annoyance’s month-old New York location, below the Williamsburg Bridge and underneath a jazz club, seems to be their attempt to assert themselves as the alternative to the former alternative, UCB.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">“Loud-lings” is how I like to describe the Holy Fuck Comedy Hour, which mixes Chicago-style brashness with the character focus of L.A.’s Groundlings. Perhaps to drown out the jazz coming from upstairs, everything is turned up to 11: Each sketch ends with someone onstage giving the cue to hit the “lights”; New York’s most inventive stand-up, Jo Firestone, asks the audience to shout out “sunshine, lollipops, or rainbows” whenever they want to interrupt her deadly serious (and hilarious) one-woman show about moving to New York; for a sketch in which gay parents bring their straight son to a doctor to turn him gay, a projector screen plays gay porn (with the actor who’s playing the son’s face Photoshopped onto every porn star’s) on a loop.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">The sketch of the night is slightly more subdued, but just as absurd. A woman comes onstage wearing a Saturday Night Live T-shirt, thumbing through her copy of the definitive SNL oral history Live From New York, geeking out just at the names of the people who agreed to be interviewed — “Bill Murray, hehe, awesome!” Then a boy comes onstage and asks what she’s reading. They flirt by repeating iconic SNL catchphrases (“Choppin’ broccoli!?”). The audience, packed densely enough to worry any good fire marshal, laughs knowingly. The woman is Claire Mulaney (John’s younger sister), a writer for Saturday Night Live, who is there performing the sketch on her week off. This is more than meta comedy for an accepting audience; it’s damn near postmodern. The sketch ends with Mulaney and her new beau screaming, “Live from New York, it’s Saturday Night!!!!!” Mulaney laughs. We all do. “Lights!”</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Source: <a href="http://www.vulture.com/2015/03/welcome-to-the-second-comedy-boom.html" target="_blank">www.vulture.com</a></span></span></p>', '', 1, 9, '2015-04-08 05:37:13', 258, '', '2015-04-12 19:30:19', 258, 0, '0000-00-00 00:00:00', '2015-04-08 05:37:13', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"0","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"0","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"0","show_item_navigation":"","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_vote":"","show_hits":"0","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 5, 1, '', '', 1, 9, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');
INSERT INTO `imwls_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(3, 58, 'Comedic genres', 'comedic-genres', '<p style="margin: 0.5em 0px; line-height: 22.3999996185303px; color: #252525; font-family: sans-serif; font-size: 14px; text-align: justify;">Comedy may be divided into multiple genres based on the source of humor, the method of delivery, and the context in which it is delivered.</p>\r\n<p style="margin: 0.5em 0px; line-height: 22.3999996185303px; color: #252525; font-family: sans-serif; font-size: 14px; text-align: justify;">These classifications overlap, and most comedians can fit into multiple genres. For example, deadpan comics often fall into observational boom comedy, or into black comedy or blue comedy to contrast the morbidity or offensiveness of the joke with a lack of emotion.</p>\r\n<table class="wikitable" style="margin: 0.5em 0px; line-height: 22.3999996185303px; color: #252525; font-family: sans-serif; font-size: 14px; text-align: justify;">\r\n<tbody>\r\n<tr><th style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: center;">Genre</th><th style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: center;">Description</th><th style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: center;">Notable examples</th></tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Alternative comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Differs from traditional punchline jokes which features many other forms of comedy such as observation, satire, surrealism, slapstick and improvisation</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Alexei Sayle, Mark Steel, Dan Harmon, Dave Gorman, Linda Smith, Jeremy Hardy, Ron Sparks, Alan Davies, Ben Elton, Jo Brand, Stewart Lee, Sean Hughes, Rik Mayall, Adrian Edmonson,Malcolm Hardee, Kristen Schaal</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Black comedyor dark comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Deals with disturbing subjects such as death,drugs, terrorism,rape, and war; can sometimes be related to the horrormovie genre</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Jim Norton, Bill Hicks, Frankie Boyle, Jimmy Carr, Denis Leary,Richard Pryor, Ricky Gervais, George Carlin, Chris Rush, Mike Ward, Penn &amp; Teller, Seth MacFarlane, Christopher Titus, Sacha Baron Cohen, Trey Parker, Quentin Tarantino, David Cross, Peter Kay</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Blue comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Typically sexual in nature (risqué) and/or using profane language; often using sexism,racism, andhomophobic views</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Jim Davidson, Frankie Boyle, Eddie Murphy, Bernard Manning,Martin Lawrence, George Lopez, Doug Stanhope, Seth MacFarlane, Redd Foxx, Bob Saget, Ron White, Dave Attell, Chris Rock, Sarah Silverman, Chappelle''s Show</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Character comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Derives humor from a persona invented by a performer; often fromstereotypes</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Phyllis Diller, Andy Kaufman, Bob Nelson, Catherine Tate, Paul Eddington, Andrew Dice Clay, Rich Hall, Tim Allen, John Gordon Sinclair, Lenny Henry, Sacha Baron Cohen, Christopher Ryan,Steve Guttenberg, Jerry Sadowitz, Steve Coogan, Bip, Jay London, Larry the Cable Guy, Sarah Silverman, Rob Brydon,Rowan Atkinson, Peter Helliar, Harry Enfield, Margaret Cho, Little Britain, Stephen Colbert, Al Murray, Paul Whitehouse, Charlie Higson, Kevin Hart</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Cringe comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">A comedy of embarrassment, in which the humor comes from inappropriate actions or words; usually popular in television shows and film, but occasionally in stand-up as well</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Ricky Gervais, Richard Herring, Rufus Hound, Larry David, Alan Partridge, Bob Saget; TV shows: <i>Curb Your Enthusiasm,</i> <i>The Office</i>, <i>Peep Show</i>, <i>The Proposal</i>, <i>Family Guy</i>, <i>The Inbetweeners</i></td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Deadpancomedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Not strictly a style of comedy, it is telling jokes without a change in facial expression or change of emotion</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Milton Jones, Jack Dee, Jimmy Carr, Steven Wright, Peter Cook,Craig Ferguson, Dylan Moran, W. Kamau Bell, Buster Keaton, Bill Murray, Jim Gaffigan, Les Dawson, Mike Birbiglia, Mitch Hedberg,Bruce McCulloch, Demetri Martin, Elliott Goblet, Aubrey Plaza,Zach Galifianakis</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Improvisational comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Improvisational (sometimes shortened to improv) comics rarely plan out their routines; television show examples:<i>Curb Your Enthusiasm</i>, <i>Whose Line Is It Anyway?</i>,<i>Thank God You''re Here</i></td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Robin Williams, Jonathan Winters, Bob Nelson, Paula Poundstone, Paul Merton, Tony Slattery, Josie Lawrence, Jim Sweeney, Steve Steen, Wayne Brady, Ryan Stiles, Colin Mochrie,Drew Carey, Greg Proops, John Sessions, Neil Mullarkey, Kathy Greenwood, Brad Sherwood, Chip Esten, Jeff Davis, Tina Fey,Amy Poehler, Jonathan Mangum, Mark Meer, Larry David</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Insult Comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">A form which consists mainly of offensive insults directed at the performer''s audience and/or other performers</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Don Rickles, Andrew Dice Clay, Ricky Gervais, Bob Saget,Frankie Boyle, Jimmy Carr, Jerry Sadowitz, Sam Kinison, Seth MacFarlane, Triumph, the Insult Comic Dog, Roy ''Chubby'' Brown,Marcus Valerius Martialis, Jeffrey Ross, Lisa Lampanelli, D.L. Hughley, Greg Giraldo, Goundamani</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Mockumentary</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">A parody using the conventions of documentary style</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Films and TV shows: <i>Borat</i>, <i>This is Spinal Tap</i>, <i>The Monkees</i>,<i>The Rutles</i>, <i>Summer Heights High</i>, <i>Electric Apricot: Quest for Festeroo</i>, <i>The Office</i>, <i>Brüno</i>, <i>Parks and Recreation</i>, <i>Modern Family</i>, <i>Come Fly with Me</i>, <i>Angry Boys</i>, <i>The Compleat Al</i>, "trailer park boys"</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Musical Comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">A form of alternative comedy where humor is mostly derived from musicwith (or sometimes without) lyrics</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Bill Bailey, Denis Leary, Tim Minchin, The Lonely Island, Flight Of The Conchords, Les Luthiers, Mitch Benn, Tenacious D, Spinal Tap, Stephen Lynch, "Weird Al" Yankovic, Bob Rivers, Bo Burnham, Wayne Brady, the Bonzo Dog Doo-Dah Band, Tom Lehrer, Victor Borge</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Observational comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Pokes fun ateveryday life, often by inflating the importance of trivial things or by observing the silliness of something that society accepts as normal</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">George Carlin, Jerry Seinfeld, Craig Ferguson, Larry David, Bill Cosby, Mitch Hedberg, Billy Connolly, Russell Howard, Cedric the Entertainer, Steve Harvey, W. Kamau Bell, Ray Romano, Chris Rush, Dane Cook, Ricky Gervais, Chris Rock, Jim Gaffigan, Kathy Greenwood, Ellen DeGeneres, Russell Peters</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Physical comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Somewhat similar toslapstick, this form uses physical movement and gestures; often influenced byclowning</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Jim Carrey, Bob Nelson, Norman Wisdom, Jerry Lewis, Robin Williams, Chevy Chase, John Ritter, Conan O''Brien, Kunal Nayyar, Mr. Bean, Michael Mcintyre, Lee Evans, Max Wall,Matthew Perry, Brent Butt, Kathy Greenwood, The Three Stooges, Lano &amp; Woodley, Lucille Ball, Dane Cook, Chris Farley</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Prop comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Relies on ridiculous props, casual jackets or everyday objects used in humorous ways</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Bob Nelson, Carrot Top, Jeff Dunham, Gallagher, Timmy Mallett,The Amazing Johnathan, Jerry Sadowitz</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Spoof</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">The recreating of a book, film or play for humor; it can be used to make fun of, or ridicule, a certain production</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Mel Brooks, French and Saunders, Mitchell and Webb, I''m Sorry I Haven''t a Clue, Peter Serafinowicz, Weird Al Yankovic, Zucker, Abrahams and Zucker; Films and TV shows: <i>Hot Shots</i>, <i>Frankie Boyle''s Tramadol Nights</i>, <i>Shriek</i>, <i>Look Around You</i></td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Sitcom</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Scripted dialogue creating a thematic situation; commonly found on television series</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;"><i>Seinfeld</i>, <i>Fawlty Towers</i>, <i>Black Books</i>, <i>Porridge</i>, <i>Dad''s Army</i>,<i>Blackadder</i>, <i>Gavin and Stacey</i>, <i>My Wife and Kids</i>, <i>I Love Lucy</i>,<i>Friends</i>, <i>Corner Gas</i>, <i>That ''70s Show</i>, <i>The Office</i>, <i>The Cosby Show</i>, <i>The Simpsons</i>, <i>Open All Hours</i>, <i>Only Fools and Horses</i>,<i>Dinner Ladies</i>, <i>Modern Family,</i> Melissa &amp; Joey</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Sketch</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">A shorter version of a sitcom, practised and typically performed live</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Armstrong and Miller, Jennifer Saunders, Lorne Michaels, Dawn French, Craig Ferguson, Catherine Tate; TV shows: <i>Monty Python</i>, <i>Armstrong and Miller</i>, <i>Saturday Night Live</i>, <i>Chappelle''s Show</i>, <i>Firesign Theatre</i>, <i>In Living Color</i>, <i>A Bit of Fry &amp; Laurie</i>,<i>Mad TV</i></td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Surreal comedy</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">A form of humorbased on bizarrejuxtapositions,absurd situations, and nonsense logic</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Spike Milligan, Jay Kogen, Eddie Izzard, J. Stewart Burns, Ross Noble, Bill Bailey, Brent Butt, The Mighty Boosh, Steven Wright,Trey Parker, Monty Python, Seth MacFarlane, David X. Cohen,Vic and Bob, The Goodies, Jack Handey, Derek Drymon, Wallace Wolodarsky, Harry Hill, The Kids in the Hall, Conan O''Brien, Tim and Eric, Paul Merton, Mitch Hedberg, Firesign Theatre, Shaun Micallef, Emo Philips</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Topical comedy/Satire</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Relies on headlining/important news and current affairs; it dates quickly, but is a popular form for late night talk-variety shows</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">George Carlin, Bill Hicks, Chris Morris, Dennis Miller, Conan O''Brien, Russell Howard, Craig Ferguson, David Letterman, Jay Leno, Dan Harmon, Andy Hamilton, Bill Maher, Ian Hislop, Brent Butt, Paul Merton, Kathy Griffin, Jon Stewart, Stephen Colbert,Stewart Lee, Matt Groening, Rory Bremner, W. Kamau Bell, Ben Elton, David Cross, Lewis Black, Chris Rock, Dave Chappelle, The Chaser, Punt and Dennis, Jon Holmes; TV shows: <i>The Daily Show</i>, <i>Have I Got News For You</i>, <i>Mock The Week</i>, <i>The News Quiz</i>, <i>Saturday Night Live</i>, <i>The Simpsons</i>, <i>The Tonight Show,</i>Late Show with David Letterman<i>,</i> Wait Wait... Don''t Tell Me!<i>,</i>South Park</td>\r\n</tr>\r\n<tr>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">Wit/Word play</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em;">More intellectual forms based on clever, often subtle manipulation of language (thoughpuns can be crude and farcical)</td>\r\n<td style="border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;">Groucho Marx, William Shakespeare, Harry Hill, Jay Jason, Oscar Wilde, Rodney Marks, Woody Allen, George Carlin, Tim Vine,Stephen Fry, Demetri Martin, Bo Burnham, Firesign Theatre, Myq Kaplan</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Source: <a href="http://en.wikipedia.org/wiki/Comedic_genres" target="_blank">Wikipedia</a></p>', '', 1, 8, '2015-04-08 05:38:36', 258, '', '2015-04-08 05:43:07', 258, 0, '0000-00-00 00:00:00', '2015-04-08 05:38:36', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 0, '', '', 1, 5, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(4, 64, ' Justin Bieber’s Comedy Central Roast: The top 10 jokes from the night ', 'justin-bieber-s-comedy-central-roast-the-top-10-jokes-from-the-night', '<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;"><img src="images/comedy-central-roast-justin-bieber-show.jpg" alt="" /></span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">The joke’s on Justin Bieber.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Everything haters have wanted to tell the scandal-ridden pop star was finally uttered during Monday night’s Comedy Central roast of the 21-year-old Canadian – and then some.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Whether it was his rocky on-and-off romance with Selena Gomez or his numerous run-ins with the law, Bieber egged on comedians, including host Kevin Hart, as they took turns skewering him onstage.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">The tattooed troublemaker took the humorously inappropriate jabs in good spirit, even using the moment as a turning point in his attempt to shed his bad-boy image and reinvent himself for the better.</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">"The things that I''ve done really don''t define who I am. I''m a kind-hearted person who loves people, and through it all, I lost some of my best qualities. For that, I''m sorry," he said during the event, taped two weeks prior. "What I can say is I''m looking forward to being someone who you guys can look at and be proud of."</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Until that day comes, here’s a look at the best jokes from Comedy Central’s roast:</span></span></p>\r\n<h3><strong>KEVIN HART</strong></h3>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Ebola patients hear about ‘Bieber fever’ and say, ‘I’m gonna go ahead and ride this one out.’”</span></span></p>\r\n<h3><strong>JEFFREY ROSS</strong></h3>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">“Is it true you dumped (Selena Gomez) because she grew a mustache before you?”</span></span></p>\r\n<h3><strong>LUDACRIS</strong></h3>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">"The Brazilian prostitute that claimed she was with Justin told the news that he was well-endowed. And that prostitute would know because so was he."</span></span></p>\r\n<h3><strong>CHRIS D''ELIA</strong></h3>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">“You have it all … Except love, friends, good parents, and a Grammy.”</span></span></p>\r\n<h3><strong>NATASHA LEGGERO</strong></h3>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">"Justin, Selena Gomez had to f--k you. She is literally the least lucky Selena in all of entertainment history."</span></span></p>\r\n<h3><strong>HANNIBAL BURESS</strong></h3>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">“They say that you roast the ones you love, but I don’t like you at all, man. I’m just here because it’s a real good opportunity for me.”</span></span></p>\r\n<h3><strong>RON BURGUNDY</strong></h3>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">“This kid has spunk, moxie, and a few other STDs.”</span></span></p>\r\n<h3><strong>SHAQ</strong></h3>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">“Justin, as a father of six you have to straighten up, son. Last year, you were ranked the fifth most hated person of all time. Kim Jong-Un didn’t rank that low. And he uses your music to torture people.”</span></span></p>\r\n<h3><strong>SNOOP DOGG</strong></h3>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">“You bought a monkey! I mean, that monkey was more embarrassed than the one that started the AIDS epidemic.”</span></span></p>\r\n<h3><strong>MARTHA STEWART</strong></h3>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">“Justin, I’m sure it’s great to have 60 million followers on Twitter, but the only place people will be following you in jail is into the shower.”</span></span></p>\r\n<p style="margin: 0.5em 0px; text-align: justify;"><span style="color: #252525; font-family: sans-serif;"><span style="font-size: 14px; line-height: 22.3999996185303px;">Source: <a href="http://www.nydailynews.com/entertainment/tv/10-best-jokes-justin-bieber-comedy-central-roast-article-1.2167143" target="_blank">Daily News</a></span></span></p>', '', 1, 9, '2015-04-12 19:31:49', 258, '', '2015-04-12 19:32:30', 258, 0, '0000-00-00 00:00:00', '2015-04-01 19:31:00', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"0","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"0","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"0","show_item_navigation":"","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_vote":"","show_hits":"0","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 0, '', '', 1, 1, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_contentitem_tag_map`
--

CREATE TABLE IF NOT EXISTS `imwls_contentitem_tag_map` (
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `core_content_id` int(10) unsigned NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) unsigned NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table',
  UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  KEY `idx_tag_type` (`tag_id`,`type_id`),
  KEY `idx_date_id` (`tag_date`,`tag_id`),
  KEY `idx_tag` (`tag_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_core_content_id` (`core_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Table structure for table `imwls_content_frontpage`
--

CREATE TABLE IF NOT EXISTS `imwls_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_content_rating`
--

CREATE TABLE IF NOT EXISTS `imwls_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_content_types`
--

CREATE TABLE IF NOT EXISTS `imwls_content_types` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_title` varchar(255) NOT NULL DEFAULT '',
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `table` varchar(255) NOT NULL DEFAULT '',
  `rules` text NOT NULL,
  `field_mappings` text NOT NULL,
  `router` varchar(255) NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) DEFAULT NULL COMMENT 'JSON string for com_contenthistory options',
  PRIMARY KEY (`type_id`),
  KEY `idx_alias` (`type_alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10000 ;

--
-- Dumping data for table `imwls_content_types`
--

INSERT INTO `imwls_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{"special":{"dbtable":"#__content","key":"id","type":"Content","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"state","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"introtext", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"attribs", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"asset_id"}, "special":{"fulltext":"fulltext"}}', 'ContentHelperRoute::getArticleRoute', '{"formFile":"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml", "hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(2, 'Contact', 'com_contact.contact', '{"special":{"dbtable":"#__contact_details","key":"id","type":"Contact","prefix":"ContactTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"address", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"image", "core_urls":"webpage", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"con_position":"con_position","suburb":"suburb","state":"state","country":"country","postcode":"postcode","telephone":"telephone","fax":"fax","misc":"misc","email_to":"email_to","default_con":"default_con","user_id":"user_id","mobile":"mobile","sortname1":"sortname1","sortname2":"sortname2","sortname3":"sortname3"}}', 'ContactHelperRoute::getContactRoute', '{"formFile":"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml","hideFields":["default_con","checked_out","checked_out_time","version","xreference"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"], "displayLookup":[ {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ] }'),
(3, 'Newsfeed', 'com_newsfeeds.newsfeed', '{"special":{"dbtable":"#__newsfeeds","key":"id","type":"Newsfeed","prefix":"NewsfeedsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special":{"numarticles":"numarticles","cache_time":"cache_time","rtl":"rtl"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{"formFile":"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml","hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(4, 'User', 'com_users.user', '{"special":{"dbtable":"#__users","key":"id","type":"User","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"null","core_alias":"username","core_created_time":"registerdate","core_modified_time":"lastvisitDate","core_body":"null", "core_hits":"null","core_publish_up":"null","core_publish_down":"null","access":"null", "core_params":"params", "core_featured":"null", "core_metadata":"null", "core_language":"null", "core_images":"null", "core_urls":"null", "core_version":"null", "core_ordering":"null", "core_metakey":"null", "core_metadesc":"null", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{}}', 'UsersHelperRoute::getUserRoute', ''),
(5, 'Article Category', 'com_content.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContentHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(6, 'Contact Category', 'com_contact.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContactHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(7, 'Newsfeeds Category', 'com_newsfeeds.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(8, 'Tag', 'com_tags.tag', '{"special":{"dbtable":"#__tags","key":"tag_id","type":"Tag","prefix":"TagsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path"}}', 'TagsHelperRoute::getTagRoute', '{"formFile":"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml", "hideFields":["checked_out","checked_out_time","version", "lft", "rgt", "level", "path", "urls", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(9, 'Banner', 'com_banners.banner', '{"special":{"dbtable":"#__banners","key":"id","type":"Banner","prefix":"BannersTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"null","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"null", "asset_id":"null"}, "special":{"imptotal":"imptotal", "impmade":"impmade", "clicks":"clicks", "clickurl":"clickurl", "custombannercode":"custombannercode", "cid":"cid", "purchase_type":"purchase_type", "track_impressions":"track_impressions", "track_clicks":"track_clicks"}}', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml", "hideFields":["checked_out","checked_out_time","version", "reset"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "imptotal", "impmade", "reset"], "convertToInt":["publish_up", "publish_down", "ordering"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"cid","targetTable":"#__banner_clients","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(10, 'Banners Category', 'com_banners.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(11, 'Banner Client', 'com_banners.client', '{"special":{"dbtable":"#__banner_clients","key":"id","type":"Client","prefix":"BannersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml", "hideFields":["checked_out","checked_out_time"], "ignoreChanges":["checked_out", "checked_out_time"], "convertToInt":[], "displayLookup":[]}'),
(12, 'User Notes', 'com_users.note', '{"special":{"dbtable":"#__user_notes","key":"id","type":"Note","prefix":"UsersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml", "hideFields":["checked_out","checked_out_time", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(13, 'User Notes Category', 'com_users.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_core_log_searches`
--

CREATE TABLE IF NOT EXISTS `imwls_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_extensions`
--

CREATE TABLE IF NOT EXISTS `imwls_extensions` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `element` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) unsigned NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text NOT NULL,
  `params` text NOT NULL,
  `custom_data` text NOT NULL,
  `system_data` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`extension_id`),
  KEY `element_clientid` (`element`,`client_id`),
  KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  KEY `extension` (`type`,`element`,`folder`,`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10016 ;

--
-- Dumping data for table `imwls_extensions`
--

INSERT INTO `imwls_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{"name":"com_mailto","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MAILTO_XML_DESCRIPTION","group":"","filename":"mailto"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{"name":"com_wrapper","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_WRAPPER_XML_DESCRIPTION","group":"","filename":"wrapper"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{"name":"com_admin","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_ADMIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{"name":"com_banners","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_BANNERS_XML_DESCRIPTION","group":"","filename":"banners"}', '{"purchase_type":"3","track_impressions":"0","track_clicks":"0","metakey_prefix":"","save_history":"1","history_limit":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{"name":"com_cache","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CACHE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{"name":"com_categories","type":"component","creationDate":"December 2007","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{"name":"com_checkin","type":"component","creationDate":"Unknown","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CHECKIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{"name":"com_contact","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTACT_XML_DESCRIPTION","group":"","filename":"contact"}', '{"show_contact_category":"hide","save_history":"1","history_limit":10,"show_contact_list":"0","presentation_style":"sliders","show_name":"1","show_position":"1","show_email":"0","show_street_address":"1","show_suburb":"1","show_state":"1","show_postcode":"1","show_country":"1","show_telephone":"1","show_mobile":"1","show_fax":"1","show_webpage":"1","show_misc":"1","show_image":"1","image":"","allow_vcard":"0","show_articles":"0","show_profile":"0","show_links":"0","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","contact_icons":"0","icon_address":"","icon_email":"","icon_telephone":"","icon_mobile":"","icon_fax":"","icon_misc":"","show_headings":"1","show_position_headings":"1","show_email_headings":"0","show_telephone_headings":"1","show_mobile_headings":"0","show_fax_headings":"0","allow_vcard_headings":"0","show_suburb_headings":"1","show_state_headings":"1","show_country_headings":"1","show_email_form":"1","show_email_copy":"1","banned_email":"","banned_subject":"","banned_text":"","validate_session":"1","custom_reply":"0","redirect":"","show_category_crumb":"0","metakey":"","metadesc":"","robots":"","author":"","rights":"","xreference":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{"name":"com_cpanel","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CPANEL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{"name":"com_installer","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_INSTALLER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{"name":"com_languages","type":"component","creationDate":"2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LANGUAGES_XML_DESCRIPTION","group":""}', '{"administrator":"en-GB","site":"en-GB"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{"name":"com_login","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{"name":"com_media","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MEDIA_XML_DESCRIPTION","group":"","filename":"media"}', '{"upload_extensions":"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS","upload_maxsize":"10","file_path":"images","image_path":"images","restrict_uploads":"1","allowed_media_usergroup":"3","check_mime":"1","image_extensions":"bmp,gif,jpg,png","ignore_extensions":"","upload_mime":"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip","upload_mime_illegal":"text\\/html"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{"name":"com_menus","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MENUS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{"name":"com_messages","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MESSAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{"name":"com_modules","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MODULES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{"name":"com_newsfeeds","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '{"newsfeed_layout":"_:default","save_history":"1","history_limit":5,"show_feed_image":"1","show_feed_description":"1","show_item_description":"1","feed_character_count":"0","feed_display_order":"des","float_first":"right","float_second":"right","show_tags":"1","category_layout":"_:default","show_category_title":"1","show_description":"1","show_description_image":"1","maxLevel":"-1","show_empty_categories":"0","show_subcat_desc":"1","show_cat_items":"1","show_cat_tags":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_items_cat":"1","filter_field":"1","show_pagination_limit":"1","show_headings":"1","show_articles":"0","show_link":"1","show_pagination":"1","show_pagination_results":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{"name":"com_plugins","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_PLUGINS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{"name":"com_search","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_SEARCH_XML_DESCRIPTION","group":"","filename":"search"}', '{"enabled":"0","show_date":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{"name":"com_templates","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_TEMPLATES_XML_DESCRIPTION","group":""}', '{"template_positions_display":"0","upload_limit":"2","image_formats":"gif,bmp,jpg,jpeg,png","source_formats":"txt,less,ini,xml,js,php,css","font_formats":"woff,ttf,otf","compressed_formats":"zip"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{"name":"com_content","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '{"article_layout":"_:default","show_title":"1","link_titles":"1","show_intro":"1","info_block_position":"0","show_category":"1","link_category":"1","show_parent_category":"0","link_parent_category":"0","show_author":"1","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"1","show_item_navigation":"1","show_vote":"0","show_readmore":"1","show_readmore_title":"1","readmore_limit":"100","show_tags":"1","show_icons":"1","show_print_icon":"1","show_email_icon":"1","show_hits":"1","show_noauth":"0","urls_position":"0","show_publishing_options":"1","show_article_options":"1","save_history":"1","history_limit":10,"show_urls_images_frontend":"0","show_urls_images_backend":"1","targeta":0,"targetb":0,"targetc":0,"float_intro":"left","float_fulltext":"left","category_layout":"_:blog","show_category_heading_title_text":"1","show_category_title":"0","show_description":"0","show_description_image":"0","maxLevel":"1","show_empty_categories":"0","show_no_articles":"1","show_subcat_desc":"1","show_cat_num_articles":"0","show_cat_tags":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_num_articles_cat":"1","num_leading_articles":"1","num_intro_articles":"4","num_columns":"2","num_links":"4","multi_column_order":"0","show_subcategory_content":"0","show_pagination_limit":"1","filter_field":"hide","show_headings":"1","list_show_date":"0","date_format":"","list_show_hits":"1","list_show_author":"1","orderby_pri":"order","orderby_sec":"rdate","order_date":"published","show_pagination":"2","show_pagination_results":"1","show_featured":"show","show_feed_link":"1","feed_summary":"0","feed_show_readmore":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{"name":"com_config","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONFIG_XML_DESCRIPTION","group":""}', '{"filters":{"1":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"9":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"6":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"7":{"filter_type":"NONE","filter_tags":"","filter_attributes":""},"2":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"3":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"4":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"5":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"8":{"filter_type":"NONE","filter_tags":"","filter_attributes":""}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{"name":"com_redirect","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_REDIRECT_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{"name":"com_users","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_USERS_XML_DESCRIPTION","group":"","filename":"users"}', '{"allowUserRegistration":"0","new_usertype":"2","guest_usergroup":"9","sendpassword":"1","useractivation":"1","mail_to_admin":"0","captcha":"","frontend_userparams":"1","site_language":"0","change_login_name":"0","reset_count":"10","reset_time":"1","minimum_length":"4","minimum_integers":"0","minimum_symbols":"0","minimum_uppercase":"0","save_history":"1","history_limit":5,"mailSubjectPrefix":"","mailBodySuffix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{"name":"com_finder","type":"component","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_FINDER_XML_DESCRIPTION","group":"","filename":"finder"}', '{"show_description":"1","description_length":255,"allow_empty_query":"0","show_url":"1","show_advanced":"1","expand_advanced":"0","show_date_filters":"0","highlight_terms":"1","opensearch_name":"","opensearch_description":"","batch_size":"50","memory_table_limit":30000,"title_multiplier":"1.7","text_multiplier":"0.7","meta_multiplier":"1.2","path_multiplier":"2.0","misc_multiplier":"0.3","stemmer":"snowball"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{"name":"com_joomlaupdate","type":"component","creationDate":"February 2012","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{"name":"com_tags","type":"component","creationDate":"December 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"COM_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '{"tag_layout":"_:default","save_history":"1","history_limit":5,"show_tag_title":"0","tag_list_show_tag_image":"0","tag_list_show_tag_description":"0","tag_list_image":"","show_tag_num_items":"0","tag_list_orderby":"title","tag_list_orderby_direction":"ASC","show_headings":"0","tag_list_show_date":"0","tag_list_show_item_image":"0","tag_list_show_item_description":"0","tag_list_item_maximum_characters":0,"return_any_or_all":"1","include_children":"0","maximum":200,"tag_list_language_filter":"all","tags_layout":"_:default","all_tags_orderby":"title","all_tags_orderby_direction":"ASC","all_tags_show_tag_image":"0","all_tags_show_tag_descripion":"0","all_tags_tag_maximum_characters":20,"all_tags_show_tag_hits":"0","filter_field":"1","show_pagination_limit":"1","show_pagination":"2","show_pagination_results":"1","tag_field_ajax_mode":"1","show_feed_link":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{"name":"com_contenthistory","type":"component","creationDate":"May 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_CONTENTHISTORY_XML_DESCRIPTION","group":"","filename":"contenthistory"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 0, '{"name":"com_ajax","type":"component","creationDate":"August 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_AJAX_XML_DESCRIPTION","group":"","filename":"ajax"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{"name":"com_postinstall","type":"component","creationDate":"September 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_POSTINSTALL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(101, 'SimplePie', 'library', 'simplepie', '', 0, 1, 1, 1, '{"name":"SimplePie","type":"library","creationDate":"2004","author":"SimplePie","copyright":"Copyright (c) 2004-2009, Ryan Parman and Geoffrey Sneddon","authorEmail":"","authorUrl":"http:\\/\\/simplepie.org\\/","version":"1.2","description":"LIB_SIMPLEPIE_XML_DESCRIPTION","group":"","filename":"simplepie"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 'phputf8', 'library', 'phputf8', '', 0, 1, 1, 1, '{"name":"phputf8","type":"library","creationDate":"2006","author":"Harry Fuecks","copyright":"Copyright various authors","authorEmail":"hfuecks@gmail.com","authorUrl":"http:\\/\\/sourceforge.net\\/projects\\/phputf8","version":"0.5","description":"LIB_PHPUTF8_XML_DESCRIPTION","group":"","filename":"phputf8"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 'Joomla! Platform', 'library', 'joomla', '', 0, 1, 1, 1, '{"name":"Joomla! Platform","type":"library","creationDate":"2008","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"http:\\/\\/www.joomla.org","version":"13.1","description":"LIB_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '{"mediaversion":"e03d86d92713e2ee0a2aa2f42f8595b9"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 'IDNA Convert', 'library', 'idna_convert', '', 0, 1, 1, 1, '{"name":"IDNA Convert","type":"library","creationDate":"2004","author":"phlyLabs","copyright":"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de","authorEmail":"phlymail@phlylabs.de","authorUrl":"http:\\/\\/phlylabs.de","version":"0.8.0","description":"LIB_IDNA_XML_DESCRIPTION","group":"","filename":"idna_convert"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{"name":"FOF","type":"library","creationDate":"2015-03-11 11:59:00","author":"Nicholas K. Dionysopoulos \\/ Akeeba Ltd","copyright":"(C)2011-2015 Nicholas K. Dionysopoulos","authorEmail":"nicholas@akeebabackup.com","authorUrl":"https:\\/\\/www.akeebabackup.com","version":"2.4.2","description":"LIB_FOF_XML_DESCRIPTION","group":"","filename":"fof"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 'PHPass', 'library', 'phpass', '', 0, 1, 1, 1, '{"name":"PHPass","type":"library","creationDate":"2004-2006","author":"Solar Designer","copyright":"","authorEmail":"solar@openwall.com","authorUrl":"http:\\/\\/www.openwall.com\\/phpass\\/","version":"0.3","description":"LIB_PHPASS_XML_DESCRIPTION","group":"","filename":"phpass"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{"name":"mod_articles_archive","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION","group":"","filename":"mod_articles_archive"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{"name":"mod_articles_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_NEWS_XML_DESCRIPTION","group":"","filename":"mod_articles_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{"name":"mod_articles_popular","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_articles_popular"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{"name":"mod_banners","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BANNERS_XML_DESCRIPTION","group":"","filename":"mod_banners"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{"name":"mod_breadcrumbs","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BREADCRUMBS_XML_DESCRIPTION","group":"","filename":"mod_breadcrumbs"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":"","filename":"mod_custom"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":"","filename":"mod_feed"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{"name":"mod_footer","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FOOTER_XML_DESCRIPTION","group":"","filename":"mod_footer"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":"","filename":"mod_login"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{"name":"mod_menu","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":"","filename":"mod_menu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{"name":"mod_articles_news","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_NEWS_XML_DESCRIPTION","group":"","filename":"mod_articles_news"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{"name":"mod_random_image","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RANDOM_IMAGE_XML_DESCRIPTION","group":"","filename":"mod_random_image"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{"name":"mod_related_items","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RELATED_XML_DESCRIPTION","group":"","filename":"mod_related_items"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{"name":"mod_search","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SEARCH_XML_DESCRIPTION","group":"","filename":"mod_search"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{"name":"mod_stats","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":"","filename":"mod_stats"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{"name":"mod_syndicate","type":"module","creationDate":"May 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SYNDICATE_XML_DESCRIPTION","group":"","filename":"mod_syndicate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{"name":"mod_users_latest","type":"module","creationDate":"December 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_USERS_LATEST_XML_DESCRIPTION","group":"","filename":"mod_users_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{"name":"mod_whosonline","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WHOSONLINE_XML_DESCRIPTION","group":"","filename":"mod_whosonline"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{"name":"mod_wrapper","type":"module","creationDate":"October 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WRAPPER_XML_DESCRIPTION","group":"","filename":"mod_wrapper"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{"name":"mod_articles_category","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION","group":"","filename":"mod_articles_category"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{"name":"mod_articles_categories","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION","group":"","filename":"mod_articles_categories"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{"name":"mod_languages","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LANGUAGES_XML_DESCRIPTION","group":"","filename":"mod_languages"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{"name":"mod_finder","type":"module","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FINDER_XML_DESCRIPTION","group":"","filename":"mod_finder"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":"","filename":"mod_custom"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":"","filename":"mod_feed"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{"name":"mod_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_XML_DESCRIPTION","group":"","filename":"mod_latest"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{"name":"mod_logged","type":"module","creationDate":"January 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGGED_XML_DESCRIPTION","group":"","filename":"mod_logged"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"March 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":"","filename":"mod_login"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{"name":"mod_menu","type":"module","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":"","filename":"mod_menu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{"name":"mod_popular","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_popular"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{"name":"mod_quickicon","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_QUICKICON_XML_DESCRIPTION","group":"","filename":"mod_quickicon"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{"name":"mod_status","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATUS_XML_DESCRIPTION","group":"","filename":"mod_status"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{"name":"mod_submenu","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SUBMENU_XML_DESCRIPTION","group":"","filename":"mod_submenu"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{"name":"mod_title","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TITLE_XML_DESCRIPTION","group":"","filename":"mod_title"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{"name":"mod_toolbar","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TOOLBAR_XML_DESCRIPTION","group":"","filename":"mod_toolbar"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{"name":"mod_multilangstatus","type":"module","creationDate":"September 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MULTILANGSTATUS_XML_DESCRIPTION","group":"","filename":"mod_multilangstatus"}', '{"cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{"name":"mod_version","type":"module","creationDate":"January 2012","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_VERSION_XML_DESCRIPTION","group":"","filename":"mod_version"}', '{"format":"short","product":"1","cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{"name":"mod_stats_admin","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":"","filename":"mod_stats_admin"}', '{"serverinfo":"0","siteinfo":"0","counter":"0","increase":"0","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{"name":"mod_tags_popular","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_POPULAR_XML_DESCRIPTION","group":"","filename":"mod_tags_popular"}', '{"maximum":"5","timeframe":"alltime","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{"name":"mod_tags_similar","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_SIMILAR_XML_DESCRIPTION","group":"","filename":"mod_tags_similar"}', '{"maximum":"5","matchtype":"any","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_gmail","type":"plugin","creationDate":"February 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_GMAIL_XML_DESCRIPTION","group":"","filename":"gmail"}', '{"applysuffix":"0","suffix":"","verifypeer":"1","user_blacklist":""}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{"name":"plg_authentication_joomla","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_ldap","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LDAP_XML_DESCRIPTION","group":"","filename":"ldap"}', '{"host":"","port":"389","use_ldapV3":"0","negotiate_tls":"0","no_referrals":"0","auth_method":"bind","base_dn":"","search_string":"","users_dn":"","username":"admin","password":"bobby7","ldap_fullname":"fullName","ldap_email":"mail","ldap_uid":"uid"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(403, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{"name":"plg_content_contact","type":"plugin","creationDate":"January 2014","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.2","description":"PLG_CONTENT_CONTACT_XML_DESCRIPTION","group":"","filename":"contact"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(404, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{"name":"plg_content_emailcloak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION","group":"","filename":"emailcloak"}', '{"mode":"1"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{"name":"plg_content_loadmodule","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOADMODULE_XML_DESCRIPTION","group":"","filename":"loadmodule"}', '{"style":"xhtml"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagebreak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION","group":"","filename":"pagebreak"}', '{"title":"1","multipage_toc":"1","showall":"1"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagenavigation","type":"plugin","creationDate":"January 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_PAGENAVIGATION_XML_DESCRIPTION","group":"","filename":"pagenavigation"}', '{"position":"1"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 0, '{"name":"plg_content_vote","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_VOTE_XML_DESCRIPTION","group":"","filename":"vote"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_codemirror","type":"plugin","creationDate":"28 March 2011","author":"Marijn Haverbeke","copyright":"Copyright (C) 2014 by Marijn Haverbeke <marijnh@gmail.com> and others","authorEmail":"marijnh@gmail.com","authorUrl":"http:\\/\\/codemirror.net\\/","version":"5.0","description":"PLG_CODEMIRROR_XML_DESCRIPTION","group":"","filename":"codemirror"}', '{"lineNumbers":"1","lineWrapping":"1","matchTags":"1","matchBrackets":"1","marker-gutter":"1","autoCloseTags":"1","autoCloseBrackets":"1","autoFocus":"1","theme":"default","tabmode":"indent"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_none","type":"plugin","creationDate":"September 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_NONE_XML_DESCRIPTION","group":"","filename":"none"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{"name":"plg_editors_tinymce","type":"plugin","creationDate":"2005-2014","author":"Moxiecode Systems AB","copyright":"Moxiecode Systems AB","authorEmail":"N\\/A","authorUrl":"tinymce.moxiecode.com","version":"4.1.7","description":"PLG_TINY_XML_DESCRIPTION","group":"","filename":"tinymce"}', '{"mode":"1","skin":"0","mobile":"0","entity_encoding":"raw","lang_mode":"1","text_direction":"ltr","content_css":"1","content_css_custom":"","relative_urls":"1","newlines":"0","invalid_elements":"script,applet,iframe","extended_elements":"","html_height":"550","html_width":"750","resizing":"1","element_path":"1","fonts":"1","paste":"1","searchreplace":"1","insertdate":"1","colors":"1","table":"1","smilies":"1","hr":"1","link":"1","media":"1","print":"1","directionality":"1","fullscreen":"1","alignment":"1","visualchars":"1","visualblocks":"1","nonbreaking":"1","template":"1","blockquote":"1","wordcount":"1","advlist":"1","autosave":"1","contextmenu":"1","inlinepopups":"1","custom_plugin":"","custom_button":""}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_article","type":"plugin","creationDate":"October 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_ARTICLE_XML_DESCRIPTION","group":"","filename":"article"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_image","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_IMAGE_XML_DESCRIPTION","group":"","filename":"image"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_pagebreak","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION","group":"","filename":"pagebreak"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_readmore","type":"plugin","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_READMORE_XML_DESCRIPTION","group":"","filename":"readmore"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0);
INSERT INTO `imwls_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(417, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{"name":"plg_search_categories","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION","group":"","filename":"categories"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{"name":"plg_search_contacts","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTACTS_XML_DESCRIPTION","group":"","filename":"contacts"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{"name":"plg_search_content","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{"name":"plg_search_newsfeeds","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{"name":"plg_system_languagefilter","type":"plugin","creationDate":"July 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION","group":"","filename":"languagefilter"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 0, 1, 0, '{"name":"plg_system_p3p","type":"plugin","creationDate":"September 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_P3P_XML_DESCRIPTION","group":"","filename":"p3p"}', '{"headers":"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{"name":"plg_system_cache","type":"plugin","creationDate":"February 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CACHE_XML_DESCRIPTION","group":"","filename":"cache"}', '{"browsercache":"0","cachetime":"15"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{"name":"plg_system_debug","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_DEBUG_XML_DESCRIPTION","group":"","filename":"debug"}', '{"profile":"1","queries":"1","memory":"1","language_files":"1","language_strings":"1","strip-first":"1","strip-prefix":"","strip-suffix":""}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{"name":"plg_system_log","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOG_XML_DESCRIPTION","group":"","filename":"log"}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{"name":"plg_system_redirect","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_REDIRECT_XML_DESCRIPTION","group":"","filename":"redirect"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{"name":"plg_system_remember","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_REMEMBER_XML_DESCRIPTION","group":"","filename":"remember"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{"name":"plg_system_sef","type":"plugin","creationDate":"December 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEF_XML_DESCRIPTION","group":"","filename":"sef"}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{"name":"plg_system_logout","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION","group":"","filename":"logout"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{"name":"plg_user_contactcreator","type":"plugin","creationDate":"August 2009","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTACTCREATOR_XML_DESCRIPTION","group":"","filename":"contactcreator"}', '{"autowebpage":"","category":"34","autopublish":"0"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{"name":"plg_user_joomla","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '{"autoregister":"1","mail_to_user":"1","forceLogout":"1"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{"name":"plg_user_profile","type":"plugin","creationDate":"January 2008","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_PROFILE_XML_DESCRIPTION","group":"","filename":"profile"}', '{"register-require_address1":"1","register-require_address2":"1","register-require_city":"1","register-require_region":"1","register-require_country":"1","register-require_postal_code":"1","register-require_phone":"1","register-require_website":"1","register-require_favoritebook":"1","register-require_aboutme":"1","register-require_tos":"1","register-require_dob":"1","profile-require_address1":"1","profile-require_address2":"1","profile-require_city":"1","profile-require_region":"1","profile-require_country":"1","profile-require_postal_code":"1","profile-require_phone":"1","profile-require_website":"1","profile-require_favoritebook":"1","profile-require_aboutme":"1","profile-require_tos":"1","profile-require_dob":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{"name":"plg_extension_joomla","type":"plugin","creationDate":"May 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{"name":"plg_content_joomla","type":"plugin","creationDate":"November 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_JOOMLA_XML_DESCRIPTION","group":"","filename":"joomla"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{"name":"plg_system_languagecode","type":"plugin","creationDate":"November 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION","group":"","filename":"languagecode"}', '', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_joomlaupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION","group":"","filename":"joomlaupdate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_extensionupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION","group":"","filename":"extensionupdate"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{"name":"plg_captcha_recaptcha","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.0","description":"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION","group":"","filename":"recaptcha"}', '{"public_key":"","private_key":"","theme":"clean"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{"name":"plg_system_highlight","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION","group":"","filename":"highlight"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{"name":"plg_content_finder","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_FINDER_XML_DESCRIPTION","group":"","filename":"finder"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_categories","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CATEGORIES_XML_DESCRIPTION","group":"","filename":"categories"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_contacts","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTACTS_XML_DESCRIPTION","group":"","filename":"contacts"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_content","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTENT_XML_DESCRIPTION","group":"","filename":"content"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_newsfeeds","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION","group":"","filename":"newsfeeds"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(447, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_tags","type":"plugin","creationDate":"February 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_totp","type":"plugin","creationDate":"August 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION","group":"","filename":"totp"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{"name":"plg_authentication_cookie","type":"plugin","creationDate":"July 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_COOKIE_XML_DESCRIPTION","group":"","filename":"cookie"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_yubikey","type":"plugin","creationDate":"September 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION","group":"","filename":"yubikey"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 1, 1, 0, '{"name":"plg_search_tags","type":"plugin","creationDate":"March 2014","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_TAGS_XML_DESCRIPTION","group":"","filename":"tags"}', '{"search_limit":"50","show_tagged_items":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{"name":"beez3","type":"template","creationDate":"25 November 2009","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"3.1.0","description":"TPL_BEEZ3_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","templatecolor":"nature"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{"name":"hathor","type":"template","creationDate":"May 2010","author":"Andrea Tarr","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"hathor@tarrconsulting.com","authorUrl":"http:\\/\\/www.tarrconsulting.com","version":"3.0.0","description":"TPL_HATHOR_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"showSiteName":"0","colourChoice":"0","boldText":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(506, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{"name":"protostar","type":"template","creationDate":"4\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_PROTOSTAR_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{"name":"isis","type":"template","creationDate":"3\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_ISIS_XML_DESCRIPTION","group":"","filename":"templateDetails"}', '{"templateColor":"","logoFile":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{"name":"English (en-GB)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.1","description":"en-GB site language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{"name":"English (en-GB)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2015 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.1","description":"en-GB administrator language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{"name":"files_joomla","type":"file","creationDate":"March 2015","author":"Joomla! Project","copyright":"(C) 2005 - 2015 Open Source Matters. All rights reserved","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.4.1","description":"FILES_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10011, 'COM_COMMENT', 'component', 'com_comment', '', 1, 1, 0, 0, '{"name":"COM_COMMENT","type":"component","creationDate":"2015-02-02","author":"Compojoom.com","copyright":"(C) 2010 Daniel Dimitrov","authorEmail":"contact-us@compojoom.com","authorUrl":"https:\\/\\/compojoom.com","version":"5.3.1","description":"","group":"","filename":"comment"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10012, 'Content - CComment', 'plugin', 'joscomment', 'content', 0, 1, 1, 0, '{"name":"Content - CComment","type":"plugin","creationDate":"2015-02-02","author":"Daniel Dimitrov ","copyright":"Copyright(C) Daniel Dimitrov All rights reserved!","authorEmail":"services@compojoom.com","authorUrl":"http:\\/\\/compojoom.com","version":"5.3.1","description":"You need to enable this plugin if you want to have comments in com_content or in virtuemart(com_virtuemart)","group":"","filename":"joscomment"}', '{"support_com_content":"1","on_content_prepare":"0","support_com_virtuemart":"0","support_com_matukio":"0","printView":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10013, 'K2 Plugin - CComment', 'plugin', 'ccomment', 'k2', 0, 0, 1, 0, '{"name":"K2 Plugin - CComment","type":"plugin","creationDate":"2015-02-02","author":"Compojoom","copyright":"Copyright (c) 2008 - 2010 Compojoom.com. All rights reserved.","authorEmail":"services@compojoom.com","authorUrl":"www.compojoom.com","version":"5.3.1","description":"A K2 plugin to allow comments with compojoomcomment","group":"","filename":"ccomment"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10014, 'Search - CComment', 'plugin', 'ccomment', 'search', 0, 0, 1, 0, '{"name":"Search - CComment","type":"plugin","creationDate":"2015-02-02","author":"Daniel Dimitrov","copyright":"Copyright (C) 2008 - 2013 compojoom.com. All rights reserved.","authorEmail":"daniel@compojoom.com","authorUrl":"https:\\/\\/compojoom.com","version":"5.3.1","description":"This plugin enables you to search comments through joomla''s search","group":"","filename":"ccomment"}', '{"search_limit":"50"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10015, 'Library - compojoom', 'library', 'compojoom', '', 0, 1, 1, 0, '{"name":"Library - compojoom","type":"library","creationDate":"2015-02-02","author":"compojoom.com","copyright":"(C) 2008-2014 Daniel Dimitrov and Yves Hoppe","authorEmail":"daniel@compojoom.com","authorUrl":"https:\\/\\/compojoom.com","version":"4.0.31","description":"LIB_COMPOJOOM_DESC","group":"","filename":"compojoom"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_filters`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_filters` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext,
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double unsigned NOT NULL DEFAULT '0',
  `sale_price` double unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL,
  PRIMARY KEY (`link_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_title` (`title`),
  KEY `idx_md5` (`md5sum`),
  KEY `idx_url` (`url`(75)),
  KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_terms0`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_terms0` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_terms1`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_terms1` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_terms2`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_terms2` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_terms3`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_terms3` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_terms4`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_terms4` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_terms5`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_terms5` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_terms6`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_terms6` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_terms7`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_terms7` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_terms8`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_terms8` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_terms9`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_terms9` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_termsa`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_termsa` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_termsb`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_termsb` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_termsc`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_termsc` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_termsd`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_termsd` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_termse`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_termse` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_links_termsf`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_links_termsf` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_taxonomy`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_taxonomy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `access` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `state` (`state`),
  KEY `ordering` (`ordering`),
  KEY `access` (`access`),
  KEY `idx_parent_published` (`parent_id`,`state`,`access`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `imwls_finder_taxonomy`
--

INSERT INTO `imwls_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_taxonomy_map`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_taxonomy_map` (
  `link_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`node_id`),
  KEY `link_id` (`link_id`),
  KEY `node_id` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_terms`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_terms` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `idx_term` (`term`),
  KEY `idx_term_phrase` (`term`,`phrase`),
  KEY `idx_stem_phrase` (`stem`,`phrase`),
  KEY `idx_soundex_phrase` (`soundex`,`phrase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_terms_common`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL,
  KEY `idx_word_lang` (`term`,`language`),
  KEY `idx_lang` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imwls_finder_terms_common`
--

INSERT INTO `imwls_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('ani', 'en'),
('any', 'en'),
('are', 'en'),
('aren''t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn''t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('noth', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('onli', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('veri', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('whi', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_tokens`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '1',
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT '',
  KEY `idx_word` (`term`),
  KEY `idx_context` (`context`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_tokens_aggregate`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_tokens_aggregate` (
  `term_id` int(10) unsigned NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `term_weight` float unsigned NOT NULL,
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `context_weight` float unsigned NOT NULL,
  `total_weight` float unsigned NOT NULL,
  `language` char(3) NOT NULL DEFAULT '',
  KEY `token` (`term`),
  KEY `keyword_id` (`term_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_finder_types`
--

CREATE TABLE IF NOT EXISTS `imwls_finder_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_komento_acl`
--

CREATE TABLE IF NOT EXISTS `imwls_komento_acl` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cid` varchar(255) NOT NULL,
  `component` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `rules` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `komento_acl_content_type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `imwls_komento_acl`
--

INSERT INTO `imwls_komento_acl` (`id`, `cid`, `component`, `type`, `rules`) VALUES
(1, '1', 'com_content', 'usergroup', '{"read_comment":true,"read_stickies":true,"read_lovies":true,"add_comment":true,"like_comment":false,"report_comment":false,"share_comment":true,"reply_comment":false,"upload_attachment":false,"download_attachment":true,"edit_own_comment":false,"delete_own_comment":false,"delete_own_attachment":false,"author_edit_comment":false,"author_delete_comment":false,"author_publish_comment":false,"author_unpublish_comment":false,"author_stick_comment":false,"author_delete_attachment":false,"edit_all_comment":false,"delete_all_comment":false,"publish_all_comment":false,"unpublish_all_comment":false,"stick_all_comment":false,"delete_all_attachment":false}'),
(2, '9', 'com_content', 'usergroup', '{"read_comment":true,"read_stickies":true,"read_lovies":true,"add_comment":true,"like_comment":false,"report_comment":false,"share_comment":true,"reply_comment":false,"upload_attachment":false,"download_attachment":true,"edit_own_comment":false,"delete_own_comment":false,"delete_own_attachment":false,"author_edit_comment":false,"author_delete_comment":false,"author_publish_comment":false,"author_unpublish_comment":false,"author_stick_comment":false,"author_delete_attachment":false,"edit_all_comment":false,"delete_all_comment":false,"publish_all_comment":false,"unpublish_all_comment":false,"stick_all_comment":false,"delete_all_attachment":false}'),
(3, '2', 'com_content', 'usergroup', '{"read_comment":true,"read_stickies":true,"read_lovies":true,"add_comment":true,"like_comment":true,"report_comment":true,"share_comment":true,"reply_comment":true,"upload_attachment":true,"download_attachment":true,"edit_own_comment":false,"delete_own_comment":false,"delete_own_attachment":false,"author_edit_comment":false,"author_delete_comment":false,"author_publish_comment":false,"author_unpublish_comment":false,"author_stick_comment":false,"author_delete_attachment":false,"edit_all_comment":false,"delete_all_comment":false,"publish_all_comment":false,"unpublish_all_comment":false,"stick_all_comment":false,"delete_all_attachment":false}'),
(4, '6', 'com_content', 'usergroup', '{"read_comment":true,"read_stickies":true,"read_lovies":true,"add_comment":true,"like_comment":true,"report_comment":true,"share_comment":true,"reply_comment":true,"upload_attachment":true,"download_attachment":true,"edit_own_comment":true,"delete_own_comment":true,"delete_own_attachment":true,"author_edit_comment":false,"author_delete_comment":false,"author_publish_comment":false,"author_unpublish_comment":false,"author_stick_comment":false,"author_delete_attachment":false,"edit_all_comment":false,"delete_all_comment":false,"publish_all_comment":false,"unpublish_all_comment":false,"stick_all_comment":false,"delete_all_attachment":false}'),
(5, '7', 'com_content', 'usergroup', '{"read_comment":true,"read_stickies":true,"read_lovies":true,"add_comment":true,"like_comment":true,"report_comment":true,"share_comment":true,"reply_comment":true,"upload_attachment":true,"download_attachment":true,"edit_own_comment":true,"delete_own_comment":true,"delete_own_attachment":true,"author_edit_comment":false,"author_delete_comment":false,"author_publish_comment":false,"author_unpublish_comment":false,"author_stick_comment":false,"author_delete_attachment":false,"edit_all_comment":true,"delete_all_comment":true,"publish_all_comment":true,"unpublish_all_comment":true,"stick_all_comment":true,"delete_all_attachment":true}'),
(6, '3', 'com_content', 'usergroup', '{"read_comment":true,"read_stickies":true,"read_lovies":true,"add_comment":true,"like_comment":true,"report_comment":true,"share_comment":true,"reply_comment":true,"upload_attachment":true,"download_attachment":true,"edit_own_comment":true,"delete_own_comment":false,"delete_own_attachment":false,"author_edit_comment":true,"author_delete_comment":true,"author_publish_comment":true,"author_unpublish_comment":true,"author_stick_comment":true,"author_delete_attachment":true,"edit_all_comment":false,"delete_all_comment":false,"publish_all_comment":false,"unpublish_all_comment":false,"stick_all_comment":false,"delete_all_attachment":false}'),
(7, '4', 'com_content', 'usergroup', '{"read_comment":true,"read_stickies":true,"read_lovies":true,"add_comment":true,"like_comment":true,"report_comment":true,"share_comment":true,"reply_comment":true,"upload_attachment":true,"download_attachment":true,"edit_own_comment":true,"delete_own_comment":false,"delete_own_attachment":false,"author_edit_comment":true,"author_delete_comment":true,"author_publish_comment":true,"author_unpublish_comment":true,"author_stick_comment":true,"author_delete_attachment":true,"edit_all_comment":false,"delete_all_comment":false,"publish_all_comment":false,"unpublish_all_comment":false,"stick_all_comment":false,"delete_all_attachment":false}'),
(8, '5', 'com_content', 'usergroup', '{"read_comment":true,"read_stickies":true,"read_lovies":true,"add_comment":true,"like_comment":true,"report_comment":true,"share_comment":true,"reply_comment":true,"upload_attachment":true,"download_attachment":true,"edit_own_comment":true,"delete_own_comment":false,"delete_own_attachment":false,"author_edit_comment":true,"author_delete_comment":true,"author_publish_comment":true,"author_unpublish_comment":true,"author_stick_comment":true,"author_delete_attachment":true,"edit_all_comment":false,"delete_all_comment":false,"publish_all_comment":true,"unpublish_all_comment":true,"stick_all_comment":true,"delete_all_attachment":false}'),
(9, '8', 'com_content', 'usergroup', '{"read_comment":true,"read_stickies":true,"read_lovies":true,"add_comment":true,"like_comment":true,"report_comment":true,"share_comment":true,"reply_comment":true,"upload_attachment":true,"download_attachment":true,"edit_own_comment":true,"delete_own_comment":true,"delete_own_attachment":true,"author_edit_comment":true,"author_delete_comment":true,"author_publish_comment":true,"author_unpublish_comment":true,"author_stick_comment":true,"author_delete_attachment":true,"edit_all_comment":true,"delete_all_comment":true,"publish_all_comment":true,"unpublish_all_comment":true,"stick_all_comment":true,"delete_all_attachment":true}');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_komento_actions`
--

CREATE TABLE IF NOT EXISTS `imwls_komento_actions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL,
  `action_by` bigint(20) unsigned NOT NULL DEFAULT '0',
  `actioned` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `komento_actions` (`type`,`comment_id`,`action_by`),
  KEY `komento_actions_comment_id` (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_komento_activities`
--

CREATE TABLE IF NOT EXISTS `imwls_komento_activities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `comment_id` bigint(20) NOT NULL,
  `uid` bigint(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_komento_captcha`
--

CREATE TABLE IF NOT EXISTS `imwls_komento_captcha` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response` varchar(5) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_komento_comments`
--

CREATE TABLE IF NOT EXISTS `imwls_komento_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `component` varchar(255) NOT NULL,
  `cid` bigint(20) unsigned NOT NULL,
  `comment` text,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT '',
  `url` varchar(255) DEFAULT '',
  `ip` varchar(255) DEFAULT '',
  `created_by` bigint(20) unsigned DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) unsigned DEFAULT '0',
  `modified` datetime DEFAULT '0000-00-00 00:00:00',
  `deleted_by` bigint(20) unsigned DEFAULT '0',
  `deleted` datetime DEFAULT '0000-00-00 00:00:00',
  `flag` tinyint(1) DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `publish_up` datetime DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime DEFAULT '0000-00-00 00:00:00',
  `sticked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sent` tinyint(1) DEFAULT '0',
  `parent_id` int(11) unsigned DEFAULT '0',
  `lft` int(11) unsigned NOT NULL DEFAULT '0',
  `rgt` int(11) unsigned NOT NULL DEFAULT '0',
  `depth` int(11) unsigned NOT NULL DEFAULT '0',
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `address` text,
  `params` text,
  `ratings` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `komento_threaded` (`component`,`cid`,`published`,`lft`,`rgt`),
  KEY `komento_threaded_reverse` (`component`,`cid`,`published`,`rgt`),
  KEY `komento_module_comments` (`component`,`cid`,`published`,`created`),
  KEY `komento_backend` (`parent_id`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_komento_configs`
--

CREATE TABLE IF NOT EXISTS `imwls_komento_configs` (
  `component` varchar(255) NOT NULL,
  `params` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imwls_komento_configs`
--

INSERT INTO `imwls_komento_configs` (`component`, `params`) VALUES
('com_komento_comments_columns', '{"column_comment":1,"column_published":1,"column_sticked":1,"column_link":1,"column_edit":1,"column_component":1,"column_article":1,"column_date":1,"column_author":1,"column_id":1}'),
('com_komento_pending_columns', '{"column_comment":1,"column_published":1,"column_link":1,"column_edit":1,"column_component":1,"column_article":1,"column_date":1,"column_author":1,"column_id":1}'),
('com_komento_reports_columns', '{"column_comment":1,"column_published":1,"column_link":1,"column_edit":1,"column_component":1,"column_article":1,"column_date":1,"column_author":1,"column_id":1}'),
('com_komento', '{"database_clearcaptchaonpageload":"1","componentSelection":"all","articleSelection":"all","profile_enable":"1","profile_tab_comments":"1","profile_tab_activities":"1","profile_tab_popular":"1","profile_tab_sticked":"1","name_type":"default","layout_avatar_integration":"gravatar","layout_phpbb_path":"","layout_phpbb_url":"","profile_activities_comments":"1","profile_activities_replies":"1","profile_activities_likes":"1","enable_schema":"1","komento_environment":"static","komento_mode":"compressed"}'),
('com_content', '{"enable_komento":"1","disable_komento_on_tmpl_component":"0","allowed_categories_mode":"0","enable_orphanitem_convert":"1","orphanitem_ownership":"0","enable_login_form":"0","login_provider":"joomla","enable_moderation":"1","requires_moderation":["1"],"subscription_auto":"0","subscription_confirmation":"0","pagebreak_load":"last","antispam_akismet":"0","antispam_akismet_key":"","antispam_akismet_trackback":"0","antispam_flood_control":"0","antispam_flood_interval":"10","antispam_min_length_enable":"0","antispam_min_length":"10","antispam_max_length_enable":"0","antispam_max_length":"300","filter_word":"0","filter_word_text":"","blacklist_ip":"","antispam_captcha_enable":"0","antispam_captcha_type":"0","show_captcha":["1"],"antispam_recaptcha_ssl":"0","antispam_recaptcha_public_key":"","antispam_recaptcha_private_key":"","antispam_recaptcha_theme":"clean","antispam_recaptcha_lang":"en","layout_theme":"kuro","enable_responsive":"1","tabbed_comments":"1","max_threaded_level":"5","enable_threaded":"1","thread_indentation":"60","show_sort_buttons":"1","default_sort":"oldest","load_previous":"1","max_comments_per_page":"10","layout_template_override":"1","layout_component_override":"1","layout_inherit_kuro_css":"1","layout_css_admin":"kmt-comment-item-admin","layout_css_registered":"kmt-comment-item-registered","layout_css_author":"kmt-comment-item-author","layout_css_public":"kmt-comment-item-public","name_type":"default","guest_label":"0","auto_hyperlink":"1","links_nofollow":"0","datetime_permalink":"0","date_format":"l, M j Y g:i:sa","max_image_width":"300","max_image_height":"300","allow_video":"1","bbcode_video_width":"350","bbcode_video_height":"350","layout_frontpage_comment":"1","layout_frontpage_readmore":"1","layout_frontpage_readmore_use_joomla":"0","layout_frontpage_hits":"1","layout_frontpage_alignment":"right","enable_lapsed_time":"1","layout_avatar_enable":"1","enable_guest_link":"1","enable_permalink":"1","enable_share":"1","enable_likes":"1","enable_reply":"1","enable_report":"1","enable_location":"1","enable_info":"1","enable_syntax_highlighting":"1","enable_id":"0","enable_reply_reference":"1","enable_rank_bar":"1","enable_live_notification":"1","live_notification_interval":"30","form_position":"0","form_toggle_button":"0","autohide_form_notification":"1","form_show_moderate_message":"1","scroll_to_comment":"1","show_location":"1","enable_bbcode":"1","enable_subscription":"1","show_tnc":["1"],"tnc_text":"Before submitting the comment, you agree that:\\r\\n\\r\\na. To accept full responsibility for the comment that you submit.\\r\\nb. To use this function only for lawful purposes.\\r\\nc. Not to post defamatory, abusive, offensive, racist, sexist, threatening, vulgar, obscene, hateful or otherwise inappropriate comments, or to post comments which will constitute a criminal offense or give rise to civil liability.\\r\\nd. Not to post or make available any material which is protected by copyright, trade mark or other proprietary right without the express permission of the owner of the copyright, trade mark or any other proprietary right.\\r\\ne. To evaluate for yourself the accuracy of any opinion, advice or other content.","show_name":"1","show_email":"1","show_website":"1","require_name":"1","require_email":"0","require_website":"0","enable_email_regex":"1","email_regex":["%5CS%2B%40%5CS%2B"],"enable_website_regex":"1","website_regex":["%28http%3A%2F%2F%7Cftp%3A%2F%2F%7Cwww%29%5CS%2B"],"layout_avatar_integration":"gravatar","use_komento_profile":"1","easysocial_profile_popbox":"0","gravatar_default_avatar":"mm","layout_phpbb_path":"","layout_phpbb_url":"","bbcode_bold":"1","bbcode_italic":"1","bbcode_underline":"1","bbcode_link":"1","bbcode_picture":"1","bbcode_video":"1","bbcode_bulletlist":"1","bbcode_numericlist":"1","bbcode_bullet":"1","bbcode_quote":"1","bbcode_code":"1","bbcode_clean":"1","bbcode_smile":"1","bbcode_happy":"1","bbcode_surprised":"1","bbcode_tongue":"1","bbcode_unhappy":"1","bbcode_wink":"1","smileycode":[],"smileypath":[],"share_facebook":"1","share_twitter":"1","share_googleplus":"0","share_linkedin":"0","share_tumblr":"0","share_digg":"0","share_delicious":"0","share_reddit":"0","share_stumbleupon":"0","enable_conversation_bar":"1","conversation_bar_max_authors":"10","conversation_bar_include_guest":"0","enable_stickies":"1","max_stickies":"5","enable_lovies":"1","minimum_likes_lovies":"0","max_lovies":"5","syntaxhighlighter_theme":"default","notification_enable":"0","notification_sendmailonpageload":"0","notification_sendmailinhtml":"0","notification_event_new_comment":"1","notification_event_new_reply":"1","notification_event_new_pending":"1","notification_event_reported_comment":"1","notification_to_author":"1","notification_to_subscribers":"1","activities_comment":"1","activities_reply":"1","activities_like":"1","trigger_method":"component","target":"com_content","tnc":"","allowed_categories":[],"notification_to_usergroup_comment":[],"notification_to_usergroup_reply":[],"notification_to_usergroup_pending":[],"notification_to_usergroup_reported":[],"notification_es_to_usergroup_comment":[],"notification_es_to_usergroup_reply":[],"notification_es_to_usergroup_like":[]}');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_komento_hashkeys`
--

CREATE TABLE IF NOT EXISTS `imwls_komento_hashkeys` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `key` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_komento_ipfilter`
--

CREATE TABLE IF NOT EXISTS `imwls_komento_ipfilter` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `component` varchar(255) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `rules` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `komento_ipfilter` (`component`,`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_komento_mailq`
--

CREATE TABLE IF NOT EXISTS `imwls_komento_mailq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mailfrom` varchar(255) DEFAULT NULL,
  `fromname` varchar(255) DEFAULT NULL,
  `recipient` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `created` datetime NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'text',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `komento_mailq_status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_komento_subscription`
--

CREATE TABLE IF NOT EXISTS `imwls_komento_subscription` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `component` varchar(255) NOT NULL,
  `cid` bigint(20) unsigned NOT NULL,
  `userid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `komento_subscription` (`type`,`component`,`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_komento_uploads`
--

CREATE TABLE IF NOT EXISTS `imwls_komento_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `filename` text NOT NULL,
  `hashname` text NOT NULL,
  `path` text,
  `created` datetime NOT NULL,
  `created_by` bigint(20) unsigned DEFAULT '0',
  `published` tinyint(1) NOT NULL,
  `mime` text NOT NULL,
  `size` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_languages`
--

CREATE TABLE IF NOT EXISTS `imwls_languages` (
  `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` char(7) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_native` varchar(50) NOT NULL,
  `sef` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` varchar(512) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `sitename` varchar(1024) NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `idx_sef` (`sef`),
  UNIQUE KEY `idx_image` (`image`),
  UNIQUE KEY `idx_langcode` (`lang_code`),
  KEY `idx_access` (`access`),
  KEY `idx_ordering` (`ordering`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `imwls_languages`
--

INSERT INTO `imwls_languages` (`lang_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 'en-GB', 'English (UK)', 'English (UK)', 'en', 'en', '', '', '', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `imwls_menu`
--

CREATE TABLE IF NOT EXISTS `imwls_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(1024) NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`,`language`),
  KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  KEY `idx_menutype` (`menutype`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_path` (`path`(255)),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=125 ;

--
-- Dumping data for table `imwls_menu`
--

INSERT INTO `imwls_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 59, 0, '*', 0),
(2, 'menu', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 0, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'menu', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'menu', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 0, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'menu', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'menu', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'menu', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 0, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 11, 16, 0, '*', 1),
(8, 'menu', 'com_contact', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 0, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 12, 13, 0, '*', 1),
(9, 'menu', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 0, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 14, 15, 0, '*', 1),
(10, 'menu', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 0, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 17, 22, 0, '*', 1),
(11, 'menu', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 18, 19, 0, '*', 1),
(12, 'menu', 'com_messages_read', 'Read Private Message', '', 'Messaging/Read Private Message', 'index.php?option=com_messages', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-read', 0, '', 20, 21, 0, '*', 1),
(13, 'menu', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 23, 28, 0, '*', 1),
(14, 'menu', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 24, 25, 0, '*', 1),
(15, 'menu', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 0, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 26, 27, 0, '*', 1),
(16, 'menu', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 0, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 29, 30, 0, '*', 1),
(17, 'menu', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 0, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 31, 32, 0, '*', 1),
(18, 'menu', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 0, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 33, 34, 0, '*', 1),
(19, 'menu', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 35, 36, 0, '*', 1),
(20, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 0, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 37, 38, 0, '', 1),
(21, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 0, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 39, 40, 0, '*', 1),
(101, 'mainmenu', 'Home', 'home', '', 'home', 'index.php?option=com_content&view=article&id=1', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"0","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"0","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"0","show_item_navigation":"","show_vote":"","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"1","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 41, 42, 1, '*', 0),
(102, 'mainmenu', 'Comedic genres', 'comedic-genres', '', 'comedic-genres', 'index.php?option=com_content&view=article&id=3', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"0","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"0","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"0","show_item_navigation":"","show_vote":"","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_tags":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 43, 44, 0, '*', 0),
(103, 'mainmenu', 'News', 'news', '', 'news', 'index.php?option=com_content&view=category&id=9', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_category_title":"1","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_category_heading_title":"","show_subcat_desc":"","show_cat_num_articles":"","show_cat_tags":"","page_subheading":"","show_pagination_limit":"","filter_field":"","show_headings":"","list_show_date":"published","date_format":"","list_show_hits":"0","list_show_author":"0","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","display_num":"10","show_featured":"","show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":"","page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0}', 45, 46, 0, '*', 0),
(119, 'main', 'COM_COMMENT', 'com-comment', '', 'com-comment', 'index.php?option=com_comment', 'component', 0, 1, 1, 10011, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_comment/backend/images/ccomment-logo.png', 0, '', 47, 56, 0, '', 1),
(120, 'main', 'COM_COMMENT_MANAGE_COMMENTS', 'com-comment-manage-comments', '', 'com-comment/com-comment-manage-comments', 'index.php?option=com_comment&view=comments', 'component', 0, 119, 2, 10011, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_comment/backend/images/edit.png', 0, '', 48, 49, 0, '', 1),
(121, 'main', 'COM_COMMENT_SETTINGS', 'com-comment-settings', '', 'com-comment/com-comment-settings', 'index.php?option=com_comment&view=settings', 'component', 0, 119, 2, 10011, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_comment/backend/images/config.png', 0, '', 50, 51, 0, '', 1),
(122, 'main', 'COM_COMMENT_IMPORT', 'com-comment-import', '', 'com-comment/com-comment-import', 'index.php?option=com_comment&view=import', 'component', 0, 119, 2, 10011, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_comment/backend/images/controlpanel.png', 0, '', 52, 53, 0, '', 1),
(123, 'main', 'COM_COMMENT_CUSTOM_FIELDS', 'com-comment-custom-fields', '', 'com-comment/com-comment-custom-fields', 'index.php?option=com_comment&view=customfields', 'component', 0, 119, 2, 10011, 0, '0000-00-00 00:00:00', 0, 1, 'class:component', 0, '', 54, 55, 0, '', 1),
(124, 'mainmenu', 'Log In', '2015-04-12-18-01-27', '', '2015-04-12-18-01-27', 'http://52.16.118.234/joomla/administrator/', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1}', 57, 58, 0, '*', 0);

-- --------------------------------------------------------

--
-- Table structure for table `imwls_menu_types`
--

CREATE TABLE IF NOT EXISTS `imwls_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL,
  `title` varchar(48) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_menutype` (`menutype`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `imwls_menu_types`
--

INSERT INTO `imwls_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_messages`
--

CREATE TABLE IF NOT EXISTS `imwls_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_messages_cfg`
--

CREATE TABLE IF NOT EXISTS `imwls_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_modules`
--

CREATE TABLE IF NOT EXISTS `imwls_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) NOT NULL DEFAULT '',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=89 ;

--
-- Dumping data for table `imwls_modules`
--

INSERT INTO `imwls_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 39, 'Main Menu', '', '', 1, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","startLevel":"0","endLevel":"0","showAllChildren":"0","tag_id":"","class_sfx":"","window_open":"","layout":"","moduleclass_sfx":"_menu","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(2, 40, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 41, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{"count":"5","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(4, 42, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{"count":"5","ordering":"c_dsc","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(8, 43, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 44, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 45, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{"count":"5","name":"1","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(12, 46, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{"layout":"","moduleclass_sfx":"","shownew":"1","showhelp":"1","cache":"0"}', 1, '*'),
(13, 47, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 48, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 49, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 50, 'Login Form', '', '', 7, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_login', 1, 1, '{"greeting":"1","name":"0"}', 0, '*'),
(17, 51, 'Breadcrumbs', '', '', 1, 'position-2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 1, 1, '{"moduleclass_sfx":"","showHome":"1","homeText":"","showComponent":"1","separator":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(79, 52, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{"layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(86, 53, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{"format":"short","product":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_modules_menu`
--

CREATE TABLE IF NOT EXISTS `imwls_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imwls_modules_menu`
--

INSERT INTO `imwls_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(79, 0),
(86, 0);

-- --------------------------------------------------------

--
-- Table structure for table `imwls_newsfeeds`
--

CREATE TABLE IF NOT EXISTS `imwls_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `link` varchar(200) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(10) unsigned NOT NULL DEFAULT '3600',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `images` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_overrider`
--

CREATE TABLE IF NOT EXISTS `imwls_overrider` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `constant` varchar(255) NOT NULL,
  `string` text NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_postinstall_messages`
--

CREATE TABLE IF NOT EXISTS `imwls_postinstall_messages` (
  `postinstall_message_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) NOT NULL DEFAULT '',
  `language_extension` varchar(255) NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`postinstall_message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `imwls_postinstall_messages`
--

INSERT INTO `imwls_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 1),
(2, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `imwls_redirect_links`
--

CREATE TABLE IF NOT EXISTS `imwls_redirect_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `old_url` varchar(255) NOT NULL,
  `new_url` varchar(255) DEFAULT NULL,
  `referer` varchar(150) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_link_old` (`old_url`),
  KEY `idx_link_modifed` (`modified_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_schemas`
--

CREATE TABLE IF NOT EXISTS `imwls_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) NOT NULL,
  PRIMARY KEY (`extension_id`,`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imwls_schemas`
--

INSERT INTO `imwls_schemas` (`extension_id`, `version_id`) VALUES
(700, '3.4.0-2015-02-26');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_session`
--

CREATE TABLE IF NOT EXISTS `imwls_session` (
  `session_id` varchar(200) NOT NULL DEFAULT '',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `guest` tinyint(4) unsigned DEFAULT '1',
  `time` varchar(14) DEFAULT '',
  `data` mediumtext,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) DEFAULT '',
  PRIMARY KEY (`session_id`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imwls_session`
--

INSERT INTO `imwls_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('f0fufc67nk20ujik9tf8v9tfo2', 1, 1, '1429905305', '__default|a:8:{s:15:"session.counter";i:1;s:19:"session.timer.start";i:1429905305;s:18:"session.timer.last";i:1429905305;s:17:"session.timer.now";i:1429905305;s:22:"session.client.browser";s:108:"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36";s:8:"registry";O:24:"Joomla\\Registry\\Registry":2:{s:7:"\\0\\0\\0data";O:8:"stdClass":0:{}s:9:"separator";s:1:".";}s:4:"user";O:5:"JUser":26:{s:9:"\\0\\0\\0isRoot";N;s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:5:"block";N;s:9:"sendEmail";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:6:"groups";a:1:{i:0;s:1:"9";}s:5:"guest";i:1;s:13:"lastResetTime";N;s:10:"resetCount";N;s:12:"requireReset";N;s:10:"\\0\\0\\0_params";O:24:"Joomla\\Registry\\Registry":2:{s:7:"\\0\\0\\0data";O:8:"stdClass":0:{}s:9:"separator";s:1:".";}s:14:"\\0\\0\\0_authGroups";N;s:14:"\\0\\0\\0_authLevels";a:3:{i:0;i:1;i:1;i:1;i:2;i:5;}s:15:"\\0\\0\\0_authActions";N;s:12:"\\0\\0\\0_errorMsg";N;s:13:"\\0\\0\\0userHelper";O:18:"JUserWrapperHelper":0:{}s:10:"\\0\\0\\0_errors";a:0:{}s:3:"aid";i:0;}s:13:"session.token";s:32:"4bdfa2a7d5aa125e63e2f7e730a91dd5";}', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_tags`
--

CREATE TABLE IF NOT EXISTS `imwls_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tag_idx` (`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `imwls_tags`
--

INSERT INTO `imwls_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 0, '2011-01-01 00:00:01', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_template_styles`
--

CREATE TABLE IF NOT EXISTS `imwls_template_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(50) NOT NULL DEFAULT '',
  `client_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `home` char(7) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_template` (`template`),
  KEY `idx_home` (`home`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `imwls_template_styles`
--

INSERT INTO `imwls_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '0', 'Beez3 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/joomla_black.gif","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","templatecolor":"personal","html5":"0"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{"showSiteName":"0","colourChoice":"","boldText":"0"}'),
(7, 'protostar', 0, '1', 'protostar - Default', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}'),
(8, 'isis', 1, '1', 'isis - Default', '{"templateColor":"","logoFile":""}');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_ucm_base`
--

CREATE TABLE IF NOT EXISTS `imwls_ucm_base` (
  `ucm_id` int(10) unsigned NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL,
  PRIMARY KEY (`ucm_id`),
  KEY `idx_ucm_item_id` (`ucm_item_id`),
  KEY `idx_ucm_type_id` (`ucm_type_id`),
  KEY `idx_ucm_language_id` (`ucm_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_ucm_content`
--

CREATE TABLE IF NOT EXISTS `imwls_ucm_content` (
  `core_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `core_type_alias` varchar(255) NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(255) NOT NULL,
  `core_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `core_body` mediumtext NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) NOT NULL DEFAULT '',
  `core_checked_out_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_access` int(10) unsigned NOT NULL DEFAULT '0',
  `core_params` text NOT NULL,
  `core_featured` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) NOT NULL,
  `core_publish_up` datetime NOT NULL,
  `core_publish_down` datetime NOT NULL,
  `core_content_item_id` int(10) unsigned DEFAULT NULL COMMENT 'ID from the individual type table',
  `asset_id` int(10) unsigned DEFAULT NULL COMMENT 'FK to the #__assets table.',
  `core_images` text NOT NULL,
  `core_urls` text NOT NULL,
  `core_hits` int(10) unsigned NOT NULL DEFAULT '0',
  `core_version` int(10) unsigned NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text NOT NULL,
  `core_metadesc` text NOT NULL,
  `core_catid` int(10) unsigned NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`core_content_id`),
  KEY `tag_idx` (`core_state`,`core_access`),
  KEY `idx_access` (`core_access`),
  KEY `idx_alias` (`core_alias`),
  KEY `idx_language` (`core_language`),
  KEY `idx_title` (`core_title`),
  KEY `idx_modified_time` (`core_modified_time`),
  KEY `idx_created_time` (`core_created_time`),
  KEY `idx_content_type` (`core_type_alias`),
  KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  KEY `idx_core_created_user_id` (`core_created_user_id`),
  KEY `idx_core_type_id` (`core_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains core content data in name spaced fields' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_ucm_history`
--

CREATE TABLE IF NOT EXISTS `imwls_ucm_history` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ucm_item_id` int(10) unsigned NOT NULL,
  `ucm_type_id` int(10) unsigned NOT NULL,
  `version_note` varchar(255) NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `character_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep',
  PRIMARY KEY (`version_id`),
  KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  KEY `idx_save_date` (`save_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `imwls_ucm_history`
--

INSERT INTO `imwls_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(1, 8, 5, '', '2015-04-08 01:12:19', 258, 547, '232a7be6ee9035db1d08f994e9c032eb51d6c7b0', '{"id":8,"asset_id":54,"parent_id":"1","lft":"11","rgt":12,"level":1,"path":null,"extension":"com_content","title":"About","alias":"about","note":"","description":"","published":"1","checked_out":null,"checked_out_time":null,"access":"1","params":"{\\"category_layout\\":\\"\\",\\"image\\":\\"\\",\\"image_alt\\":\\"\\"}","metadesc":"","metakey":"","metadata":"{\\"author\\":\\"\\",\\"robots\\":\\"\\"}","created_user_id":"258","created_time":"2015-04-08 01:12:19","modified_user_id":null,"modified_time":"2015-04-08 01:12:19","hits":"0","language":"*","version":null}', 0),
(2, 1, 1, '', '2015-04-08 01:13:18', 258, 4434, '0092ef0c98f3fa4db536e77627a584e38b0cacab', '{"id":1,"asset_id":55,"title":"Welcome to Comedy Central...!!!","alias":"welcome-to-comedy-central","introtext":"<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">According to modern views, Comedy (from the Greek: \\u03ba\\u03c9\\u03bc\\u1ff3\\u03b4\\u03af\\u03b1, k\\u014dm\\u014did\\u00eda) refers to any discourse or work generally intended to be humorous or to amuse by inducing laughter, especially in theatre, television, film and stand-up comedy. The origins of the term are found in Ancient Greece. In the Athenian democracy, the public opinion of voters was influenced by the political satire performed by the comic poets at the theaters. The theatrical genre of Greek comedy can be described as a dramatic performance which pits two groups or societies against each other in an amusing agon or conflict. Northrop Frye depicted these two opposing sides as a \\"Society of Youth\\" and a \\"Society of the Old\\". A revised view characterizes the essential agon of comedy as a struggle between a relatively powerless youth and the societal conventions that pose obstacles to his hopes. In this struggle, the youth is understood to be constrained by his lack of social authority, and is left with little choice but to take recourse in ruses which engender very dramatic irony which provokes laughter.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Satire and political satire use comedy to portray persons or social institutions as ridiculous or corrupt, thus alienating their audience from the object of their humor. Parody subverts popular genres and forms, critiquing those forms without necessarily condemning them.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Other forms of comedy include Screwball comedy, which derives its humor largely from bizarre, surprising (and improbable) situations or characters, and Black comedy, which is characterized by a form of humor that includes darker aspects of human behavior or human nature. Similarly scatological humor, sexual humor, and race humor create comedy by violating social conventions or taboos in comic ways. A comedy of manners typically takes as its subject a particular part of society (usually upper class society) and uses humor to parody or satirize the behavior and mannerisms of its members. Romantic comedy is a popular genre that depicts burgeoning romance in humorous terms and focuses on the foibles of those who are falling in love.<\\/span><\\/span><\\/p>","fulltext":"","state":1,"catid":"2","created":"2015-04-08 01:13:18","created_by":"258","created_by_alias":"","modified":"2015-04-08 01:13:18","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-04-08 01:13:18","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(3, 2, 1, '', '2015-04-08 05:37:13', 258, 30288, '51f1eb1a8e72b21e720871343da29ed7c2b63628', '{"id":2,"asset_id":56,"title":"How the Internet and a New Generation of Superfans Helped Create the Second Comedy Boom","alias":"how-the-internet-and-a-new-generation-of-superfans-helped-create-the-second-comedy-boom","introtext":"<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cWhat\\u2019s up, people of the sex shop?\\u201d asks Hannibal Buress into an overdriven microphone at 9:30 p.m. on a recent Tuesday in L.A. He\\u2019s standing in front of a wall of crotchless panties, and next to a black curtain that divides the part of the West Hollywood sex-toy shop where a customer is examining Fleshlights from the part where the bi-weekly comedy show Performance Anxiety is taking place. Buress is in town for the roast of Justin Bieber, and he\\u2019s at the Pleasure Chest to rehearse his set for a small but enthusiastic audience that\\u2019s doing its best to ignore the commercial for stainless-steel G-spotters playing on a TV to Buress\\u2019s right.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">The sex-toy shop was the second place in two hours where I saw Buress, whose upcoming Comedy Central show had just been announced that morning. Earlier, he made a surprise appearance at Put Your Hands Together, a weekly stand-up show at the UCB Theatre on Franklin Avenue, to do the same roast jokes to a fully packed house. Of course he\\u2019s doing multiple sets: It\\u2019s Tuesday in Los Angeles, the best night for comedy in the country. I\\u2019d planned to see four shows, and I could\\u2019ve easily picked four different, equally great-sounding shows happening at the same time.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">After both sets, Buress, as many comedians do at midnight on Tuesdays, came to the Belly Room at the Comedy Store to watch a show called Roast Battle. Roast Battle is a phenomenon in L.A. (and will be everywhere when a TV deal comes through, which it surely will) in which comedians, two at a time, face off against each other, trading insults. Looking around the room \\u2014 to Buress\\u2019s left is Jeff Ross, the stand-up veteran who mentors this small show; behind Buress is Jerrod Carmichael, the young comedian with a Spike Lee\\u2013directed HBO special under his belt; to Buress\\u2019s right is Dave Chappelle, smoking cigarettes and laughing loudly at every joke; further back, the standing-room-only crowd is spilling into the hallway \\u2014 one can\\u2019t help but feel that this is the epicenter of a boom.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">It\\u2019s not just Los Angeles. There has never been a better time to be a comedian: The talent pool is broad, deep, and more diverse than ever before; a new generation of passionate fans is supporting experimental work; and there are countless ways (online, onscreen, in your earbuds, at live shows) for new voices to be heard and \\u2014 not always a given when it comes to the internet \\u2014 make a living. It\\u2019s a peak that hasn\\u2019t been seen since the first comedy boom, which lasted from 1979 to about 1995, and was defined by two stages: First, in the wake of the phenomenon that was Saturday Night Live, the popularity of George Carlin, Richard Pryor, and Steve Martin, and the debut of TV shows like An Evening at the Improv, hundreds of comedy clubs opened up overnight, resulting in a wave of frequently terrible observational comedians getting paid a lot of money to fill the bounty of open stages. Then, after the huge success of Seinfeld, Home Improvement, and Roseanne, many of those same comedians were all given sitcoms, most of which failed.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">With clubs closing and TV deals drying up, alternative comedy began to rise from the ashes around 1995, with confessional comedians like Janeane Garofalo and Marc Maron pushing comedy shows into nontraditional spaces. That lit a long fuse that finally boomed in a big way in 2009: the year Marc Maron started \\u201cWTF\\u201d; the year Broad City launched as a web series; the year Aziz Ansari taped his first stand-up special; the year Rob Delaney joined Twitter; the year Parks and Recreation, Community, and The League premiered, and Ellie Kemper joined The Office; when it seemed like UCB was the unofficial farm team for Saturday Night Live and every sitcom on TV.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Prior to 2009, only three comedians had ever sold out Madison Square Garden (not the theater, the actual arena); three comedians have done it in the last three years, each multiple times. According to UCB, over 10,000 people will take improv classes in the U.S. this year. More people watched SNL 40 than the Golden Globes. Comedy Central\\u2019s original programming has nearly doubled in the last three years, in competition with other networks that have beefed up their own comedy offerings. The hiring of a new Daily Show host \\u2014 congratulations, Trevor Noah \\u2014 was treated with as much anticipation and passionate critique as when LeBron James decided to go play for the Miami Heat. About as many people follow Sarah Silverman on Twitter than follow Hillary and Bill Clinton combined. Critics revere Louis C.K. and Amy Schumer as geniuses. Practically every comedian has a podcast or web series, or both. Right now, comedy is bigger than it\\u2019s ever been. There are more people doing it, and more people enjoying it. Thanks to the internet, comedians now have infinite means by which to reach their fans; and those fans are more fanatical than ever. Welcome to the Second Comedy Boom.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cAmazon has shows, Netflix, YouTube, Hulu \\u2014 there are a lot of different ways to get yourself out there as a comedian,\\u201d Buress told Vulture before the taping of the Bieber roast. \\u201cYou can build yourself through the internet. People have been getting TV shows from having successful podcasts.\\u201d And even those who aren\\u2019t on TV are finding enthusiastic audiences, and ways to sustain a living in comedy on their own terms.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Thirty years ago, comedians had to fight for a few large slices of a small pie. In the \\u201990s, a few performers made millions as stars of network sitcoms, but most were left in the cold when comedy clubs started shutting down. Now the pie is bigger and slices more plentiful, which benefits everyone from Buress (who now draws paychecks as a star on three cable shows) down to the armies of unknown UCB performers getting paid to make videos for Funny or Die and College Humor.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Podcasting is just one of the many ways by which comedians can develop a fan base, but over the last couple years, it\\u2019s also become one of the most lucrative. According to Adam Sachs, the CEO of Midroll \\u2014 the company sells ads for popular podcasts such as \\u201cWTF With Marc Maron,\\u201d Scott Aukerman\\u2019s \\u201cComedy Bang! Bang!,\\u201d Pete Holmes\\u2019s \\u201cYou Made It Weird,\\u201d and Paul Scheer\\u2019s \\u201cHow Did This Get Made\\u201d \\u2014 \\u201cmany comedians could survive today with the revenue from their podcasts alone.\\u201d Sachs says a podcast with 40,000 downloads per episode can gross well over $75,000 a year, and shows in the 100,000-download range can gross somewhere between $250,000 and $400,000. He estimates that three or four Midroll podcasts will make over $1 million this year. This has been a boon to comedians like Maron, Aukerman, Chris Hardwick, and Bill Burr, comedy veterans who had to wait for the boom to truly break through \\u2014 all of whom have recently parlayed successful podcasts into cable-TV shows and larger fees for stand-up appearances.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cPretty much all of my friends that came up with me in comedy are working,\\u201d says Kristen Schaal. \\u201cTelevision is in a golden age, and now there\\u2019s like 2,000 channels for comedians to play around on.\\u201d Instead of broad network sitcoms, more comedians are getting small, idiosyncratic shows on cable or streaming sites. In the last five years, Comedy Central has seen increased competition from IFC, FX, FXX, Fusion, TBS, Netflix, Amazon, Hulu, Yahoo, and TruTV.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Broadcast networks have also been getting smaller and stranger lately, as with Fox\\u2019s brilliantly weird (and well-rated) The Last Man on Earth. Vulture\\u2019s Josef Adalian explains that \\u201cwith most new network comedies in recent years getting ratings not much higher than cable,\\u201d networks are realizing they might as well aim for \\u201ccable quality.\\u201d The ultimate goal for a comedian in 2015 isn\\u2019t starring on his or her own Home Improvement, a mainstream sitcom that waters down their stand-up material \\u2014 it\\u2019s having their own Louie, a show on which the creator retains full auteurist control over every weird aspect: Kroll Show, Broad City, Comedy Bang! Bang!, The Last Man on Earth, The Eric Andre Show, NTSF:SD:SUV, Inside Amy Schumer, Nathan for You, Lucas Bros. Moving Co., Mulaney (yes, Mulaney might\\u2019ve seemed like a traditional broad sitcom, but the point is that was exactly what John Mulaney wanted), and others were all created, written, and headlined by their comedian stars. None of these performers are rich for life thanks to their shows, but they\\u2019re all earning a living doing artistically fulfilling work.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Compare this to the grind of the original comedy boom, during which, to eke out a decent income, a comedian had to tour the country doing the same 45-minute set. As Marc Maron explained to me over the phone, comedy clubs of the time were a \\u201cbar business, [they had] never been a show business. You were there to sell drinks.\\u201d Discos were dying, and comedy was the word. Richard Zoglin, explaining the original Comedy Boom in his 2008 book Comedy at the Edge, wrote: \\u201cAn evening at the comedy club was perfect entertainment for a baby-boom generation that was just hitting its peak dating years. You could take a date to the movies, but the conversation had to wait until afterward. The discos were too noisy. At a comedy club you could talk during the show, gauge each other\\u2019s reactions, see what your date laughed at \\u2014 or was grossed out by.\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">To today\\u2019s fans, comedy is more than just background noise: \\u201cMillennials have a deeper connection to comedy than previous generations,\\u201d says Chanon Cook, Comedy Central\\u2019s head of research. According to Cook, it was television that made the difference. The explosion of comedy on TV helped kill comedy clubs and bring the original boom to an end \\u2014 but the biggest stars of that boom wound up on Saturday Night Live or on their own sitcoms, many of which were essential viewing for millennials, planting the seeds of the second boom. Endless reruns of Seinfeld and stand-up specials on Comedy Central gave this generation an inbuilt awareness of comedy\\u2019s conventions: The comedy nerd was born.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">As Comedy Central claimed in an (admittedly self-serving) 2012 survey, millennials are the first generation that views humor as the most valuable part of their self-definition. \\u201cComedy is to this generation what music was to previous generations,\\u201d Cook says. \\u201cThey use it to define themselves. They use it to connect with people.\\u201d Maron described comedy nerds to me as \\u201cpeople who are into different facets of the history of comedy, the different types of comedy.\\u201d Says Pete Holmes: \\u201cThe comedy audience of 2015 is like the guitar to the musician. They\\u2019re not just sitting there to get fucked up and smoke a cigarette inside, which is what it was in the \\u201980s; they\\u2019re there to actually participate in something.\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">On Wednesday in L.A., I saw this type of fan in action at The Meltdown, the best comedy show in the city. Situated at NerdMelt, the West Hollywood comedy theater in the back of a comic-book store, produced by Emily Gordon, and hosted by comedians Jonah Ray and Kumail Nanjiani (who is Gordon\\u2019s husband and a star of HBO\\u2019s Silicon Valley), The Meltdown has run for five years and last year became a show on Comedy Central. With 200 people crammed into a 150-person space, the room was hot \\u2014 figuratively hot and, thanks to L.A.\\u2019s late-winter heat wave, literally hot. The stage is less a stage and more a place where the chairs stop and the lights shine. The audience \\u2026Well, the audience is something. I\\u2019ll let Ray describe it:<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">They are some of the most dedicated, sweetest weirdos around. When we started, there were these different people that we would recognize, and they slowly started sitting closer to the stage, and they all started to become friends and know each other. Now, every week, the first three or four rows are the diehard regulars. They\\u2019re creatives and artists and comics themselves, and them knowing each other, and us knowing them, has created an amazing dynamic for Kumail and I to play with.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Gordon would tell me later that the week I was there, one of these fans got a tattoo of the hand stamp given to The Meltdown attendees.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Diehard fans, like those of The Meltdown, are now the norm at comedy shows around the country. Because of them, comedy has fundamentally changed in the following ways:<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">1. Comedy is more meta.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Modern fans\\u2019 familiarity with the way comedy works means that the content of the comedy is often based in the process by which it\\u2019s created. \\u201cThere\\u2019s a sophistication [in the audience] that wasn\\u2019t there back in the Evening at the Improv days,\\u201d Holmes tells me, referring to the 1980s stand-up TV show. \\u201cThey know what a bit is.\\u201d Holmes, like most comedians in 2015, addresses the fact that he\\u2019s doing stand-up while he\\u2019s performing. Early in my week in L.A., at UCB\\u2019s monolithic Thai Town theater, Holmes closed the weeklyshow stand-up show Church, following Melissa Villase\\u00f1or, who destroyed the audience with a series of impressions ranging from Pikachu to Wanda Sykes. Taking the stage, Holmes sighed: \\u201cWell, she killed. Great.\\u201d He began his set: \\u201cMy dog got sprayed by a skunk today \\u2014 another comedian would then do 20 minutes of perfect material about that,\\u201d he said, mocking comedians who say things happened to them recently when, in fact, the things happened ages ago, whenever the performer started working on that joke. (Nowadays you\\u2019ll often hear jokes that start like, \\u201cA week ago \\u2014 or a week ago from whenever I wrote this bit.\\u201d) His set\\u2019s biggest laugh probably came during a bit about having a dog despite having grown up with cats, riffing, \\u201cOoh, a comedian talking about the difference between cats and dogs.\\u201d Comedy nerds are as interested in the source code as they are the jokes themselves. In 2015, the source code is the jokes.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">2. The line between fan and performer is blurred.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">The UCB Theatre has done plenty to help foster the second comedy boom \\u2014 popularizing improv, launching countless star performers, serving as the comedic hub for both New York and L.A., providing budding comedians with free stage time \\u2014 but its most important contribution is the way it changed the relationship between audience and performer. According to Kevin Hines, from the UCB Theatre\\u2019s training center in New York, 11,918 students took classes at UCB NY or LA last year. 11,918! (About 6,400 of those took Improv 101, the theater\\u2019s entry-level class, which is twice as many that took the same class five years ago). At UCB, everyone performs, and everyone watches everyone else. It\\u2019s a scene, not just a bunch of shows. Pete Holmes, who took classes at UCB while pursuing stand-up, explains: \\u201cI like doing stand-up like improv, by looking at the audience as your scene partner.\\u201d At a comedy show in 2015, everybody works together to create the best show possible. Everyone\\u2019s a comedian.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">3. Comedy is more conversational and less punch-line-driven.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Today, audiences expect comedians to talk to them like peers \\u2014 they don\\u2019t like being performed down to. \\u201c[Comedy\\u2019s] changed and become more conversational,\\u201d Nanjiani says. \\u201cPeople are okay listening to a podcast that\\u2019s an hour and a half, and it\\u2019s not funny, funny, funny, funny.\\u201d Nanjiani says those fans \\u201ctake that expectation to a comedy show as well,\\u201d and as a result, he\\u2019s learned to \\u201chide his punch lines\\u201d so as not to seem detached. Now Nanjiani and Ray often go into The Meltdown shows without any particular plan, and essentially just hang out for a few minutes. \\u201cComics that don\\u2019t make an effort to exist in that moment with everybody else in the room tend to get a colder reaction,\\u201d says Ray.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">This was on my mind as I watched comedian Mark Normand take the stage at The Meltdown. Two days earlier, at Hot Tub, Kristen Schaal and Kurt Braunohler\\u2019s weekly show, he had a rough go of it, telling well-crafted jokes (i.e., \\u201cI look at racism the same way I look at Nickelback: It\\u2019s fun to joke about, but ugh, you never want to see it live\\u201d) that would get short pulses of laughter instead of the bigger, rolling laugh of a fully engaged audience. The distance increased as Normand blamed the crowd for being too sensitive. He was trying to deliver his best material to an audience that didn\\u2019t want \\u201cmaterial\\u201d at all \\u2014 they just wanted to just get to know him. He connected better at the Meltdown. Setting up a set of jokes about homophobia, he asked if there were any gay guys in the audience. When no one responded, he joked, \\u201cWell, that is statistically impossible, [especially here,] in the back of a nerd museum.\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">4. Experimentation is expected.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Kristen Schaal, however, killed at both Hot Tub and The Meltdown with one of the funniest things I\\u2019ve seen in years: a one-woman show about Emily Dickinson (titled Emily Dick-unsung). Schaal\\u2019s version of Dickinson is a spinster with messy hair who hides poems around the stage and asks audience members to read them. One poem was just a grocery list, but Schaal acted as though it contained her innermost secrets: \\u201cThis poem is not ready for the world!\\u201d Another poem was actually just an olive in a folded piece of paper; she made an audience member \\u201cread\\u201d it by eating the olive. The bit doesn\\u2019t make much sense (especially when one attempts to describe it after the fact), but it destroyed both of the audiences I saw it with. I learned later that these were only the second and third times she\\u2019d ever performed it.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">In 2015, audiences are happily willing to follow comedians down any rabbit hole, no matter how unlikely the payoff. Sure, experimental comedians have existed since the \\u201970s \\u2013 Steve Martin, Andy Kaufman, Emo Philips, etc. \\u2014 but no longer are they the exception, as comedy-nerd audiences expect and are ready for comedians to have fun with the form (especially, for the first time, not just white males). Watch Rory Scovel\\u2019s brilliant set from The Meltdown TV show, for example. Scovel takes the stage and, responding to the audience\\u2019s enthusiastic welcome, ditches his planned material, and tells the crowd to keep clapping for the entirety of his ten-minute set. It\\u2019s an odd bit, especially considering that it\\u2019s being taped for television. He doesn\\u2019t tell jokes. He just offers a running commentary on what\\u2019s happening and eggs his audience on. He wonders what it would be like to be a TV viewer tuning in halfway through the bit. Eventually, he becomes mock-angry at his at-home audience: \\u201cStay the fuck out of here!\\u201d he shouts at the camera. \\u201cThis is our time down here!\\u201d At the end, the claps become a standing ovation, and Scovel seems actually touched that everyone went along with this very silly idea. Even in the two minutes and 41 seconds that aired on Comedy Central, you can see exactly what comedy looks like right now.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">So, where to from here? Actually, comedy\\u2019s next generation is already beginning to emerge. Back in New York, I go to the Holy Fuck Comedy Hour, a madcap weekly sketch show at the new Williamsburg location of the Annoyance Theatre. Started in Chicago in the late \\u201980s, the Annoyance has been a launchpad for many comedy careers \\u2014 including those of Stephen Colbert, Jane Lynch, Jeff Garlin, Vanessa Bayer, Jason Sudeikis, and others \\u2014 though Second City, where those Chicagoans would also perform, usually hogs credit for them. The Annoyance\\u2019s month-old New York location, below the Williamsburg Bridge and underneath a jazz club, seems to be their attempt to assert themselves as the alternative to the former alternative, UCB.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cLoud-lings\\u201d is how I like to describe the Holy Fuck Comedy Hour, which mixes Chicago-style brashness with the character focus of L.A.\\u2019s Groundlings. Perhaps to drown out the jazz coming from upstairs, everything is turned up to 11: Each sketch ends with someone onstage giving the cue to hit the \\u201clights\\u201d; New York\\u2019s most inventive stand-up, Jo Firestone, asks the audience to shout out \\u201csunshine, lollipops, or rainbows\\u201d whenever they want to interrupt her deadly serious (and hilarious) one-woman show about moving to New York; for a sketch in which gay parents bring their straight son to a doctor to turn him gay, a projector screen plays gay porn (with the actor who\\u2019s playing the son\\u2019s face Photoshopped onto every porn star\\u2019s) on a loop.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">The sketch of the night is slightly more subdued, but just as absurd. A woman comes onstage wearing a Saturday Night Live T-shirt, thumbing through her copy of the definitive SNL oral history Live From New York, geeking out just at the names of the people who agreed to be interviewed \\u2014 \\u201cBill Murray, hehe, awesome!\\u201d Then a boy comes onstage and asks what she\\u2019s reading. They flirt by repeating iconic SNL catchphrases (\\u201cChoppin\\u2019 broccoli!?\\u201d). The audience, packed densely enough to worry any good fire marshal, laughs knowingly. The woman is Claire Mulaney (John\\u2019s younger sister), a writer for Saturday Night Live, who is there performing the sketch on her week off. This is more than meta comedy for an accepting audience; it\\u2019s damn near postmodern. The sketch ends with Mulaney and her new beau screaming, \\u201cLive from New York, it\\u2019s Saturday Night!!!!!\\u201d Mulaney laughs. We all do. \\u201cLights!\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Source:\\u00a0<a href=\\"http:\\/\\/www.vulture.com\\/2015\\/03\\/welcome-to-the-second-comedy-boom.html\\" target=\\"_blank\\">www.vulture.com<\\/a><\\/span><\\/span><\\/p>","fulltext":"","state":1,"catid":"2","created":"2015-04-08 05:37:13","created_by":"258","created_by_alias":"","modified":"2015-04-08 05:37:13","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-04-08 05:37:13","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"0\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"0\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"0\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"0\\",\\"show_print_icon\\":\\"0\\",\\"show_email_icon\\":\\"0\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"0\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(4, 9, 5, '', '2015-04-08 05:37:31', 258, 545, 'cbc2c946b521b07c046b5bd552a7a87c3bea1394', '{"id":9,"asset_id":57,"parent_id":"1","lft":"13","rgt":14,"level":1,"path":null,"extension":"com_content","title":"News","alias":"news","note":"","description":"","published":"1","checked_out":null,"checked_out_time":null,"access":"1","params":"{\\"category_layout\\":\\"\\",\\"image\\":\\"\\",\\"image_alt\\":\\"\\"}","metadesc":"","metakey":"","metadata":"{\\"author\\":\\"\\",\\"robots\\":\\"\\"}","created_user_id":"258","created_time":"2015-04-08 05:37:31","modified_user_id":null,"modified_time":"2015-04-08 05:37:31","hits":"0","language":"*","version":null}', 0);
INSERT INTO `imwls_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(5, 2, 1, '', '2015-04-08 05:37:47', 258, 30307, 'db7e1f30611411004b370f52f97ab9c799eee2fb', '{"id":2,"asset_id":"56","title":"How the Internet and a New Generation of Superfans Helped Create the Second Comedy Boom","alias":"how-the-internet-and-a-new-generation-of-superfans-helped-create-the-second-comedy-boom","introtext":"<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cWhat\\u2019s up, people of the sex shop?\\u201d asks Hannibal Buress into an overdriven microphone at 9:30 p.m. on a recent Tuesday in L.A. He\\u2019s standing in front of a wall of crotchless panties, and next to a black curtain that divides the part of the West Hollywood sex-toy shop where a customer is examining Fleshlights from the part where the bi-weekly comedy show Performance Anxiety is taking place. Buress is in town for the roast of Justin Bieber, and he\\u2019s at the Pleasure Chest to rehearse his set for a small but enthusiastic audience that\\u2019s doing its best to ignore the commercial for stainless-steel G-spotters playing on a TV to Buress\\u2019s right.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">The sex-toy shop was the second place in two hours where I saw Buress, whose upcoming Comedy Central show had just been announced that morning. Earlier, he made a surprise appearance at Put Your Hands Together, a weekly stand-up show at the UCB Theatre on Franklin Avenue, to do the same roast jokes to a fully packed house. Of course he\\u2019s doing multiple sets: It\\u2019s Tuesday in Los Angeles, the best night for comedy in the country. I\\u2019d planned to see four shows, and I could\\u2019ve easily picked four different, equally great-sounding shows happening at the same time.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">After both sets, Buress, as many comedians do at midnight on Tuesdays, came to the Belly Room at the Comedy Store to watch a show called Roast Battle. Roast Battle is a phenomenon in L.A. (and will be everywhere when a TV deal comes through, which it surely will) in which comedians, two at a time, face off against each other, trading insults. Looking around the room \\u2014 to Buress\\u2019s left is Jeff Ross, the stand-up veteran who mentors this small show; behind Buress is Jerrod Carmichael, the young comedian with a Spike Lee\\u2013directed HBO special under his belt; to Buress\\u2019s right is Dave Chappelle, smoking cigarettes and laughing loudly at every joke; further back, the standing-room-only crowd is spilling into the hallway \\u2014 one can\\u2019t help but feel that this is the epicenter of a boom.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">It\\u2019s not just Los Angeles. There has never been a better time to be a comedian: The talent pool is broad, deep, and more diverse than ever before; a new generation of passionate fans is supporting experimental work; and there are countless ways (online, onscreen, in your earbuds, at live shows) for new voices to be heard and \\u2014 not always a given when it comes to the internet \\u2014 make a living. It\\u2019s a peak that hasn\\u2019t been seen since the first comedy boom, which lasted from 1979 to about 1995, and was defined by two stages: First, in the wake of the phenomenon that was Saturday Night Live, the popularity of George Carlin, Richard Pryor, and Steve Martin, and the debut of TV shows like An Evening at the Improv, hundreds of comedy clubs opened up overnight, resulting in a wave of frequently terrible observational comedians getting paid a lot of money to fill the bounty of open stages. Then, after the huge success of Seinfeld, Home Improvement, and Roseanne, many of those same comedians were all given sitcoms, most of which failed.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">With clubs closing and TV deals drying up, alternative comedy began to rise from the ashes around 1995, with confessional comedians like Janeane Garofalo and Marc Maron pushing comedy shows into nontraditional spaces. That lit a long fuse that finally boomed in a big way in 2009: the year Marc Maron started \\u201cWTF\\u201d; the year Broad City launched as a web series; the year Aziz Ansari taped his first stand-up special; the year Rob Delaney joined Twitter; the year Parks and Recreation, Community, and The League premiered, and Ellie Kemper joined The Office; when it seemed like UCB was the unofficial farm team for Saturday Night Live and every sitcom on TV.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Prior to 2009, only three comedians had ever sold out Madison Square Garden (not the theater, the actual arena); three comedians have done it in the last three years, each multiple times. According to UCB, over 10,000 people will take improv classes in the U.S. this year. More people watched SNL 40 than the Golden Globes. Comedy Central\\u2019s original programming has nearly doubled in the last three years, in competition with other networks that have beefed up their own comedy offerings. The hiring of a new Daily Show host \\u2014 congratulations, Trevor Noah \\u2014 was treated with as much anticipation and passionate critique as when LeBron James decided to go play for the Miami Heat. About as many people follow Sarah Silverman on Twitter than follow Hillary and Bill Clinton combined. Critics revere Louis C.K. and Amy Schumer as geniuses. Practically every comedian has a podcast or web series, or both. Right now, comedy is bigger than it\\u2019s ever been. There are more people doing it, and more people enjoying it. Thanks to the internet, comedians now have infinite means by which to reach their fans; and those fans are more fanatical than ever. Welcome to the Second Comedy Boom.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cAmazon has shows, Netflix, YouTube, Hulu \\u2014 there are a lot of different ways to get yourself out there as a comedian,\\u201d Buress told Vulture before the taping of the Bieber roast. \\u201cYou can build yourself through the internet. People have been getting TV shows from having successful podcasts.\\u201d And even those who aren\\u2019t on TV are finding enthusiastic audiences, and ways to sustain a living in comedy on their own terms.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Thirty years ago, comedians had to fight for a few large slices of a small pie. In the \\u201990s, a few performers made millions as stars of network sitcoms, but most were left in the cold when comedy clubs started shutting down. Now the pie is bigger and slices more plentiful, which benefits everyone from Buress (who now draws paychecks as a star on three cable shows) down to the armies of unknown UCB performers getting paid to make videos for Funny or Die and College Humor.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Podcasting is just one of the many ways by which comedians can develop a fan base, but over the last couple years, it\\u2019s also become one of the most lucrative. According to Adam Sachs, the CEO of Midroll \\u2014 the company sells ads for popular podcasts such as \\u201cWTF With Marc Maron,\\u201d Scott Aukerman\\u2019s \\u201cComedy Bang! Bang!,\\u201d Pete Holmes\\u2019s \\u201cYou Made It Weird,\\u201d and Paul Scheer\\u2019s \\u201cHow Did This Get Made\\u201d \\u2014 \\u201cmany comedians could survive today with the revenue from their podcasts alone.\\u201d Sachs says a podcast with 40,000 downloads per episode can gross well over $75,000 a year, and shows in the 100,000-download range can gross somewhere between $250,000 and $400,000. He estimates that three or four Midroll podcasts will make over $1 million this year. This has been a boon to comedians like Maron, Aukerman, Chris Hardwick, and Bill Burr, comedy veterans who had to wait for the boom to truly break through \\u2014 all of whom have recently parlayed successful podcasts into cable-TV shows and larger fees for stand-up appearances.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cPretty much all of my friends that came up with me in comedy are working,\\u201d says Kristen Schaal. \\u201cTelevision is in a golden age, and now there\\u2019s like 2,000 channels for comedians to play around on.\\u201d Instead of broad network sitcoms, more comedians are getting small, idiosyncratic shows on cable or streaming sites. In the last five years, Comedy Central has seen increased competition from IFC, FX, FXX, Fusion, TBS, Netflix, Amazon, Hulu, Yahoo, and TruTV.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Broadcast networks have also been getting smaller and stranger lately, as with Fox\\u2019s brilliantly weird (and well-rated) The Last Man on Earth. Vulture\\u2019s Josef Adalian explains that \\u201cwith most new network comedies in recent years getting ratings not much higher than cable,\\u201d networks are realizing they might as well aim for \\u201ccable quality.\\u201d The ultimate goal for a comedian in 2015 isn\\u2019t starring on his or her own Home Improvement, a mainstream sitcom that waters down their stand-up material \\u2014 it\\u2019s having their own Louie, a show on which the creator retains full auteurist control over every weird aspect: Kroll Show, Broad City, Comedy Bang! Bang!, The Last Man on Earth, The Eric Andre Show, NTSF:SD:SUV, Inside Amy Schumer, Nathan for You, Lucas Bros. Moving Co., Mulaney (yes, Mulaney might\\u2019ve seemed like a traditional broad sitcom, but the point is that was exactly what John Mulaney wanted), and others were all created, written, and headlined by their comedian stars. None of these performers are rich for life thanks to their shows, but they\\u2019re all earning a living doing artistically fulfilling work.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Compare this to the grind of the original comedy boom, during which, to eke out a decent income, a comedian had to tour the country doing the same 45-minute set. As Marc Maron explained to me over the phone, comedy clubs of the time were a \\u201cbar business, [they had] never been a show business. You were there to sell drinks.\\u201d Discos were dying, and comedy was the word. Richard Zoglin, explaining the original Comedy Boom in his 2008 book Comedy at the Edge, wrote: \\u201cAn evening at the comedy club was perfect entertainment for a baby-boom generation that was just hitting its peak dating years. You could take a date to the movies, but the conversation had to wait until afterward. The discos were too noisy. At a comedy club you could talk during the show, gauge each other\\u2019s reactions, see what your date laughed at \\u2014 or was grossed out by.\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">To today\\u2019s fans, comedy is more than just background noise: \\u201cMillennials have a deeper connection to comedy than previous generations,\\u201d says Chanon Cook, Comedy Central\\u2019s head of research. According to Cook, it was television that made the difference. The explosion of comedy on TV helped kill comedy clubs and bring the original boom to an end \\u2014 but the biggest stars of that boom wound up on Saturday Night Live or on their own sitcoms, many of which were essential viewing for millennials, planting the seeds of the second boom. Endless reruns of Seinfeld and stand-up specials on Comedy Central gave this generation an inbuilt awareness of comedy\\u2019s conventions: The comedy nerd was born.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">As Comedy Central claimed in an (admittedly self-serving) 2012 survey, millennials are the first generation that views humor as the most valuable part of their self-definition. \\u201cComedy is to this generation what music was to previous generations,\\u201d Cook says. \\u201cThey use it to define themselves. They use it to connect with people.\\u201d Maron described comedy nerds to me as \\u201cpeople who are into different facets of the history of comedy, the different types of comedy.\\u201d Says Pete Holmes: \\u201cThe comedy audience of 2015 is like the guitar to the musician. They\\u2019re not just sitting there to get fucked up and smoke a cigarette inside, which is what it was in the \\u201980s; they\\u2019re there to actually participate in something.\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">On Wednesday in L.A., I saw this type of fan in action at The Meltdown, the best comedy show in the city. Situated at NerdMelt, the West Hollywood comedy theater in the back of a comic-book store, produced by Emily Gordon, and hosted by comedians Jonah Ray and Kumail Nanjiani (who is Gordon\\u2019s husband and a star of HBO\\u2019s Silicon Valley), The Meltdown has run for five years and last year became a show on Comedy Central. With 200 people crammed into a 150-person space, the room was hot \\u2014 figuratively hot and, thanks to L.A.\\u2019s late-winter heat wave, literally hot. The stage is less a stage and more a place where the chairs stop and the lights shine. The audience \\u2026Well, the audience is something. I\\u2019ll let Ray describe it:<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">They are some of the most dedicated, sweetest weirdos around. When we started, there were these different people that we would recognize, and they slowly started sitting closer to the stage, and they all started to become friends and know each other. Now, every week, the first three or four rows are the diehard regulars. They\\u2019re creatives and artists and comics themselves, and them knowing each other, and us knowing them, has created an amazing dynamic for Kumail and I to play with.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Gordon would tell me later that the week I was there, one of these fans got a tattoo of the hand stamp given to The Meltdown attendees.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Diehard fans, like those of The Meltdown, are now the norm at comedy shows around the country. Because of them, comedy has fundamentally changed in the following ways:<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">1. Comedy is more meta.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Modern fans\\u2019 familiarity with the way comedy works means that the content of the comedy is often based in the process by which it\\u2019s created. \\u201cThere\\u2019s a sophistication [in the audience] that wasn\\u2019t there back in the Evening at the Improv days,\\u201d Holmes tells me, referring to the 1980s stand-up TV show. \\u201cThey know what a bit is.\\u201d Holmes, like most comedians in 2015, addresses the fact that he\\u2019s doing stand-up while he\\u2019s performing. Early in my week in L.A., at UCB\\u2019s monolithic Thai Town theater, Holmes closed the weeklyshow stand-up show Church, following Melissa Villase\\u00f1or, who destroyed the audience with a series of impressions ranging from Pikachu to Wanda Sykes. Taking the stage, Holmes sighed: \\u201cWell, she killed. Great.\\u201d He began his set: \\u201cMy dog got sprayed by a skunk today \\u2014 another comedian would then do 20 minutes of perfect material about that,\\u201d he said, mocking comedians who say things happened to them recently when, in fact, the things happened ages ago, whenever the performer started working on that joke. (Nowadays you\\u2019ll often hear jokes that start like, \\u201cA week ago \\u2014 or a week ago from whenever I wrote this bit.\\u201d) His set\\u2019s biggest laugh probably came during a bit about having a dog despite having grown up with cats, riffing, \\u201cOoh, a comedian talking about the difference between cats and dogs.\\u201d Comedy nerds are as interested in the source code as they are the jokes themselves. In 2015, the source code is the jokes.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">2. The line between fan and performer is blurred.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">The UCB Theatre has done plenty to help foster the second comedy boom \\u2014 popularizing improv, launching countless star performers, serving as the comedic hub for both New York and L.A., providing budding comedians with free stage time \\u2014 but its most important contribution is the way it changed the relationship between audience and performer. According to Kevin Hines, from the UCB Theatre\\u2019s training center in New York, 11,918 students took classes at UCB NY or LA last year. 11,918! (About 6,400 of those took Improv 101, the theater\\u2019s entry-level class, which is twice as many that took the same class five years ago). At UCB, everyone performs, and everyone watches everyone else. It\\u2019s a scene, not just a bunch of shows. Pete Holmes, who took classes at UCB while pursuing stand-up, explains: \\u201cI like doing stand-up like improv, by looking at the audience as your scene partner.\\u201d At a comedy show in 2015, everybody works together to create the best show possible. Everyone\\u2019s a comedian.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">3. Comedy is more conversational and less punch-line-driven.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Today, audiences expect comedians to talk to them like peers \\u2014 they don\\u2019t like being performed down to. \\u201c[Comedy\\u2019s] changed and become more conversational,\\u201d Nanjiani says. \\u201cPeople are okay listening to a podcast that\\u2019s an hour and a half, and it\\u2019s not funny, funny, funny, funny.\\u201d Nanjiani says those fans \\u201ctake that expectation to a comedy show as well,\\u201d and as a result, he\\u2019s learned to \\u201chide his punch lines\\u201d so as not to seem detached. Now Nanjiani and Ray often go into The Meltdown shows without any particular plan, and essentially just hang out for a few minutes. \\u201cComics that don\\u2019t make an effort to exist in that moment with everybody else in the room tend to get a colder reaction,\\u201d says Ray.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">This was on my mind as I watched comedian Mark Normand take the stage at The Meltdown. Two days earlier, at Hot Tub, Kristen Schaal and Kurt Braunohler\\u2019s weekly show, he had a rough go of it, telling well-crafted jokes (i.e., \\u201cI look at racism the same way I look at Nickelback: It\\u2019s fun to joke about, but ugh, you never want to see it live\\u201d) that would get short pulses of laughter instead of the bigger, rolling laugh of a fully engaged audience. The distance increased as Normand blamed the crowd for being too sensitive. He was trying to deliver his best material to an audience that didn\\u2019t want \\u201cmaterial\\u201d at all \\u2014 they just wanted to just get to know him. He connected better at the Meltdown. Setting up a set of jokes about homophobia, he asked if there were any gay guys in the audience. When no one responded, he joked, \\u201cWell, that is statistically impossible, [especially here,] in the back of a nerd museum.\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">4. Experimentation is expected.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Kristen Schaal, however, killed at both Hot Tub and The Meltdown with one of the funniest things I\\u2019ve seen in years: a one-woman show about Emily Dickinson (titled Emily Dick-unsung). Schaal\\u2019s version of Dickinson is a spinster with messy hair who hides poems around the stage and asks audience members to read them. One poem was just a grocery list, but Schaal acted as though it contained her innermost secrets: \\u201cThis poem is not ready for the world!\\u201d Another poem was actually just an olive in a folded piece of paper; she made an audience member \\u201cread\\u201d it by eating the olive. The bit doesn\\u2019t make much sense (especially when one attempts to describe it after the fact), but it destroyed both of the audiences I saw it with. I learned later that these were only the second and third times she\\u2019d ever performed it.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">In 2015, audiences are happily willing to follow comedians down any rabbit hole, no matter how unlikely the payoff. Sure, experimental comedians have existed since the \\u201970s \\u2013 Steve Martin, Andy Kaufman, Emo Philips, etc. \\u2014 but no longer are they the exception, as comedy-nerd audiences expect and are ready for comedians to have fun with the form (especially, for the first time, not just white males). Watch Rory Scovel\\u2019s brilliant set from The Meltdown TV show, for example. Scovel takes the stage and, responding to the audience\\u2019s enthusiastic welcome, ditches his planned material, and tells the crowd to keep clapping for the entirety of his ten-minute set. It\\u2019s an odd bit, especially considering that it\\u2019s being taped for television. He doesn\\u2019t tell jokes. He just offers a running commentary on what\\u2019s happening and eggs his audience on. He wonders what it would be like to be a TV viewer tuning in halfway through the bit. Eventually, he becomes mock-angry at his at-home audience: \\u201cStay the fuck out of here!\\u201d he shouts at the camera. \\u201cThis is our time down here!\\u201d At the end, the claps become a standing ovation, and Scovel seems actually touched that everyone went along with this very silly idea. Even in the two minutes and 41 seconds that aired on Comedy Central, you can see exactly what comedy looks like right now.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">So, where to from here? Actually, comedy\\u2019s next generation is already beginning to emerge. Back in New York, I go to the Holy Fuck Comedy Hour, a madcap weekly sketch show at the new Williamsburg location of the Annoyance Theatre. Started in Chicago in the late \\u201980s, the Annoyance has been a launchpad for many comedy careers \\u2014 including those of Stephen Colbert, Jane Lynch, Jeff Garlin, Vanessa Bayer, Jason Sudeikis, and others \\u2014 though Second City, where those Chicagoans would also perform, usually hogs credit for them. The Annoyance\\u2019s month-old New York location, below the Williamsburg Bridge and underneath a jazz club, seems to be their attempt to assert themselves as the alternative to the former alternative, UCB.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cLoud-lings\\u201d is how I like to describe the Holy Fuck Comedy Hour, which mixes Chicago-style brashness with the character focus of L.A.\\u2019s Groundlings. Perhaps to drown out the jazz coming from upstairs, everything is turned up to 11: Each sketch ends with someone onstage giving the cue to hit the \\u201clights\\u201d; New York\\u2019s most inventive stand-up, Jo Firestone, asks the audience to shout out \\u201csunshine, lollipops, or rainbows\\u201d whenever they want to interrupt her deadly serious (and hilarious) one-woman show about moving to New York; for a sketch in which gay parents bring their straight son to a doctor to turn him gay, a projector screen plays gay porn (with the actor who\\u2019s playing the son\\u2019s face Photoshopped onto every porn star\\u2019s) on a loop.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">The sketch of the night is slightly more subdued, but just as absurd. A woman comes onstage wearing a Saturday Night Live T-shirt, thumbing through her copy of the definitive SNL oral history Live From New York, geeking out just at the names of the people who agreed to be interviewed \\u2014 \\u201cBill Murray, hehe, awesome!\\u201d Then a boy comes onstage and asks what she\\u2019s reading. They flirt by repeating iconic SNL catchphrases (\\u201cChoppin\\u2019 broccoli!?\\u201d). The audience, packed densely enough to worry any good fire marshal, laughs knowingly. The woman is Claire Mulaney (John\\u2019s younger sister), a writer for Saturday Night Live, who is there performing the sketch on her week off. This is more than meta comedy for an accepting audience; it\\u2019s damn near postmodern. The sketch ends with Mulaney and her new beau screaming, \\u201cLive from New York, it\\u2019s Saturday Night!!!!!\\u201d Mulaney laughs. We all do. \\u201cLights!\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Source:\\u00a0<a href=\\"http:\\/\\/www.vulture.com\\/2015\\/03\\/welcome-to-the-second-comedy-boom.html\\" target=\\"_blank\\">www.vulture.com<\\/a><\\/span><\\/span><\\/p>","fulltext":"","state":1,"catid":"9","created":"2015-04-08 05:37:13","created_by":"258","created_by_alias":"","modified":"2015-04-08 05:37:47","modified_by":"258","checked_out":"258","checked_out_time":"2015-04-08 05:37:41","publish_up":"2015-04-08 05:37:13","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"0\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"0\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"0\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"0\\",\\"show_print_icon\\":\\"0\\",\\"show_email_icon\\":\\"0\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"0\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":2,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"0","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(6, 3, 1, '', '2015-04-08 05:38:36', 258, 17473, '6be9fb4be30fda11e765658a184e4f8f6630dc12', '{"id":3,"asset_id":58,"title":"Comedic genres","alias":"comedic-genres","introtext":"<p style=\\"margin: 0.5em 0px; line-height: 22.3999996185303px; color: #252525; font-family: sans-serif; font-size: 14px; text-align: justify;\\">Comedy\\u00a0may be divided into multiple\\u00a0genres\\u00a0based on the source of humor, the method of delivery, and the context in which it is delivered.<\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; line-height: 22.3999996185303px; color: #252525; font-family: sans-serif; font-size: 14px; text-align: justify;\\">These classifications overlap, and most\\u00a0comedians\\u00a0can fit into multiple genres. For example,\\u00a0deadpan\\u00a0comics often fall into observational boom comedy, or into black comedy or blue comedy to contrast the morbidity or offensiveness of the joke with a lack of emotion.<\\/p>\\r\\n<table class=\\"wikitable\\" style=\\"margin: 0.5em 0px; line-height: 22.3999996185303px; color: #252525; font-family: sans-serif; font-size: 14px; text-align: justify;\\">\\r\\n<tbody>\\r\\n<tr><th style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: center;\\">Genre<\\/th><th style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: center;\\">Description<\\/th><th style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: center;\\">Notable examples<\\/th><\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Alternative comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Differs from traditional punchline jokes which features many other forms of comedy such as observation, satire, surrealism, slapstick and improvisation<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Alexei Sayle,\\u00a0Mark Steel,\\u00a0Dan Harmon,\\u00a0Dave Gorman,\\u00a0Linda Smith,\\u00a0Jeremy Hardy,\\u00a0Ron Sparks,\\u00a0Alan Davies,\\u00a0Ben Elton,\\u00a0Jo Brand,\\u00a0Stewart Lee,\\u00a0Sean Hughes,\\u00a0Rik Mayall,\\u00a0Adrian Edmonson,Malcolm Hardee,\\u00a0Kristen Schaal<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Black comedyor dark comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Deals with disturbing subjects such as\\u00a0death,drugs,\\u00a0terrorism,rape, and\\u00a0war; can sometimes be related to the\\u00a0horrormovie genre<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Jim Norton,\\u00a0Bill Hicks,\\u00a0Frankie Boyle,\\u00a0Jimmy Carr,\\u00a0Denis Leary,Richard Pryor,\\u00a0Ricky Gervais,\\u00a0George Carlin,\\u00a0Chris Rush,\\u00a0Mike Ward,\\u00a0Penn &amp; Teller,\\u00a0Seth MacFarlane,\\u00a0Christopher Titus,\\u00a0Sacha Baron Cohen,\\u00a0Trey Parker,\\u00a0Quentin Tarantino,\\u00a0David Cross,\\u00a0Peter Kay<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Blue comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Typically sexual in nature (risqu\\u00e9) and\\/or using profane language; often using\\u00a0sexism,racism, andhomophobic\\u00a0views<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Jim Davidson,\\u00a0Frankie Boyle,\\u00a0Eddie Murphy,\\u00a0Bernard Manning,Martin Lawrence,\\u00a0George Lopez,\\u00a0Doug Stanhope,\\u00a0Seth MacFarlane,\\u00a0Redd Foxx,\\u00a0Bob Saget,\\u00a0Ron White,\\u00a0Dave Attell,\\u00a0Chris Rock,\\u00a0Sarah Silverman,\\u00a0Chappelle''s Show<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Character comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Derives humor from a persona invented by a performer; often fromstereotypes<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Phyllis Diller,\\u00a0Andy Kaufman,\\u00a0Bob Nelson,\\u00a0Catherine Tate,\\u00a0Paul Eddington,\\u00a0Andrew Dice Clay,\\u00a0Rich Hall,\\u00a0Tim Allen,\\u00a0John Gordon Sinclair,\\u00a0Lenny Henry,\\u00a0Sacha Baron Cohen,\\u00a0Christopher Ryan,Steve Guttenberg,\\u00a0Jerry Sadowitz,\\u00a0Steve Coogan,\\u00a0Bip,\\u00a0Jay London,\\u00a0Larry the Cable Guy,\\u00a0Sarah Silverman,\\u00a0Rob Brydon,Rowan Atkinson,\\u00a0Peter Helliar,\\u00a0Harry Enfield,\\u00a0Margaret Cho,\\u00a0Little Britain,\\u00a0Stephen Colbert,\\u00a0Al Murray,\\u00a0Paul Whitehouse,\\u00a0Charlie Higson,\\u00a0Kevin Hart<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Cringe comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A comedy of embarrassment, in which the humor comes from inappropriate actions or words; usually popular in television shows and film, but occasionally in stand-up as well<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Ricky Gervais,\\u00a0Richard Herring,\\u00a0Rufus Hound,\\u00a0Larry David,\\u00a0Alan Partridge,\\u00a0Bob Saget; TV shows:\\u00a0<i>Curb Your Enthusiasm,<\\/i>\\u00a0<i>The Office<\\/i>,\\u00a0<i>Peep Show<\\/i>,\\u00a0<i>The Proposal<\\/i>,\\u00a0<i>Family Guy<\\/i>,\\u00a0<i>The Inbetweeners<\\/i><\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Deadpancomedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Not strictly a style of comedy, it is telling jokes without a change in facial expression or change of emotion<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Milton Jones,\\u00a0Jack Dee,\\u00a0Jimmy Carr,\\u00a0Steven Wright,\\u00a0Peter Cook,Craig Ferguson,\\u00a0Dylan Moran,\\u00a0W. Kamau Bell,\\u00a0Buster Keaton,\\u00a0Bill Murray,\\u00a0Jim Gaffigan,\\u00a0Les Dawson,\\u00a0Mike Birbiglia,\\u00a0Mitch Hedberg,Bruce McCulloch,\\u00a0Demetri Martin,\\u00a0Elliott Goblet,\\u00a0Aubrey Plaza,Zach Galifianakis<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Improvisational comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Improvisational (sometimes shortened to improv) comics rarely plan out their routines; television show examples:<i>Curb Your Enthusiasm<\\/i>,\\u00a0<i>Whose Line Is It Anyway?<\\/i>,<i>Thank God You''re Here<\\/i><\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Robin Williams,\\u00a0Jonathan Winters,\\u00a0Bob Nelson,\\u00a0Paula Poundstone,\\u00a0Paul Merton,\\u00a0Tony Slattery,\\u00a0Josie Lawrence,\\u00a0Jim Sweeney,\\u00a0Steve Steen,\\u00a0Wayne Brady,\\u00a0Ryan Stiles,\\u00a0Colin Mochrie,Drew Carey,\\u00a0Greg Proops,\\u00a0John Sessions,\\u00a0Neil Mullarkey,\\u00a0Kathy Greenwood,\\u00a0Brad Sherwood,\\u00a0Chip Esten,\\u00a0Jeff Davis,\\u00a0Tina Fey,Amy Poehler,\\u00a0Jonathan Mangum,\\u00a0Mark Meer,\\u00a0Larry David<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Insult Comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A form which consists mainly of offensive insults directed at the performer''s audience and\\/or other performers<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Don Rickles,\\u00a0Andrew Dice Clay,\\u00a0Ricky Gervais,\\u00a0Bob Saget,Frankie Boyle,\\u00a0Jimmy Carr,\\u00a0Jerry Sadowitz,\\u00a0Sam Kinison,\\u00a0Seth MacFarlane,\\u00a0Triumph, the Insult Comic Dog,\\u00a0Roy ''Chubby'' Brown,Marcus Valerius Martialis,\\u00a0Jeffrey Ross,\\u00a0Lisa Lampanelli,\\u00a0D.L. Hughley,\\u00a0Greg Giraldo,\\u00a0Goundamani<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Mockumentary<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A parody using the conventions of documentary style<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Films and TV shows:\\u00a0<i>Borat<\\/i>,\\u00a0<i>This is Spinal Tap<\\/i>,\\u00a0<i>The Monkees<\\/i>,<i>The Rutles<\\/i>,\\u00a0<i>Summer Heights High<\\/i>,\\u00a0<i>Electric Apricot: Quest for Festeroo<\\/i>,\\u00a0<i>The Office<\\/i>,\\u00a0<i>Br\\u00fcno<\\/i>,\\u00a0<i>Parks and Recreation<\\/i>,\\u00a0<i>Modern Family<\\/i>,\\u00a0<i>Come Fly with Me<\\/i>,\\u00a0<i>Angry Boys<\\/i>,\\u00a0<i>The Compleat Al<\\/i>, \\"trailer park boys\\"<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Musical Comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A form of alternative comedy where humor is mostly derived from\\u00a0musicwith (or sometimes without)\\u00a0lyrics<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Bill Bailey,\\u00a0Denis Leary,\\u00a0Tim Minchin,\\u00a0The Lonely Island,\\u00a0Flight Of The Conchords,\\u00a0Les Luthiers,\\u00a0Mitch Benn,\\u00a0Tenacious D,\\u00a0Spinal Tap,\\u00a0Stephen Lynch,\\u00a0\\"Weird Al\\" Yankovic,\\u00a0Bob Rivers,\\u00a0Bo Burnham,\\u00a0Wayne Brady, the\\u00a0Bonzo Dog Doo-Dah Band,\\u00a0Tom Lehrer,\\u00a0Victor Borge<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Observational comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Pokes fun ateveryday life, often by inflating the importance of trivial things or by observing the silliness of something that society accepts as normal<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">George Carlin,\\u00a0Jerry Seinfeld,\\u00a0Craig Ferguson,\\u00a0Larry David,\\u00a0Bill Cosby,\\u00a0Mitch Hedberg,\\u00a0Billy Connolly,\\u00a0Russell Howard,\\u00a0Cedric the Entertainer,\\u00a0Steve Harvey,\\u00a0W. Kamau Bell,\\u00a0Ray Romano,\\u00a0Chris Rush,\\u00a0Dane Cook,\\u00a0Ricky Gervais,\\u00a0Chris Rock,\\u00a0Jim Gaffigan,\\u00a0Kathy Greenwood,\\u00a0Ellen DeGeneres,\\u00a0Russell Peters<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Physical comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Somewhat similar toslapstick, this form uses physical movement and gestures; often influenced byclowning<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Jim Carrey,\\u00a0Bob Nelson,\\u00a0Norman Wisdom,\\u00a0Jerry Lewis,\\u00a0Robin Williams,\\u00a0Chevy Chase,\\u00a0John Ritter,\\u00a0Conan O''Brien,\\u00a0Kunal Nayyar,\\u00a0Mr. Bean,\\u00a0Michael Mcintyre,\\u00a0Lee Evans,\\u00a0Max Wall,Matthew Perry,\\u00a0Brent Butt,\\u00a0Kathy Greenwood,\\u00a0The Three Stooges,\\u00a0Lano &amp; Woodley,\\u00a0Lucille Ball,\\u00a0Dane Cook,\\u00a0Chris Farley<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Prop comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Relies on ridiculous props, casual jackets or everyday objects used in humorous ways<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Bob Nelson,\\u00a0Carrot Top,\\u00a0Jeff Dunham,\\u00a0Gallagher,\\u00a0Timmy Mallett,The Amazing Johnathan,\\u00a0Jerry Sadowitz<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Spoof<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">The recreating of a book, film or play for humor; it can be used to make fun of, or ridicule, a certain production<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Mel Brooks,\\u00a0French and Saunders,\\u00a0Mitchell and Webb,\\u00a0I''m Sorry I Haven''t a Clue,\\u00a0Peter Serafinowicz,\\u00a0Weird Al Yankovic,\\u00a0Zucker, Abrahams and Zucker; Films and TV shows:\\u00a0<i>Hot Shots<\\/i>,\\u00a0<i>Frankie Boyle''s Tramadol Nights<\\/i>,\\u00a0<i>Shriek<\\/i>,\\u00a0<i>Look Around You<\\/i><\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Sitcom<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Scripted dialogue creating a thematic situation; commonly found on television series<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\"><i>Seinfeld<\\/i>,\\u00a0<i>Fawlty Towers<\\/i>,\\u00a0<i>Black Books<\\/i>,\\u00a0<i>Porridge<\\/i>,\\u00a0<i>Dad''s Army<\\/i>,<i>Blackadder<\\/i>,\\u00a0<i>Gavin and Stacey<\\/i>,\\u00a0<i>My Wife and Kids<\\/i>,\\u00a0<i>I Love Lucy<\\/i>,<i>Friends<\\/i>,\\u00a0<i>Corner Gas<\\/i>,\\u00a0<i>That ''70s Show<\\/i>,\\u00a0<i>The Office<\\/i>,\\u00a0<i>The Cosby Show<\\/i>,\\u00a0<i>The Simpsons<\\/i>,\\u00a0<i>Open All Hours<\\/i>,\\u00a0<i>Only Fools and Horses<\\/i>,<i>Dinner Ladies<\\/i>,\\u00a0<i>Modern Family,<\\/i>\\u00a0Melissa &amp; Joey<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Sketch<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A shorter version of a sitcom, practised and typically performed live<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Armstrong and Miller,\\u00a0Jennifer Saunders,\\u00a0Lorne Michaels,\\u00a0Dawn French,\\u00a0Craig Ferguson,\\u00a0Catherine Tate; TV shows:\\u00a0<i>Monty Python<\\/i>,\\u00a0<i>Armstrong and Miller<\\/i>,\\u00a0<i>Saturday Night Live<\\/i>,\\u00a0<i>Chappelle''s Show<\\/i>,\\u00a0<i>Firesign Theatre<\\/i>,\\u00a0<i>In Living Color<\\/i>,\\u00a0<i>A Bit of Fry &amp; Laurie<\\/i>,<i>Mad TV<\\/i><\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Surreal comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A form of\\u00a0humorbased on bizarrejuxtapositions,absurd\\u00a0situations, and\\u00a0nonsense\\u00a0logic<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Spike Milligan,\\u00a0Jay Kogen,\\u00a0Eddie Izzard,\\u00a0J. Stewart Burns,\\u00a0Ross Noble,\\u00a0Bill Bailey,\\u00a0Brent Butt,\\u00a0The Mighty Boosh,\\u00a0Steven Wright,Trey Parker,\\u00a0Monty Python,\\u00a0Seth MacFarlane,\\u00a0David X. Cohen,Vic and Bob,\\u00a0The Goodies,\\u00a0Jack Handey,\\u00a0Derek Drymon,\\u00a0Wallace Wolodarsky,\\u00a0Harry Hill,\\u00a0The Kids in the Hall,\\u00a0Conan O''Brien,\\u00a0Tim and Eric,\\u00a0Paul Merton,\\u00a0Mitch Hedberg,\\u00a0Firesign Theatre,\\u00a0Shaun Micallef,\\u00a0Emo Philips<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Topical comedy\\/Satire<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Relies on headlining\\/important news and current affairs; it dates quickly, but is a popular form for late night talk-variety shows<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">George Carlin,\\u00a0Bill Hicks,\\u00a0Chris Morris,\\u00a0Dennis Miller,\\u00a0Conan O''Brien,\\u00a0Russell Howard,\\u00a0Craig Ferguson,\\u00a0David Letterman,\\u00a0Jay Leno,\\u00a0Dan Harmon,\\u00a0Andy Hamilton,\\u00a0Bill Maher,\\u00a0Ian Hislop,\\u00a0Brent Butt,\\u00a0Paul Merton,\\u00a0Kathy Griffin,\\u00a0Jon Stewart,\\u00a0Stephen Colbert,Stewart Lee,\\u00a0Matt Groening,\\u00a0Rory Bremner,\\u00a0W. Kamau Bell,\\u00a0Ben Elton,\\u00a0David Cross,\\u00a0Lewis Black,\\u00a0Chris Rock,\\u00a0Dave Chappelle,\\u00a0The Chaser,\\u00a0Punt and Dennis,\\u00a0Jon Holmes; TV shows:\\u00a0<i>The Daily Show<\\/i>,\\u00a0<i>Have I Got News For You<\\/i>,\\u00a0<i>Mock The Week<\\/i>,\\u00a0<i>The News Quiz<\\/i>,\\u00a0<i>Saturday Night Live<\\/i>,\\u00a0<i>The Simpsons<\\/i>,\\u00a0<i>The Tonight Show,<\\/i>Late Show with David Letterman<i>,<\\/i>\\u00a0Wait Wait... Don''t Tell Me!<i>,<\\/i>South Park<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Wit\\/Word play<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">More intellectual forms based on clever, often subtle manipulation of language (thoughpuns\\u00a0can be crude and\\u00a0farcical)<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Groucho Marx,\\u00a0William Shakespeare,\\u00a0Harry Hill,\\u00a0Jay Jason,\\u00a0Oscar Wilde,\\u00a0Rodney Marks,\\u00a0Woody Allen,\\u00a0George Carlin,\\u00a0Tim Vine,Stephen Fry,\\u00a0Demetri Martin,\\u00a0Bo Burnham,\\u00a0Firesign Theatre,\\u00a0Myq Kaplan<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\\r\\n<p>Source:\\u00a0<a href=\\"http:\\/\\/en.wikipedia.org\\/wiki\\/Comedic_genres\\" target=\\"_blank\\">Wikipedia<\\/a><\\/p>","fulltext":"","state":1,"catid":"8","created":"2015-04-08 05:38:36","created_by":"258","created_by_alias":"","modified":"2015-04-08 05:38:36","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-04-08 05:38:36","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0);
INSERT INTO `imwls_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(7, 3, 1, '', '2015-04-08 05:42:36', 258, 17834, '7320ce59e1cc00137ff41d885286fde27e359def', '{"id":3,"asset_id":"58","title":"Comedic genres","alias":"comedic-genres","introtext":"<p style=\\"margin: 0.5em 0px; line-height: 22.3999996185303px; color: #252525; font-family: sans-serif; font-size: 14px; text-align: justify;\\">Comedy\\u00a0may be divided into multiple\\u00a0genres\\u00a0based on the source of humor, the method of delivery, and the context in which it is delivered.<\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; line-height: 22.3999996185303px; color: #252525; font-family: sans-serif; font-size: 14px; text-align: justify;\\">These classifications overlap, and most\\u00a0comedians\\u00a0can fit into multiple genres. For example,\\u00a0deadpan\\u00a0comics often fall into observational boom comedy, or into black comedy or blue comedy to contrast the morbidity or offensiveness of the joke with a lack of emotion.<\\/p>\\r\\n<table class=\\"wikitable\\" style=\\"margin: 0.5em 0px; line-height: 22.3999996185303px; color: #252525; font-family: sans-serif; font-size: 14px; text-align: justify;\\">\\r\\n<tbody>\\r\\n<tr><th style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: center;\\">Genre<\\/th><th style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: center;\\">Description<\\/th><th style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: center;\\">Notable examples<\\/th><\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Alternative comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Differs from traditional punchline jokes which features many other forms of comedy such as observation, satire, surrealism, slapstick and improvisation<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Alexei Sayle,\\u00a0Mark Steel,\\u00a0Dan Harmon,\\u00a0Dave Gorman,\\u00a0Linda Smith,\\u00a0Jeremy Hardy,\\u00a0Ron Sparks,\\u00a0Alan Davies,\\u00a0Ben Elton,\\u00a0Jo Brand,\\u00a0Stewart Lee,\\u00a0Sean Hughes,\\u00a0Rik Mayall,\\u00a0Adrian Edmonson,Malcolm Hardee,\\u00a0Kristen Schaal<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Black comedyor dark comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Deals with disturbing subjects such as\\u00a0death,drugs,\\u00a0terrorism,rape, and\\u00a0war; can sometimes be related to the\\u00a0horrormovie genre<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Jim Norton,\\u00a0Bill Hicks,\\u00a0Frankie Boyle,\\u00a0Jimmy Carr,\\u00a0Denis Leary,Richard Pryor,\\u00a0Ricky Gervais,\\u00a0George Carlin,\\u00a0Chris Rush,\\u00a0Mike Ward,\\u00a0Penn &amp; Teller,\\u00a0Seth MacFarlane,\\u00a0Christopher Titus,\\u00a0Sacha Baron Cohen,\\u00a0Trey Parker,\\u00a0Quentin Tarantino,\\u00a0David Cross,\\u00a0Peter Kay<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Blue comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Typically sexual in nature (risqu\\u00e9) and\\/or using profane language; often using\\u00a0sexism,racism, andhomophobic\\u00a0views<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Jim Davidson,\\u00a0Frankie Boyle,\\u00a0Eddie Murphy,\\u00a0Bernard Manning,Martin Lawrence,\\u00a0George Lopez,\\u00a0Doug Stanhope,\\u00a0Seth MacFarlane,\\u00a0Redd Foxx,\\u00a0Bob Saget,\\u00a0Ron White,\\u00a0Dave Attell,\\u00a0Chris Rock,\\u00a0Sarah Silverman,\\u00a0Chappelle''s Show<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Character comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Derives humor from a persona invented by a performer; often fromstereotypes<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Phyllis Diller,\\u00a0Andy Kaufman,\\u00a0Bob Nelson,\\u00a0Catherine Tate,\\u00a0Paul Eddington,\\u00a0Andrew Dice Clay,\\u00a0Rich Hall,\\u00a0Tim Allen,\\u00a0John Gordon Sinclair,\\u00a0Lenny Henry,\\u00a0Sacha Baron Cohen,\\u00a0Christopher Ryan,Steve Guttenberg,\\u00a0Jerry Sadowitz,\\u00a0Steve Coogan,\\u00a0Bip,\\u00a0Jay London,\\u00a0Larry the Cable Guy,\\u00a0Sarah Silverman,\\u00a0Rob Brydon,Rowan Atkinson,\\u00a0Peter Helliar,\\u00a0Harry Enfield,\\u00a0Margaret Cho,\\u00a0Little Britain,\\u00a0Stephen Colbert,\\u00a0Al Murray,\\u00a0Paul Whitehouse,\\u00a0Charlie Higson,\\u00a0Kevin Hart<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Cringe comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A comedy of embarrassment, in which the humor comes from inappropriate actions or words; usually popular in television shows and film, but occasionally in stand-up as well<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Ricky Gervais,\\u00a0Richard Herring,\\u00a0Rufus Hound,\\u00a0Larry David,\\u00a0Alan Partridge,\\u00a0Bob Saget; TV shows:\\u00a0<i>Curb Your Enthusiasm,<\\/i>\\u00a0<i>The Office<\\/i>,\\u00a0<i>Peep Show<\\/i>,\\u00a0<i>The Proposal<\\/i>,\\u00a0<i>Family Guy<\\/i>,\\u00a0<i>The Inbetweeners<\\/i><\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Deadpancomedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Not strictly a style of comedy, it is telling jokes without a change in facial expression or change of emotion<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Milton Jones,\\u00a0Jack Dee,\\u00a0Jimmy Carr,\\u00a0Steven Wright,\\u00a0Peter Cook,Craig Ferguson,\\u00a0Dylan Moran,\\u00a0W. Kamau Bell,\\u00a0Buster Keaton,\\u00a0Bill Murray,\\u00a0Jim Gaffigan,\\u00a0Les Dawson,\\u00a0Mike Birbiglia,\\u00a0Mitch Hedberg,Bruce McCulloch,\\u00a0Demetri Martin,\\u00a0Elliott Goblet,\\u00a0Aubrey Plaza,Zach Galifianakis<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Improvisational comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Improvisational (sometimes shortened to improv) comics rarely plan out their routines; television show examples:<i>Curb Your Enthusiasm<\\/i>,\\u00a0<i>Whose Line Is It Anyway?<\\/i>,<i>Thank God You''re Here<\\/i><\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Robin Williams,\\u00a0Jonathan Winters,\\u00a0Bob Nelson,\\u00a0Paula Poundstone,\\u00a0Paul Merton,\\u00a0Tony Slattery,\\u00a0Josie Lawrence,\\u00a0Jim Sweeney,\\u00a0Steve Steen,\\u00a0Wayne Brady,\\u00a0Ryan Stiles,\\u00a0Colin Mochrie,Drew Carey,\\u00a0Greg Proops,\\u00a0John Sessions,\\u00a0Neil Mullarkey,\\u00a0Kathy Greenwood,\\u00a0Brad Sherwood,\\u00a0Chip Esten,\\u00a0Jeff Davis,\\u00a0Tina Fey,Amy Poehler,\\u00a0Jonathan Mangum,\\u00a0Mark Meer,\\u00a0Larry David<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Insult Comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A form which consists mainly of offensive insults directed at the performer''s audience and\\/or other performers<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Don Rickles,\\u00a0Andrew Dice Clay,\\u00a0Ricky Gervais,\\u00a0Bob Saget,Frankie Boyle,\\u00a0Jimmy Carr,\\u00a0Jerry Sadowitz,\\u00a0Sam Kinison,\\u00a0Seth MacFarlane,\\u00a0Triumph, the Insult Comic Dog,\\u00a0Roy ''Chubby'' Brown,Marcus Valerius Martialis,\\u00a0Jeffrey Ross,\\u00a0Lisa Lampanelli,\\u00a0D.L. Hughley,\\u00a0Greg Giraldo,\\u00a0Goundamani<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Mockumentary<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A parody using the conventions of documentary style<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Films and TV shows:\\u00a0<i>Borat<\\/i>,\\u00a0<i>This is Spinal Tap<\\/i>,\\u00a0<i>The Monkees<\\/i>,<i>The Rutles<\\/i>,\\u00a0<i>Summer Heights High<\\/i>,\\u00a0<i>Electric Apricot: Quest for Festeroo<\\/i>,\\u00a0<i>The Office<\\/i>,\\u00a0<i>Br\\u00fcno<\\/i>,\\u00a0<i>Parks and Recreation<\\/i>,\\u00a0<i>Modern Family<\\/i>,\\u00a0<i>Come Fly with Me<\\/i>,\\u00a0<i>Angry Boys<\\/i>,\\u00a0<i>The Compleat Al<\\/i>, \\"trailer park boys\\"<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Musical Comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A form of alternative comedy where humor is mostly derived from\\u00a0musicwith (or sometimes without)\\u00a0lyrics<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Bill Bailey,\\u00a0Denis Leary,\\u00a0Tim Minchin,\\u00a0The Lonely Island,\\u00a0Flight Of The Conchords,\\u00a0Les Luthiers,\\u00a0Mitch Benn,\\u00a0Tenacious D,\\u00a0Spinal Tap,\\u00a0Stephen Lynch,\\u00a0\\"Weird Al\\" Yankovic,\\u00a0Bob Rivers,\\u00a0Bo Burnham,\\u00a0Wayne Brady, the\\u00a0Bonzo Dog Doo-Dah Band,\\u00a0Tom Lehrer,\\u00a0Victor Borge<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Observational comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Pokes fun ateveryday life, often by inflating the importance of trivial things or by observing the silliness of something that society accepts as normal<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">George Carlin,\\u00a0Jerry Seinfeld,\\u00a0Craig Ferguson,\\u00a0Larry David,\\u00a0Bill Cosby,\\u00a0Mitch Hedberg,\\u00a0Billy Connolly,\\u00a0Russell Howard,\\u00a0Cedric the Entertainer,\\u00a0Steve Harvey,\\u00a0W. Kamau Bell,\\u00a0Ray Romano,\\u00a0Chris Rush,\\u00a0Dane Cook,\\u00a0Ricky Gervais,\\u00a0Chris Rock,\\u00a0Jim Gaffigan,\\u00a0Kathy Greenwood,\\u00a0Ellen DeGeneres,\\u00a0Russell Peters<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Physical comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Somewhat similar toslapstick, this form uses physical movement and gestures; often influenced byclowning<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Jim Carrey,\\u00a0Bob Nelson,\\u00a0Norman Wisdom,\\u00a0Jerry Lewis,\\u00a0Robin Williams,\\u00a0Chevy Chase,\\u00a0John Ritter,\\u00a0Conan O''Brien,\\u00a0Kunal Nayyar,\\u00a0Mr. Bean,\\u00a0Michael Mcintyre,\\u00a0Lee Evans,\\u00a0Max Wall,Matthew Perry,\\u00a0Brent Butt,\\u00a0Kathy Greenwood,\\u00a0The Three Stooges,\\u00a0Lano &amp; Woodley,\\u00a0Lucille Ball,\\u00a0Dane Cook,\\u00a0Chris Farley<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Prop comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Relies on ridiculous props, casual jackets or everyday objects used in humorous ways<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Bob Nelson,\\u00a0Carrot Top,\\u00a0Jeff Dunham,\\u00a0Gallagher,\\u00a0Timmy Mallett,The Amazing Johnathan,\\u00a0Jerry Sadowitz<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Spoof<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">The recreating of a book, film or play for humor; it can be used to make fun of, or ridicule, a certain production<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Mel Brooks,\\u00a0French and Saunders,\\u00a0Mitchell and Webb,\\u00a0I''m Sorry I Haven''t a Clue,\\u00a0Peter Serafinowicz,\\u00a0Weird Al Yankovic,\\u00a0Zucker, Abrahams and Zucker; Films and TV shows:\\u00a0<i>Hot Shots<\\/i>,\\u00a0<i>Frankie Boyle''s Tramadol Nights<\\/i>,\\u00a0<i>Shriek<\\/i>,\\u00a0<i>Look Around You<\\/i><\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Sitcom<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Scripted dialogue creating a thematic situation; commonly found on television series<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\"><i>Seinfeld<\\/i>,\\u00a0<i>Fawlty Towers<\\/i>,\\u00a0<i>Black Books<\\/i>,\\u00a0<i>Porridge<\\/i>,\\u00a0<i>Dad''s Army<\\/i>,<i>Blackadder<\\/i>,\\u00a0<i>Gavin and Stacey<\\/i>,\\u00a0<i>My Wife and Kids<\\/i>,\\u00a0<i>I Love Lucy<\\/i>,<i>Friends<\\/i>,\\u00a0<i>Corner Gas<\\/i>,\\u00a0<i>That ''70s Show<\\/i>,\\u00a0<i>The Office<\\/i>,\\u00a0<i>The Cosby Show<\\/i>,\\u00a0<i>The Simpsons<\\/i>,\\u00a0<i>Open All Hours<\\/i>,\\u00a0<i>Only Fools and Horses<\\/i>,<i>Dinner Ladies<\\/i>,\\u00a0<i>Modern Family,<\\/i>\\u00a0Melissa &amp; Joey<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Sketch<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A shorter version of a sitcom, practised and typically performed live<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Armstrong and Miller,\\u00a0Jennifer Saunders,\\u00a0Lorne Michaels,\\u00a0Dawn French,\\u00a0Craig Ferguson,\\u00a0Catherine Tate; TV shows:\\u00a0<i>Monty Python<\\/i>,\\u00a0<i>Armstrong and Miller<\\/i>,\\u00a0<i>Saturday Night Live<\\/i>,\\u00a0<i>Chappelle''s Show<\\/i>,\\u00a0<i>Firesign Theatre<\\/i>,\\u00a0<i>In Living Color<\\/i>,\\u00a0<i>A Bit of Fry &amp; Laurie<\\/i>,<i>Mad TV<\\/i><\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Surreal comedy<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">A form of\\u00a0humorbased on bizarrejuxtapositions,absurd\\u00a0situations, and\\u00a0nonsense\\u00a0logic<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Spike Milligan,\\u00a0Jay Kogen,\\u00a0Eddie Izzard,\\u00a0J. Stewart Burns,\\u00a0Ross Noble,\\u00a0Bill Bailey,\\u00a0Brent Butt,\\u00a0The Mighty Boosh,\\u00a0Steven Wright,Trey Parker,\\u00a0Monty Python,\\u00a0Seth MacFarlane,\\u00a0David X. Cohen,Vic and Bob,\\u00a0The Goodies,\\u00a0Jack Handey,\\u00a0Derek Drymon,\\u00a0Wallace Wolodarsky,\\u00a0Harry Hill,\\u00a0The Kids in the Hall,\\u00a0Conan O''Brien,\\u00a0Tim and Eric,\\u00a0Paul Merton,\\u00a0Mitch Hedberg,\\u00a0Firesign Theatre,\\u00a0Shaun Micallef,\\u00a0Emo Philips<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Topical comedy\\/Satire<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Relies on headlining\\/important news and current affairs; it dates quickly, but is a popular form for late night talk-variety shows<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">George Carlin,\\u00a0Bill Hicks,\\u00a0Chris Morris,\\u00a0Dennis Miller,\\u00a0Conan O''Brien,\\u00a0Russell Howard,\\u00a0Craig Ferguson,\\u00a0David Letterman,\\u00a0Jay Leno,\\u00a0Dan Harmon,\\u00a0Andy Hamilton,\\u00a0Bill Maher,\\u00a0Ian Hislop,\\u00a0Brent Butt,\\u00a0Paul Merton,\\u00a0Kathy Griffin,\\u00a0Jon Stewart,\\u00a0Stephen Colbert,Stewart Lee,\\u00a0Matt Groening,\\u00a0Rory Bremner,\\u00a0W. Kamau Bell,\\u00a0Ben Elton,\\u00a0David Cross,\\u00a0Lewis Black,\\u00a0Chris Rock,\\u00a0Dave Chappelle,\\u00a0The Chaser,\\u00a0Punt and Dennis,\\u00a0Jon Holmes; TV shows:\\u00a0<i>The Daily Show<\\/i>,\\u00a0<i>Have I Got News For You<\\/i>,\\u00a0<i>Mock The Week<\\/i>,\\u00a0<i>The News Quiz<\\/i>,\\u00a0<i>Saturday Night Live<\\/i>,\\u00a0<i>The Simpsons<\\/i>,\\u00a0<i>The Tonight Show,<\\/i>Late Show with David Letterman<i>,<\\/i>\\u00a0Wait Wait... Don''t Tell Me!<i>,<\\/i>South Park<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">Wit\\/Word play<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em;\\">More intellectual forms based on clever, often subtle manipulation of language (thoughpuns\\u00a0can be crude and\\u00a0farcical)<\\/td>\\r\\n<td style=\\"border: 1px solid #aaaaaa; padding: 0.2em 0.4em; text-align: left;\\">Groucho Marx,\\u00a0William Shakespeare,\\u00a0Harry Hill,\\u00a0Jay Jason,\\u00a0Oscar Wilde,\\u00a0Rodney Marks,\\u00a0Woody Allen,\\u00a0George Carlin,\\u00a0Tim Vine,Stephen Fry,\\u00a0Demetri Martin,\\u00a0Bo Burnham,\\u00a0Firesign Theatre,\\u00a0Myq Kaplan<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>\\r\\n<p>Source:\\u00a0<a href=\\"http:\\/\\/en.wikipedia.org\\/wiki\\/Comedic_genres\\" target=\\"_blank\\">Wikipedia<\\/a><\\/p>","fulltext":"","state":1,"catid":"8","created":"2015-04-08 05:38:36","created_by":"258","created_by_alias":"","modified":"2015-04-08 05:42:36","modified_by":"258","checked_out":"258","checked_out_time":"2015-04-08 05:42:17","publish_up":"2015-04-08 05:38:36","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":2,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"1","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(8, 1, 1, '', '2015-04-12 18:54:19', 258, 4721, 'b76a91e130c2fe8764c76cf7efa0ac7884e1adb7', '{"id":1,"asset_id":"55","title":"Welcome to Comedy Central...!!!","alias":"welcome-to-comedy-central","introtext":"<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\"><img class=\\"pull-center\\" src=\\"images\\/comedy.jpg\\" alt=\\"\\" \\/><\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">According to modern views, Comedy (from the Greek: \\u03ba\\u03c9\\u03bc\\u1ff3\\u03b4\\u03af\\u03b1, k\\u014dm\\u014did\\u00eda) refers to any discourse or work generally intended to be humorous or to amuse by inducing laughter, especially in theatre, television, film and stand-up comedy. The origins of the term are found in Ancient Greece. In the Athenian democracy, the public opinion of voters was influenced by the political satire performed by the comic poets at the theaters. The theatrical genre of Greek comedy can be described as a dramatic performance which pits two groups or societies against each other in an amusing agon or conflict. Northrop Frye depicted these two opposing sides as a \\"Society of Youth\\" and a \\"Society of the Old\\". A revised view characterizes the essential agon of comedy as a struggle between a relatively powerless youth and the societal conventions that pose obstacles to his hopes. In this struggle, the youth is understood to be constrained by his lack of social authority, and is left with little choice but to take recourse in ruses which engender very dramatic irony which provokes laughter.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Satire and political satire use comedy to portray persons or social institutions as ridiculous or corrupt, thus alienating their audience from the object of their humor. Parody subverts popular genres and forms, critiquing those forms without necessarily condemning them.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Other forms of comedy include Screwball comedy, which derives its humor largely from bizarre, surprising (and improbable) situations or characters, and Black comedy, which is characterized by a form of humor that includes darker aspects of human behavior or human nature. Similarly scatological humor, sexual humor, and race humor create comedy by violating social conventions or taboos in comic ways. A comedy of manners typically takes as its subject a particular part of society (usually upper class society) and uses humor to parody or satirize the behavior and mannerisms of its members. Romantic comedy is a popular genre that depicts burgeoning romance in humorous terms and focuses on the foibles of those who are falling in love.<\\/span><\\/span><\\/p>","fulltext":"","state":1,"catid":"2","created":"2015-04-08 01:13:18","created_by":"258","created_by_alias":"","modified":"2015-04-12 18:54:19","modified_by":"258","checked_out":"258","checked_out_time":"2015-04-12 18:52:50","publish_up":"2015-04-08 01:13:18","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"\\",\\"show_print_icon\\":\\"\\",\\"show_email_icon\\":\\"\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":2,"ordering":"1","metakey":"","metadesc":"","access":"1","hits":"17","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0);
INSERT INTO `imwls_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(9, 2, 1, '', '2015-04-12 19:07:00', 258, 31560, '6fc1a88d2f5c311f5956c6345b8d5d7fc63e91fd', '{"id":2,"asset_id":"56","title":"How the Internet and a New Generation of Superfans Helped Create the Second Comedy Boom","alias":"how-the-internet-and-a-new-generation-of-superfans-helped-create-the-second-comedy-boom","introtext":"<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\"><img class=\\"pull-center\\" src=\\"images\\/echo-boom-opener.w529.h352.2x.jpg\\" alt=\\"Photo: Nathaniel Wood for Vulture\\" width=\\"750\\" \\/><\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cWhat\\u2019s up, people of the sex shop?\\u201d asks Hannibal Buress into an overdriven microphone at 9:30 p.m. on a recent Tuesday in L.A. He\\u2019s standing in front of a wall of crotchless panties, and next to a black curtain that divides the part of the West Hollywood sex-toy shop where a customer is examining Fleshlights from the part where the bi-weekly comedy show Performance Anxiety is taking place. Buress is in town for the roast of Justin Bieber, and he\\u2019s at the Pleasure Chest to rehearse his set for a small but enthusiastic audience that\\u2019s doing its best to ignore the commercial for stainless-steel G-spotters playing on a TV to Buress\\u2019s right.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">The sex-toy shop was the second place in two hours where I saw Buress, whose upcoming Comedy Central show had just been announced that morning. Earlier, he made a surprise appearance at Put Your Hands Together, a weekly stand-up show at the UCB Theatre on Franklin Avenue, to do the same roast jokes to a fully packed house. Of course he\\u2019s doing multiple sets: It\\u2019s Tuesday in Los Angeles, the best night for comedy in the country. I\\u2019d planned to see four shows, and I could\\u2019ve easily picked four different, equally great-sounding shows happening at the same time.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">After both sets, Buress, as many comedians do at midnight on Tuesdays, came to the Belly Room at the Comedy Store to watch a show called Roast Battle. Roast Battle is a phenomenon in L.A. (and will be everywhere when a TV deal comes through, which it surely will) in which comedians, two at a time, face off against each other, trading insults. Looking around the room \\u2014 to Buress\\u2019s left is Jeff Ross, the stand-up veteran who mentors this small show; behind Buress is Jerrod Carmichael, the young comedian with a Spike Lee\\u2013directed HBO special under his belt; to Buress\\u2019s right is Dave Chappelle, smoking cigarettes and laughing loudly at every joke; further back, the standing-room-only crowd is spilling into the hallway \\u2014 one can\\u2019t help but feel that this is the epicenter of a boom.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">It\\u2019s not just Los Angeles. There has never been a better time to be a comedian: The talent pool is broad, deep, and more diverse than ever before; a new generation of passionate fans is supporting experimental work; and there are countless ways (online, onscreen, in your earbuds, at live shows) for new voices to be heard and \\u2014 not always a given when it comes to the internet \\u2014 make a living. It\\u2019s a peak that hasn\\u2019t been seen since the first comedy boom, which lasted from 1979 to about 1995, and was defined by two stages: First, in the wake of the phenomenon that was Saturday Night Live, the popularity of George Carlin, Richard Pryor, and Steve Martin, and the debut of TV shows like An Evening at the Improv, hundreds of comedy clubs opened up overnight, resulting in a wave of frequently terrible observational comedians getting paid a lot of money to fill the bounty of open stages. Then, after the huge success of Seinfeld, Home Improvement, and Roseanne, many of those same comedians were all given sitcoms, most of which failed.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">With clubs closing and TV deals drying up, alternative comedy began to rise from the ashes around 1995, with confessional comedians like Janeane Garofalo and Marc Maron pushing comedy shows into nontraditional spaces. That lit a long fuse that finally boomed in a big way in 2009: the year Marc Maron started \\u201cWTF\\u201d; the year Broad City launched as a web series; the year Aziz Ansari taped his first stand-up special; the year Rob Delaney joined Twitter; the year Parks and Recreation, Community, and The League premiered, and Ellie Kemper joined The Office; when it seemed like UCB was the unofficial farm team for Saturday Night Live and every sitcom on TV.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Prior to 2009, only three comedians had ever sold out Madison Square Garden (not the theater, the actual arena); three comedians have done it in the last three years, each multiple times. According to UCB, over 10,000 people will take improv classes in the U.S. this year. More people watched SNL 40 than the Golden Globes. Comedy Central\\u2019s original programming has nearly doubled in the last three years, in competition with other networks that have beefed up their own comedy offerings. The hiring of a new Daily Show host \\u2014 congratulations, Trevor Noah \\u2014 was treated with as much anticipation and passionate critique as when LeBron James decided to go play for the Miami Heat. About as many people follow Sarah Silverman on Twitter than follow Hillary and Bill Clinton combined. Critics revere Louis C.K. and Amy Schumer as geniuses. Practically every comedian has a podcast or web series, or both. Right now, comedy is bigger than it\\u2019s ever been. There are more people doing it, and more people enjoying it. Thanks to the internet, comedians now have infinite means by which to reach their fans; and those fans are more fanatical than ever. Welcome to the Second Comedy Boom.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cAmazon has shows, Netflix, YouTube, Hulu \\u2014 there are a lot of different ways to get yourself out there as a comedian,\\u201d Buress told Vulture before the taping of the Bieber roast. \\u201cYou can build yourself through the internet. People have been getting TV shows from having successful podcasts.\\u201d And even those who aren\\u2019t on TV are finding enthusiastic audiences, and ways to sustain a living in comedy on their own terms.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Thirty years ago, comedians had to fight for a few large slices of a small pie. In the \\u201990s, a few performers made millions as stars of network sitcoms, but most were left in the cold when comedy clubs started shutting down. Now the pie is bigger and slices more plentiful, which benefits everyone from Buress (who now draws paychecks as a star on three cable shows) down to the armies of unknown UCB performers getting paid to make videos for Funny or Die and College Humor.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Podcasting is just one of the many ways by which comedians can develop a fan base, but over the last couple years, it\\u2019s also become one of the most lucrative. According to Adam Sachs, the CEO of Midroll \\u2014 the company sells ads for popular podcasts such as \\u201cWTF With Marc Maron,\\u201d Scott Aukerman\\u2019s \\u201cComedy Bang! Bang!,\\u201d Pete Holmes\\u2019s \\u201cYou Made It Weird,\\u201d and Paul Scheer\\u2019s \\u201cHow Did This Get Made\\u201d \\u2014 \\u201cmany comedians could survive today with the revenue from their podcasts alone.\\u201d Sachs says a podcast with 40,000 downloads per episode can gross well over $75,000 a year, and shows in the 100,000-download range can gross somewhere between $250,000 and $400,000. He estimates that three or four Midroll podcasts will make over $1 million this year. This has been a boon to comedians like Maron, Aukerman, Chris Hardwick, and Bill Burr, comedy veterans who had to wait for the boom to truly break through \\u2014 all of whom have recently parlayed successful podcasts into cable-TV shows and larger fees for stand-up appearances.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cPretty much all of my friends that came up with me in comedy are working,\\u201d says Kristen Schaal. \\u201cTelevision is in a golden age, and now there\\u2019s like 2,000 channels for comedians to play around on.\\u201d Instead of broad network sitcoms, more comedians are getting small, idiosyncratic shows on cable or streaming sites. In the last five years, Comedy Central has seen increased competition from IFC, FX, FXX, Fusion, TBS, Netflix, Amazon, Hulu, Yahoo, and TruTV.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Broadcast networks have also been getting smaller and stranger lately, as with Fox\\u2019s brilliantly weird (and well-rated) The Last Man on Earth. Vulture\\u2019s Josef Adalian explains that \\u201cwith most new network comedies in recent years getting ratings not much higher than cable,\\u201d networks are realizing they might as well aim for \\u201ccable quality.\\u201d The ultimate goal for a comedian in 2015 isn\\u2019t starring on his or her own Home Improvement, a mainstream sitcom that waters down their stand-up material \\u2014 it\\u2019s having their own Louie, a show on which the creator retains full auteurist control over every weird aspect: Kroll Show, Broad City, Comedy Bang! Bang!, The Last Man on Earth, The Eric Andre Show, NTSF:SD:SUV, Inside Amy Schumer, Nathan for You, Lucas Bros. Moving Co., Mulaney (yes, Mulaney might\\u2019ve seemed like a traditional broad sitcom, but the point is that was exactly what John Mulaney wanted), and others were all created, written, and headlined by their comedian stars. None of these performers are rich for life thanks to their shows, but they\\u2019re all earning a living doing artistically fulfilling work.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Compare this to the grind of the original comedy boom, during which, to eke out a decent income, a comedian had to tour the country doing the same 45-minute set. As Marc Maron explained to me over the phone, comedy clubs of the time were a \\u201cbar business, [they had] never been a show business. You were there to sell drinks.\\u201d Discos were dying, and comedy was the word. Richard Zoglin, explaining the original Comedy Boom in his 2008 book Comedy at the Edge, wrote: \\u201cAn evening at the comedy club was perfect entertainment for a baby-boom generation that was just hitting its peak dating years. You could take a date to the movies, but the conversation had to wait until afterward. The discos were too noisy. At a comedy club you could talk during the show, gauge each other\\u2019s reactions, see what your date laughed at \\u2014 or was grossed out by.\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">To today\\u2019s fans, comedy is more than just background noise: \\u201cMillennials have a deeper connection to comedy than previous generations,\\u201d says Chanon Cook, Comedy Central\\u2019s head of research. According to Cook, it was television that made the difference. The explosion of comedy on TV helped kill comedy clubs and bring the original boom to an end \\u2014 but the biggest stars of that boom wound up on Saturday Night Live or on their own sitcoms, many of which were essential viewing for millennials, planting the seeds of the second boom. Endless reruns of Seinfeld and stand-up specials on Comedy Central gave this generation an inbuilt awareness of comedy\\u2019s conventions: The comedy nerd was born.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">As Comedy Central claimed in an (admittedly self-serving) 2012 survey, millennials are the first generation that views humor as the most valuable part of their self-definition. \\u201cComedy is to this generation what music was to previous generations,\\u201d Cook says. \\u201cThey use it to define themselves. They use it to connect with people.\\u201d Maron described comedy nerds to me as \\u201cpeople who are into different facets of the history of comedy, the different types of comedy.\\u201d Says Pete Holmes: \\u201cThe comedy audience of 2015 is like the guitar to the musician. They\\u2019re not just sitting there to get fucked up and smoke a cigarette inside, which is what it was in the \\u201980s; they\\u2019re there to actually participate in something.\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\"><img class=\\"pull-center\\" src=\\"images\\/echo-boom-1.w529.h352.2x.jpg\\" alt=\\"\\" width=\\"750\\" \\/><\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">On Wednesday in L.A., I saw this type of fan in action at The Meltdown, the best comedy show in the city. Situated at NerdMelt, the West Hollywood comedy theater in the back of a comic-book store, produced by Emily Gordon, and hosted by comedians Jonah Ray and Kumail Nanjiani (who is Gordon\\u2019s husband and a star of HBO\\u2019s Silicon Valley), The Meltdown has run for five years and last year became a show on Comedy Central. With 200 people crammed into a 150-person space, the room was hot \\u2014 figuratively hot and, thanks to L.A.\\u2019s late-winter heat wave, literally hot. The stage is less a stage and more a place where the chairs stop and the lights shine. The audience \\u2026Well, the audience is something. I\\u2019ll let Ray describe it:<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">They are some of the most dedicated, sweetest weirdos around. When we started, there were these different people that we would recognize, and they slowly started sitting closer to the stage, and they all started to become friends and know each other. Now, every week, the first three or four rows are the diehard regulars. They\\u2019re creatives and artists and comics themselves, and them knowing each other, and us knowing them, has created an amazing dynamic for Kumail and I to play with.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Gordon would tell me later that the week I was there, one of these fans got a tattoo of the hand stamp given to The Meltdown attendees.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Diehard fans, like those of The Meltdown, are now the norm at comedy shows around the country. Because of them, comedy has fundamentally changed in the following ways:<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><strong><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.4px;\\">1. Comedy is more meta.<\\/span><\\/span><\\/strong><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Modern fans\\u2019 familiarity with the way comedy works means that the content of the comedy is often based in the process by which it\\u2019s created. \\u201cThere\\u2019s a sophistication [in the audience] that wasn\\u2019t there back in the Evening at the Improv days,\\u201d Holmes tells me, referring to the 1980s stand-up TV show. \\u201cThey know what a bit is.\\u201d Holmes, like most comedians in 2015, addresses the fact that he\\u2019s doing stand-up while he\\u2019s performing. Early in my week in L.A., at UCB\\u2019s monolithic Thai Town theater, Holmes closed the weeklyshow stand-up show Church, following Melissa Villase\\u00f1or, who destroyed the audience with a series of impressions ranging from Pikachu to Wanda Sykes. Taking the stage, Holmes sighed: \\u201cWell, she killed. Great.\\u201d He began his set: \\u201cMy dog got sprayed by a skunk today \\u2014 another comedian would then do 20 minutes of perfect material about that,\\u201d he said, mocking comedians who say things happened to them recently when, in fact, the things happened ages ago, whenever the performer started working on that joke. (Nowadays you\\u2019ll often hear jokes that start like, \\u201cA week ago \\u2014 or a week ago from whenever I wrote this bit.\\u201d) His set\\u2019s biggest laugh probably came during a bit about having a dog despite having grown up with cats, riffing, \\u201cOoh, a comedian talking about the difference between cats and dogs.\\u201d Comedy nerds are as interested in the source code as they are the jokes themselves. In 2015, the source code is the jokes.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><strong><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.4px;\\">2. The line between fan and performer is blurred.<\\/span><\\/span><\\/strong><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">The UCB Theatre has done plenty to help foster the second comedy boom \\u2014 popularizing improv, launching countless star performers, serving as the comedic hub for both New York and L.A., providing budding comedians with free stage time \\u2014 but its most important contribution is the way it changed the relationship between audience and performer. According to Kevin Hines, from the UCB Theatre\\u2019s training center in New York, 11,918 students took classes at UCB NY or LA last year. 11,918! (About 6,400 of those took Improv 101, the theater\\u2019s entry-level class, which is twice as many that took the same class five years ago). At UCB, everyone performs, and everyone watches everyone else. It\\u2019s a scene, not just a bunch of shows. Pete Holmes, who took classes at UCB while pursuing stand-up, explains: \\u201cI like doing stand-up like improv, by looking at the audience as your scene partner.\\u201d At a comedy show in 2015, everybody works together to create the best show possible. Everyone\\u2019s a comedian.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\"><img class=\\"pull-center\\" src=\\"images\\/echo-boom-3.w529.h352.2x.jpg\\" alt=\\"\\" width=\\"750\\" \\/><\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><strong><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.4px;\\">3. Comedy is more conversational and less punch-line-driven.<\\/span><\\/span><\\/strong><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Today, audiences expect comedians to talk to them like peers \\u2014 they don\\u2019t like being performed down to. \\u201c[Comedy\\u2019s] changed and become more conversational,\\u201d Nanjiani says. \\u201cPeople are okay listening to a podcast that\\u2019s an hour and a half, and it\\u2019s not funny, funny, funny, funny.\\u201d Nanjiani says those fans \\u201ctake that expectation to a comedy show as well,\\u201d and as a result, he\\u2019s learned to \\u201chide his punch lines\\u201d so as not to seem detached. Now Nanjiani and Ray often go into The Meltdown shows without any particular plan, and essentially just hang out for a few minutes. \\u201cComics that don\\u2019t make an effort to exist in that moment with everybody else in the room tend to get a colder reaction,\\u201d says Ray.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">This was on my mind as I watched comedian Mark Normand take the stage at The Meltdown. Two days earlier, at Hot Tub, Kristen Schaal and Kurt Braunohler\\u2019s weekly show, he had a rough go of it, telling well-crafted jokes (i.e., \\u201cI look at racism the same way I look at Nickelback: It\\u2019s fun to joke about, but ugh, you never want to see it live\\u201d) that would get short pulses of laughter instead of the bigger, rolling laugh of a fully engaged audience. The distance increased as Normand blamed the crowd for being too sensitive. He was trying to deliver his best material to an audience that didn\\u2019t want \\u201cmaterial\\u201d at all \\u2014 they just wanted to just get to know him. He connected better at the Meltdown. Setting up a set of jokes about homophobia, he asked if there were any gay guys in the audience. When no one responded, he joked, \\u201cWell, that is statistically impossible, [especially here,] in the back of a nerd museum.\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><strong><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.4px;\\">4. Experimentation is expected.<\\/span><\\/span><\\/strong><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Kristen Schaal, however, killed at both Hot Tub and The Meltdown with one of the funniest things I\\u2019ve seen in years: a one-woman show about Emily Dickinson (titled Emily Dick-unsung). Schaal\\u2019s version of Dickinson is a spinster with messy hair who hides poems around the stage and asks audience members to read them. One poem was just a grocery list, but Schaal acted as though it contained her innermost secrets: \\u201cThis poem is not ready for the world!\\u201d Another poem was actually just an olive in a folded piece of paper; she made an audience member \\u201cread\\u201d it by eating the olive. The bit doesn\\u2019t make much sense (especially when one attempts to describe it after the fact), but it destroyed both of the audiences I saw it with. I learned later that these were only the second and third times she\\u2019d ever performed it.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\"><img class=\\"pull-center\\" src=\\"images\\/echo-boom-2.w529.h352.2x.jpg\\" alt=\\"\\" width=\\"750\\" \\/><\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">In 2015, audiences are happily willing to follow comedians down any rabbit hole, no matter how unlikely the payoff. Sure, experimental comedians have existed since the \\u201970s \\u2013 Steve Martin, Andy Kaufman, Emo Philips, etc. \\u2014 but no longer are they the exception, as comedy-nerd audiences expect and are ready for comedians to have fun with the form (especially, for the first time, not just white males). Watch Rory Scovel\\u2019s brilliant set from The Meltdown TV show, for example. Scovel takes the stage and, responding to the audience\\u2019s enthusiastic welcome, ditches his planned material, and tells the crowd to keep clapping for the entirety of his ten-minute set. It\\u2019s an odd bit, especially considering that it\\u2019s being taped for television. He doesn\\u2019t tell jokes. He just offers a running commentary on what\\u2019s happening and eggs his audience on. He wonders what it would be like to be a TV viewer tuning in halfway through the bit. Eventually, he becomes mock-angry at his at-home audience: \\u201cStay the fuck out of here!\\u201d he shouts at the camera. \\u201cThis is our time down here!\\u201d At the end, the claps become a standing ovation, and Scovel seems actually touched that everyone went along with this very silly idea. Even in the two minutes and 41 seconds that aired on Comedy Central, you can see exactly what comedy looks like right now.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">So, where to from here? Actually, comedy\\u2019s next generation is already beginning to emerge. Back in New York, I go to the Holy Fuck Comedy Hour, a madcap weekly sketch show at the new Williamsburg location of the Annoyance Theatre. Started in Chicago in the late \\u201980s, the Annoyance has been a launchpad for many comedy careers \\u2014 including those of Stephen Colbert, Jane Lynch, Jeff Garlin, Vanessa Bayer, Jason Sudeikis, and others \\u2014 though Second City, where those Chicagoans would also perform, usually hogs credit for them. The Annoyance\\u2019s month-old New York location, below the Williamsburg Bridge and underneath a jazz club, seems to be their attempt to assert themselves as the alternative to the former alternative, UCB.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cLoud-lings\\u201d is how I like to describe the Holy Fuck Comedy Hour, which mixes Chicago-style brashness with the character focus of L.A.\\u2019s Groundlings. Perhaps to drown out the jazz coming from upstairs, everything is turned up to 11: Each sketch ends with someone onstage giving the cue to hit the \\u201clights\\u201d; New York\\u2019s most inventive stand-up, Jo Firestone, asks the audience to shout out \\u201csunshine, lollipops, or rainbows\\u201d whenever they want to interrupt her deadly serious (and hilarious) one-woman show about moving to New York; for a sketch in which gay parents bring their straight son to a doctor to turn him gay, a projector screen plays gay porn (with the actor who\\u2019s playing the son\\u2019s face Photoshopped onto every porn star\\u2019s) on a loop.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">The sketch of the night is slightly more subdued, but just as absurd. A woman comes onstage wearing a Saturday Night Live T-shirt, thumbing through her copy of the definitive SNL oral history Live From New York, geeking out just at the names of the people who agreed to be interviewed \\u2014 \\u201cBill Murray, hehe, awesome!\\u201d Then a boy comes onstage and asks what she\\u2019s reading. They flirt by repeating iconic SNL catchphrases (\\u201cChoppin\\u2019 broccoli!?\\u201d). The audience, packed densely enough to worry any good fire marshal, laughs knowingly. The woman is Claire Mulaney (John\\u2019s younger sister), a writer for Saturday Night Live, who is there performing the sketch on her week off. This is more than meta comedy for an accepting audience; it\\u2019s damn near postmodern. The sketch ends with Mulaney and her new beau screaming, \\u201cLive from New York, it\\u2019s Saturday Night!!!!!\\u201d Mulaney laughs. We all do. \\u201cLights!\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Source: <a href=\\"http:\\/\\/www.vulture.com\\/2015\\/03\\/welcome-to-the-second-comedy-boom.html\\" target=\\"_blank\\">www.vulture.com<\\/a><\\/span><\\/span><\\/p>","fulltext":"","state":1,"catid":"9","created":"2015-04-08 05:37:13","created_by":"258","created_by_alias":"","modified":"2015-04-12 19:07:00","modified_by":"258","checked_out":"258","checked_out_time":"2015-04-12 19:06:09","publish_up":"2015-04-08 05:37:13","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"0\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"0\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"0\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"0\\",\\"show_print_icon\\":\\"0\\",\\"show_email_icon\\":\\"0\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"0\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":3,"ordering":"0","metakey":"","metadesc":"","access":"1","hits":"8","metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0),
(10, 4, 1, '', '2015-04-12 19:31:49', 258, 8399, '9f3855b2ded98303f39179c0c294abc2d39acba1', '{"id":4,"asset_id":64,"title":" Justin Bieber\\u2019s Comedy Central Roast: The top 10 jokes from the night ","alias":"justin-bieber-s-comedy-central-roast-the-top-10-jokes-from-the-night","introtext":"<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\"><img src=\\"images\\/comedy-central-roast-justin-bieber-show.jpg\\" alt=\\"\\" \\/><\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">The joke\\u2019s on Justin Bieber.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Everything haters have wanted to tell the scandal-ridden pop star was finally uttered during Monday night\\u2019s Comedy Central roast of the 21-year-old Canadian \\u2013 and then some.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Whether it was his rocky on-and-off romance with Selena Gomez or his numerous run-ins with the law, Bieber egged on comedians, including host Kevin Hart, as they took turns skewering him onstage.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">The tattooed troublemaker took the humorously inappropriate jabs in good spirit, even using the moment as a turning point in his attempt to shed his bad-boy image and reinvent himself for the better.<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\"The things that I''ve done really don''t define who I am. I''m a kind-hearted person who loves people, and through it all, I lost some of my best qualities. For that, I''m sorry,\\" he said during the event, taped two weeks prior. \\"What I can say is I''m looking forward to being someone who you guys can look at and be proud of.\\"<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Until that day comes, here\\u2019s a look at the best jokes from Comedy Central\\u2019s roast:<\\/span><\\/span><\\/p>\\r\\n<h3><strong>KEVIN HART<\\/strong><\\/h3>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Ebola patients hear about \\u2018Bieber fever\\u2019 and say, \\u2018I\\u2019m gonna go ahead and ride this one out.\\u2019\\u201d<\\/span><\\/span><\\/p>\\r\\n<h3><strong>JEFFREY ROSS<\\/strong><\\/h3>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cIs it true you dumped (Selena Gomez) because she grew a mustache before you?\\u201d<\\/span><\\/span><\\/p>\\r\\n<h3><strong>LUDACRIS<\\/strong><\\/h3>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\"The Brazilian prostitute that claimed she was with Justin told the news that he was well-endowed. And that prostitute would know because so was he.\\"<\\/span><\\/span><\\/p>\\r\\n<h3><strong>CHRIS D''ELIA<\\/strong><\\/h3>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cYou have it all \\u2026 Except love, friends, good parents, and a Grammy.\\u201d<\\/span><\\/span><\\/p>\\r\\n<h3><strong>NATASHA LEGGERO<\\/strong><\\/h3>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\"Justin, Selena Gomez had to f--k you. She is literally the least lucky Selena in all of entertainment history.\\"<\\/span><\\/span><\\/p>\\r\\n<h3><strong>HANNIBAL BURESS<\\/strong><\\/h3>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cThey say that you roast the ones you love, but I don\\u2019t like you at all, man. I\\u2019m just here because it\\u2019s a real good opportunity for me.\\u201d<\\/span><\\/span><\\/p>\\r\\n<h3><strong>RON BURGUNDY<\\/strong><\\/h3>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cThis kid has spunk, moxie, and a few other STDs.\\u201d<\\/span><\\/span><\\/p>\\r\\n<h3><strong>SHAQ<\\/strong><\\/h3>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cJustin, as a father of six you have to straighten up, son. Last year, you were ranked the fifth most hated person of all time. Kim Jong-Un didn\\u2019t rank that low. And he uses your music to torture people.\\u201d<\\/span><\\/span><\\/p>\\r\\n<h3><strong>SNOOP DOGG<\\/strong><\\/h3>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cYou bought a monkey! I mean, that monkey was more embarrassed than the one that started the AIDS epidemic.\\u201d<\\/span><\\/span><\\/p>\\r\\n<h3><strong>MARTHA STEWART<\\/strong><\\/h3>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">\\u201cJustin, I\\u2019m sure it\\u2019s great to have 60 million followers on Twitter, but the only place people will be following you in jail is into the shower.\\u201d<\\/span><\\/span><\\/p>\\r\\n<p style=\\"margin: 0.5em 0px; text-align: justify;\\"><span style=\\"color: #252525; font-family: sans-serif;\\"><span style=\\"font-size: 14px; line-height: 22.3999996185303px;\\">Source: <a href=\\"http:\\/\\/www.nydailynews.com\\/entertainment\\/tv\\/10-best-jokes-justin-bieber-comedy-central-roast-article-1.2167143\\" target=\\"_blank\\">Daily News<\\/a><\\/span><\\/span><\\/p>","fulltext":"","state":1,"catid":"9","created":"2015-04-12 19:31:49","created_by":"258","created_by_alias":"","modified":"2015-04-12 19:31:49","modified_by":null,"checked_out":null,"checked_out_time":null,"publish_up":"2015-04-12 19:31:49","publish_down":"0000-00-00 00:00:00","images":"{\\"image_intro\\":\\"\\",\\"float_intro\\":\\"\\",\\"image_intro_alt\\":\\"\\",\\"image_intro_caption\\":\\"\\",\\"image_fulltext\\":\\"\\",\\"float_fulltext\\":\\"\\",\\"image_fulltext_alt\\":\\"\\",\\"image_fulltext_caption\\":\\"\\"}","urls":"{\\"urla\\":false,\\"urlatext\\":\\"\\",\\"targeta\\":\\"\\",\\"urlb\\":false,\\"urlbtext\\":\\"\\",\\"targetb\\":\\"\\",\\"urlc\\":false,\\"urlctext\\":\\"\\",\\"targetc\\":\\"\\"}","attribs":"{\\"show_title\\":\\"\\",\\"link_titles\\":\\"\\",\\"show_tags\\":\\"\\",\\"show_intro\\":\\"\\",\\"info_block_position\\":\\"\\",\\"show_category\\":\\"0\\",\\"link_category\\":\\"\\",\\"show_parent_category\\":\\"\\",\\"link_parent_category\\":\\"\\",\\"show_author\\":\\"0\\",\\"link_author\\":\\"\\",\\"show_create_date\\":\\"\\",\\"show_modify_date\\":\\"\\",\\"show_publish_date\\":\\"0\\",\\"show_item_navigation\\":\\"\\",\\"show_icons\\":\\"0\\",\\"show_print_icon\\":\\"0\\",\\"show_email_icon\\":\\"0\\",\\"show_vote\\":\\"\\",\\"show_hits\\":\\"0\\",\\"show_noauth\\":\\"\\",\\"urls_position\\":\\"\\",\\"alternative_readmore\\":\\"\\",\\"article_layout\\":\\"\\",\\"show_publishing_options\\":\\"\\",\\"show_article_options\\":\\"\\",\\"show_urls_images_backend\\":\\"\\",\\"show_urls_images_frontend\\":\\"\\"}","version":1,"ordering":null,"metakey":"","metadesc":"","access":"1","hits":null,"metadata":"{\\"robots\\":\\"\\",\\"author\\":\\"\\",\\"rights\\":\\"\\",\\"xreference\\":\\"\\"}","featured":"0","language":"*","xreference":""}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `imwls_updates`
--

CREATE TABLE IF NOT EXISTS `imwls_updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) DEFAULT '',
  `description` text NOT NULL,
  `element` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `folder` varchar(20) DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) DEFAULT '',
  `data` text NOT NULL,
  `detailsurl` text NOT NULL,
  `infourl` text NOT NULL,
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Available Updates' AUTO_INCREMENT=59 ;

--
-- Dumping data for table `imwls_updates`
--

INSERT INTO `imwls_updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`, `extra_query`) VALUES
(1, 3, 0, 'Malay', '', 'pkg_ms-MY', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/ms-MY_details.xml', '', ''),
(2, 3, 0, 'Romanian', '', 'pkg_ro-RO', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/ro-RO_details.xml', '', ''),
(3, 3, 0, 'Flemish', '', 'pkg_nl-BE', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/nl-BE_details.xml', '', ''),
(4, 3, 0, 'Chinese Traditional', '', 'pkg_zh-TW', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/zh-TW_details.xml', '', ''),
(5, 3, 0, 'French', '', 'pkg_fr-FR', 'package', '', 0, '3.4.1.2', '', 'http://update.joomla.org/language/details3/fr-FR_details.xml', '', ''),
(6, 3, 0, 'Galician', '', 'pkg_gl-ES', 'package', '', 0, '3.3.1.2', '', 'http://update.joomla.org/language/details3/gl-ES_details.xml', '', ''),
(7, 3, 0, 'German', '', 'pkg_de-DE', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/de-DE_details.xml', '', ''),
(8, 3, 0, 'Greek', '', 'pkg_el-GR', 'package', '', 0, '3.3.3.1', '', 'http://update.joomla.org/language/details3/el-GR_details.xml', '', ''),
(9, 3, 0, 'Japanese', '', 'pkg_ja-JP', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/ja-JP_details.xml', '', ''),
(10, 3, 0, 'Hebrew', '', 'pkg_he-IL', 'package', '', 0, '3.1.1.1', '', 'http://update.joomla.org/language/details3/he-IL_details.xml', '', ''),
(11, 3, 0, 'EnglishAU', '', 'pkg_en-AU', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/en-AU_details.xml', '', ''),
(12, 3, 0, 'EnglishUS', '', 'pkg_en-US', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/en-US_details.xml', '', ''),
(13, 3, 0, 'Hungarian', '', 'pkg_hu-HU', 'package', '', 0, '3.3.3.1', '', 'http://update.joomla.org/language/details3/hu-HU_details.xml', '', ''),
(14, 3, 0, 'Afrikaans', '', 'pkg_af-ZA', 'package', '', 0, '3.2.0.2', '', 'http://update.joomla.org/language/details3/af-ZA_details.xml', '', ''),
(15, 3, 0, 'Arabic Unitag', '', 'pkg_ar-AA', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/ar-AA_details.xml', '', ''),
(16, 3, 0, 'Belarusian', '', 'pkg_be-BY', 'package', '', 0, '3.2.1.1', '', 'http://update.joomla.org/language/details3/be-BY_details.xml', '', ''),
(17, 3, 0, 'Bulgarian', '', 'pkg_bg-BG', 'package', '', 0, '3.3.0.1', '', 'http://update.joomla.org/language/details3/bg-BG_details.xml', '', ''),
(18, 3, 0, 'Catalan', '', 'pkg_ca-ES', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/ca-ES_details.xml', '', ''),
(19, 3, 0, 'Chinese Simplified', '', 'pkg_zh-CN', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/zh-CN_details.xml', '', ''),
(20, 3, 0, 'Croatian', '', 'pkg_hr-HR', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/hr-HR_details.xml', '', ''),
(21, 3, 0, 'Czech', '', 'pkg_cs-CZ', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/cs-CZ_details.xml', '', ''),
(22, 3, 0, 'Danish', '', 'pkg_da-DK', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/da-DK_details.xml', '', ''),
(23, 3, 0, 'Dutch', '', 'pkg_nl-NL', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/nl-NL_details.xml', '', ''),
(24, 3, 0, 'Estonian', '', 'pkg_et-EE', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/et-EE_details.xml', '', ''),
(25, 3, 0, 'Italian', '', 'pkg_it-IT', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/it-IT_details.xml', '', ''),
(26, 3, 0, 'Korean', '', 'pkg_ko-KR', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/ko-KR_details.xml', '', ''),
(27, 3, 0, 'Latvian', '', 'pkg_lv-LV', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/lv-LV_details.xml', '', ''),
(28, 3, 0, 'Macedonian', '', 'pkg_mk-MK', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/mk-MK_details.xml', '', ''),
(29, 3, 0, 'Norwegian Bokmal', '', 'pkg_nb-NO', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/nb-NO_details.xml', '', ''),
(30, 3, 0, 'Norwegian Nynorsk', '', 'pkg_nn-NO', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/nn-NO_details.xml', '', ''),
(31, 3, 0, 'Persian', '', 'pkg_fa-IR', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/fa-IR_details.xml', '', ''),
(32, 3, 0, 'Polish', '', 'pkg_pl-PL', 'package', '', 0, '3.4.0.1', '', 'http://update.joomla.org/language/details3/pl-PL_details.xml', '', ''),
(33, 3, 0, 'Portuguese', '', 'pkg_pt-PT', 'package', '', 0, '3.3.3.1', '', 'http://update.joomla.org/language/details3/pt-PT_details.xml', '', ''),
(34, 3, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '3.3.6.2', '', 'http://update.joomla.org/language/details3/ru-RU_details.xml', '', ''),
(35, 3, 0, 'Slovak', '', 'pkg_sk-SK', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/sk-SK_details.xml', '', ''),
(36, 3, 0, 'Swedish', '', 'pkg_sv-SE', 'package', '', 0, '3.4.1.3', '', 'http://update.joomla.org/language/details3/sv-SE_details.xml', '', ''),
(37, 3, 0, 'Syriac', '', 'pkg_sy-IQ', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/sy-IQ_details.xml', '', ''),
(38, 3, 0, 'Tamil', '', 'pkg_ta-IN', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/ta-IN_details.xml', '', ''),
(39, 3, 0, 'Thai', '', 'pkg_th-TH', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/th-TH_details.xml', '', ''),
(40, 3, 0, 'Turkish', '', 'pkg_tr-TR', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/tr-TR_details.xml', '', ''),
(41, 3, 0, 'Ukrainian', '', 'pkg_uk-UA', 'package', '', 0, '3.3.3.15', '', 'http://update.joomla.org/language/details3/uk-UA_details.xml', '', ''),
(42, 3, 0, 'Uyghur', '', 'pkg_ug-CN', 'package', '', 0, '3.3.0.1', '', 'http://update.joomla.org/language/details3/ug-CN_details.xml', '', ''),
(43, 3, 0, 'Albanian', '', 'pkg_sq-AL', 'package', '', 0, '3.1.1.1', '', 'http://update.joomla.org/language/details3/sq-AL_details.xml', '', ''),
(44, 3, 0, 'Hindi', '', 'pkg_hi-IN', 'package', '', 0, '3.3.6.1', '', 'http://update.joomla.org/language/details3/hi-IN_details.xml', '', ''),
(45, 3, 0, 'Portuguese Brazil', '', 'pkg_pt-BR', 'package', '', 0, '3.4.1.2', '', 'http://update.joomla.org/language/details3/pt-BR_details.xml', '', ''),
(46, 3, 0, 'Serbian Latin', '', 'pkg_sr-YU', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/sr-YU_details.xml', '', ''),
(47, 3, 0, 'Spanish', '', 'pkg_es-ES', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/es-ES_details.xml', '', ''),
(48, 3, 0, 'Bosnian', '', 'pkg_bs-BA', 'package', '', 0, '3.4.0.1', '', 'http://update.joomla.org/language/details3/bs-BA_details.xml', '', ''),
(49, 3, 0, 'Serbian Cyrillic', '', 'pkg_sr-RS', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/sr-RS_details.xml', '', ''),
(50, 3, 0, 'Vietnamese', '', 'pkg_vi-VN', 'package', '', 0, '3.2.1.1', '', 'http://update.joomla.org/language/details3/vi-VN_details.xml', '', ''),
(51, 3, 0, 'Bahasa Indonesia', '', 'pkg_id-ID', 'package', '', 0, '3.3.0.2', '', 'http://update.joomla.org/language/details3/id-ID_details.xml', '', ''),
(52, 3, 0, 'Finnish', '', 'pkg_fi-FI', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/fi-FI_details.xml', '', ''),
(53, 3, 0, 'Swahili', '', 'pkg_sw-KE', 'package', '', 0, '3.4.1.1', '', 'http://update.joomla.org/language/details3/sw-KE_details.xml', '', ''),
(54, 3, 0, 'Montenegrin', '', 'pkg_srp-ME', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/srp-ME_details.xml', '', ''),
(55, 3, 0, 'EnglishCA', '', 'pkg_en-CA', 'package', '', 0, '3.3.6.1', '', 'http://update.joomla.org/language/details3/en-CA_details.xml', '', ''),
(56, 3, 0, 'FrenchCA', '', 'pkg_fr-CA', 'package', '', 0, '3.3.6.1', '', 'http://update.joomla.org/language/details3/fr-CA_details.xml', '', ''),
(57, 3, 0, 'Welsh', '', 'pkg_cy-GB', 'package', '', 0, '3.3.0.1', '', 'http://update.joomla.org/language/details3/cy-GB_details.xml', '', ''),
(58, 3, 0, 'Sinhala', '', 'pkg_si-LK', 'package', '', 0, '3.3.1.1', '', 'http://update.joomla.org/language/details3/si-LK_details.xml', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_update_sites`
--

CREATE TABLE IF NOT EXISTS `imwls_update_sites` (
  `update_site_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `location` text NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) DEFAULT '',
  PRIMARY KEY (`update_site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Update Sites' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `imwls_update_sites`
--

INSERT INTO `imwls_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla! Core', 'collection', 'http://update.joomla.org/core/list.xml', 1, 1429905281, ''),
(2, 'Joomla! Extension Directory', 'collection', 'http://update.joomla.org/jed/list.xml', 1, 1429905281, ''),
(3, 'Accredited Joomla! Translations', 'collection', 'http://update.joomla.org/language/translationlist_3.xml', 1, 1429905279, ''),
(4, 'Joomla! Update Component Update Site', 'extension', 'http://update.joomla.org/core/extensions/com_joomlaupdate.xml', 1, 1429905279, ''),
(6, 'CComment Core', 'extension', 'https://compojoom.com/index.php?option=com_ars&view=update&task=stream&format=xml&id=16&dummy=extension.xml', 1, 1429905299, '');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_update_sites_extensions`
--

CREATE TABLE IF NOT EXISTS `imwls_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`update_site_id`,`extension_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links extensions to update sites';

--
-- Dumping data for table `imwls_update_sites_extensions`
--

INSERT INTO `imwls_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 700),
(3, 600),
(4, 28),
(6, 10011);

-- --------------------------------------------------------

--
-- Table structure for table `imwls_usergroups`
--

CREATE TABLE IF NOT EXISTS `imwls_usergroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  KEY `idx_usergroup_title_lookup` (`title`),
  KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `imwls_usergroups`
--

INSERT INTO `imwls_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 18, 'Public'),
(2, 1, 8, 15, 'Registered'),
(3, 2, 9, 14, 'Author'),
(4, 3, 10, 13, 'Editor'),
(5, 4, 11, 12, 'Publisher'),
(6, 1, 4, 7, 'Manager'),
(7, 6, 5, 6, 'Administrator'),
(8, 1, 16, 17, 'Super Users'),
(9, 1, 2, 3, 'Guest');

-- --------------------------------------------------------

--
-- Table structure for table `imwls_users`
--

CREATE TABLE IF NOT EXISTS `imwls_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_block` (`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=259 ;

--
-- Dumping data for table `imwls_users`
--

INSERT INTO `imwls_users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES
(258, 'Super User', 'ma4V2er@r^4', 'rutujasudam.marathe@stonybrook.edu', '$2y$10$hiBZXALcAzN4hIdqkNS0/ej5qKIjLPd.gX7Xv58CuQ16EGf4YfHNO', 0, 1, '2015-03-27 06:08:45', '2015-04-24 19:55:05', '0', '', '0000-00-00 00:00:00', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `imwls_user_keys`
--

CREATE TABLE IF NOT EXISTS `imwls_user_keys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `series` varchar(255) NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) NOT NULL,
  `uastring` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `series` (`series`),
  UNIQUE KEY `series_2` (`series`),
  UNIQUE KEY `series_3` (`series`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_user_notes`
--

CREATE TABLE IF NOT EXISTS `imwls_user_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_category_id` (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `imwls_user_profiles`
--

CREATE TABLE IF NOT EXISTS `imwls_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) NOT NULL,
  `profile_value` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Table structure for table `imwls_user_usergroup_map`
--

CREATE TABLE IF NOT EXISTS `imwls_user_usergroup_map` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imwls_user_usergroup_map`
--

INSERT INTO `imwls_user_usergroup_map` (`user_id`, `group_id`) VALUES
(258, 8);

-- --------------------------------------------------------

--
-- Table structure for table `imwls_viewlevels`
--

CREATE TABLE IF NOT EXISTS `imwls_viewlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `title` varchar(100) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_assetgroup_title_lookup` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `imwls_viewlevels`
--

INSERT INTO `imwls_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 2, '[6,2,8]'),
(3, 'Special', 3, '[6,3,8]'),
(5, 'Guest', 1, '[9]'),
(6, 'Super Users', 4, '[8]');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
