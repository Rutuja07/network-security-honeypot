```
#!c

Log is written to /var/log/mail.log

Setup

PreBuild
====================================
+ Install libpam0g-dev
+ Create dovenull and dovecot users

Build
====================================
./configure
make
sudo make install

Configuration
====================================
+ Change rsyslog to not escape control characters ( Add line "$EscapeControlCharactersOnReceive off" to /etc/rsyslog.conf )
+ Copy over configuration ( cp -r /usr/local/share/doc/dovecot/example-config/* /usr/local/etc/dovecot/ )

+ Create certificates:
sudo openssl req -new -x509 -days 3650 -nodes -out /etc/ssl/certs/dovecot.pem -keyout /etc/ssl/private/dovecot.pem
sudo chmod o= /etc/ssl/private/dovecot.pem

+ Enable PAM authentication:

File: /etc/pam.d/dovecot

auth    required        pam_unix.so
account required        pam_unix.so

+ Create init.d script ( http://wiki2.dovecot.org/DovecotInit )
+ Set dovecot service to startup on boot ( sudo update-rc.d dovecot defaults )
```