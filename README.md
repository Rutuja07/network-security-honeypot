-------------------------------***     Install Prerequisites    ***----------------------------

Use following command to install prerequisites like mysql, php5 and phpmyadmin


sudo apt-get install apache2 php5-mysql libapache2-mod-php5 mysql-server php5-json phpmyadmin php5-curl unzip

During installation of phpmyadmin and mysql server, you will asked to set password. Keep record of these passwords for later purpose.

------------------------------------***   Joomla Installation   ***----------------------------

For joomla installation, use following commands:

1.	Download joomla

mkdir joomla

cd joomla

wget https://github.com/joomla/joomla-cms/releases/download/3.4.1/Joomla_3.4.1-Stable-Full_Package.zip

unzip Joomla_3.4.1-Stable-Full_Package.zip

rm -f Joomla_3.4.1-Stable-Full_Package.zip

2.	Move Joomla to var folder

cd ..

sudo mv joomla /var/www/html

cd /var/www/html/joomla

3.	Change permissions of folder

sudo chown -R www-data:www-data .

sudo find . -type f -exec chmod 644 {} \;

sudo find . -type d -exec chmod 755 {} \;

4.	Create the Database for joomla and grant permissions t root user

mysql -u root -p

mysql> CREATE DATABASE joomladb

mysql>  GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON joomla.* TO 'joomladb'@'localhost' IDENTIFIED BY 'password';

mysql> \q

‘joomladb’ is  the name of your database and set password you want use to access database in the field ‘password’.

5.	Restart Apache server

sudo service apache2 restart

6.	Setup initial information using the web based setup.

Open the link http://id_address_of _server/joomla/ web browser and enter database credentials. Follow the instructions on the webpage to set up administration account. Now you can access both website and joomla administration. 

7.	Adding extension for comments on each page.

Go to Joomla administration page. Go extension manager, choose option of install from URL. Copy paste following URL and install extension:

https://compojoom.com/downloads/official-releases-stable/ccomment/ccomment-5-3-1/com_comment-5-3-1-core-zip?format=raw

Go to Manage Plugins and enable ‘CComment administration’. For customization of CComments, go to Components/CComment. Here you can select all the pages on which comment should be enabled. 

Go to module manger and search for CComment . Publish that module and set module position to ‘body-bottom’.

----------------------------------***   Joomla Login Log   ***---------------------------------

Go to /var/www/html/joomla. 
Go to /libraries/cms/application/cms.php.
Paste following code at the start of login() function:

                date_default_timezone_set("UTC");
                $ipadd= getenv('REMOTE_ADDR');
                $myfile = fopen("/var/www/html/joomla/joomla.log", "a");
                $val="User:".$credentials['username']."\nPassword:".$credentials['password'];
                $val=$val."\nIP:".$ipadd."\nTime:".date("Y-m-d H:i:s", time())."\n";
                fwrite($myfile,$val);
                fclose($myfile);

This will create joomla.log file in joomla folder and will record all login attempt.

--------------------------------***   WordPress Installation   ***-----------------------------

For WordPress installation, use following commands:

1.	Download WordPress

mkdir wordpress

cd wordpress

wget https://wordpress.org/latest.tar.gz

tar xzfv latest.tar.zip

rm -f latest.tar.zip

2.	Move WordPress to var folder

cd ..

sudo mv wordpress /var/www/html

cd /var/www/html/wordpress

3.	Change permissions of folder

sudo chown -R www-data:www-data .

sudo find . -type f -exec chmod 644 {} \;

sudo find . -type d -exec chmod 755 {} \;

4.	Create the Database for WordPress and grant permissions t root user

mysql -u root -p

mysql> CREATE DATABASE wordpressdb

mysql>  GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON wordpress.* TO 'wordpressdb'@'localhost' IDENTIFIED BY 'password';

mysql> \q

‘wordpressdb’ is  the name of your database and set password you want use to access database in the field ‘password’.

5.	Change Configuration file

cp wp-config-sample.php wp-config.php

nano wp-config.php

Replace DB_NAME, DB_USER and DB_PASSWORD by database name, username and password you set up in above step. 

6.	Restart Apache server

	sudo service apache2 restart

7.	Setup initial information using the web based setup.

Open the link http://id_address_of _server/wordpress/ web browser and enter database credentials. Follow the instructions on the webpage to set up administration account. Now you can access both website and WordPress administration. Customize website using administration interface.

-------------------------------***   WordPress Login Log   ***---------------------------------

Go to /var/www/html/wordpress. 
Open wp-login.php.
Search for “case ‘login’:”. 
Find following if condition:

if ( !empty($_POST['log']) && !force_ssl_admin() ) 

In that if condition paste following code after line “ $user_name = sanitize_user($_POST['log']);”

                date_default_timezone_set("UTC");
                $time=date("Y-m-d H-i-s", time());
                $pas=$_POST['pwd'];
                $ipadd=$_SERVER['REMOTE_ADDR'];
                $ban = "User:$user_name\nPassword:$pas\nIP:$ipadd\nTime:$time\n";
                $file = '/var/www/html/wordpress/wp-content/wordpress.log';
                $open = fopen( $file, "a" );
                $write = fputs( $open, $ban );
                fclose( $open );

This will create wordpress.log file in wordpress/wp-content folder and will record all login attempt.